# membership.eclipse.org

Supported by our member organizations, the Eclipse Foundation provides our community with Intellectual Property, Mentorship, Marketing, Event and IT Services.

<!-- TOC -->
- [membership.eclipse.org](#membershipeclipseorg)
  - [Getting Started - Front-end](#getting-started---front-end)
    - [Requirements](#requirements)
    - [Setup](#setup)
  - [CSRF and API Security](#csrf-and-api-security)
  - [Getting Started - Full Stack](#getting-started---full-stack)
    - [Requirements](#requirements-1)
    - [Setup](#setup-1)
      - [Preliminary Setup](#preliminary-setup)
      - [Environment and properties](#environment-and-properties)
    - [Running](#running)
    - [Docker](#docker)
      - [Generate Certs for HTTPS](#generate-certs-for-https)
      - [Update your Host file](#update-your-host-file)
    - [MariaDB setup](#mariadb-setup)
  - [Contributing](#contributing)
    - [Declared Project Licenses](#declared-project-licenses)
  - [Bugs and feature requests](#bugs-and-feature-requests)
  - [Authors](#authors)
  - [Trademarks](#trademarks)
  - [Copyright and license](#copyright-and-license)
<!-- /TOC -->

[^ Top](#membershipeclipseorg)
## Getting Started - Front-end

[^ Top](#membershipeclipseorg)
### Requirements

Before you start, please make sure you have [yarn](https://classic.yarnpkg.com/en/docs/install/) and [Node 14](https://nodejs.org/en/blog/release/v14.17.3) installed.

[^ Top](#membershipeclipseorg)
### Setup

Once that's done, you can install dependencies, build assets and start a dev server:

```bash
yarn --cwd src/main/www
yarn --cwd src/main/www build
yarn --cwd src/main/www start
yarn --cwd src/main/www start-spec
```

The web app will run in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits. You will also see any lint errors in the console.

[^ Top](#membershipeclipseorg)
## CSRF and API Security
Currently, the endpoints that can contain personal data of users have been secured by OIDC and CSRF. What this means for development in the front end is all requests will need to be performed with a legitimate Eclipse Foundation login and account for the CSRF header.

Pertaining to data posted to the API, there is no current automatic deletion policy enforced, and no current way in the UI to send a call to delete data. If you wish to delete this data, you will need to craft javascript within the site to take advantage of the session and CSRF headers, and manually make the call. More information on the form deletion endpoint can be seen in the OpenAPI spec under `/spec/openapi.yml`.

Additionally, when requesting any PII/form data, a CSRF token will need to be passed unless disabled on a development server. This token will live under the `x-csrf-token` header that is supplied on every request the user makes to the server, including the unprotected `/csrf/` endpoint that is available. The token should be posted back to the server using the same header. This value will remain the same for the duration of the browser session.

[^ Top](#membershipeclipseorg)
## Getting Started - Full Stack

[^ Top](#membershipeclipseorg)
### Requirements

Before you start, you will need the following tools in your local environment:

- Docker
- Node 18
- mvn
- make
- yarn
- Java 17

[^ Top](#membershipeclipseorg)
### Setup

The initial setup assumes that the users developing for this application are employees of Eclipse Foundation with access to internal tooling and servers. If you do not have access to these resources, manual installation and setups will be required.

[^ Top](#membershipeclipseorg)
#### Preliminary Setup

The first step is to start Docker. The initial setup of this application can be started by running the command `make setup` which will pull the eclipsefoundation DDL file, create empty `secret.properties` files, start the mariadb container with inital DBs set up, and create the `volumes/imagestore` folder with the proper permissions. You may be prompted for your sudo password.

[^ Top](#membershipeclipseorg)
#### Environment and properties

Once initial setup is done, retrieve the secrets in the Webdev collection in BitWarden related to this service. They follow the naming pattern of `membership-*-dev-secrets`, where the star is replaced by the service the secrets are used for. These dev secrets will bind to the dockerized MariaDB mentioned in the previous step as well as the EF Keycloak instance, connecting to the staging realms (separate from production assets). These secrets should be placed under `./config/*/secret.properties`, where the star is the service folder in the BitWarden secret name. This needs to be done before starting the actual services as otherwise Docker will create folders with incorrect permissions and need to be manually fixed. Additionally, if the mariadb password has been changed from default, replace the `quarkus.datasource.password` and `quarkus.datasource.*.password` values with the local password value. This can be retrieved by using `echo $MARIADB_PASSWORD` in the terminal if you're unsure.

[^ Top](#membershipeclipseorg)
### Running

Once the DB is running and the secrets have been set into the secret files, then run `make compile-start`. This command will compile the react and Java applications, build the images, and start them in docker. This should be all that is needed to start the application. Below are the port mappings used to access the application from outside the docker network.

- NOTE: If the Java compilation step fails, it can be restarted by running `make compile-java`. The Docker image can then be built and started using `make start`

| Service | Port |
| ------ | ------ |
| Membership - Application | 10102 |
| Membership - Portal | 10103 |
| Membership - FoundationDB | 10104 |
| Membership - MariaDB | 10105 |
| Membership - WG API | 10106 |

[^ Top](#membershipeclipseorg)
### Docker

[^ Top](#membershipeclipseorg)
#### Generate Certs for HTTPS

You will need to create a certificate in order to serve the Application on https.

- IMPORTANT NOTE: when prompted for `Common Name (e.g. server FQDN or YOUR name) []`, ensure it is set to `www.rem.docker`.

```sh
make generate-cert
```

[^ Top](#membershipeclipseorg)
#### Update your Host file

We use [jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy) as automated Nginx reverse proxy for our docker containers. So instead of having to lookup the port of a new service, you can simply remember it's internal dev hostname.

Different operating system, different file paths!

Windows: C:\Windows\System32\drivers\etc\hosts
Linux / MacOS: /etc/hosts

```
# rem services

127.0.0.1 keycloak
127.0.0.1 api.rem.docker
127.0.0.1 www.rem.docker
127.0.0.1 nginx.rem.docker
```

[^ Top](#membershipeclipseorg)
### MariaDB setup

The base definitions of the tables as required for the connection of this API are defined under `./config/mariadb/main/init.d/`. The mariadb startup process should setup these DBs at build-time. Should there be any DBs or tables missing, these files can be used to add them.

[^ Top](#membershipeclipseorg)
## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [membership.eclipse.org](https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/membership.eclipse.org.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

[^ Top](#membershipeclipseorg)
### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

[^ Top](#membershipeclipseorg)
## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org/-/issues).

[^ Top](#membershipeclipseorg)
## Authors

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://gitlab.eclipse.org/cguindon>

**Martin Lowe (Eclipse Foundation)**

- <https://gitlab.eclipse.org/malowe>

**Zachary Sabourin (Eclipse Foundation)**

- <https://gitlab.eclipse.org/zacharysabourin>

[^ Top](#membershipeclipseorg)
## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

[^ Top](#membershipeclipseorg)
## Copyright and license

Copyright 2018-2023 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [membership.eclipse.org authors](https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org/-/graphs/main). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org/-/raw/main/README.md).
