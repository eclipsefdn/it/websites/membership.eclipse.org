Dear {name},  

This email is to confirm that you have requested to remove the following individual as a contact associated with your organization.  
  
Organization: {data.getOrganization.name} ({data.getOrganization.organizationID})  
Username: {data.getPerson.lname}, {data.getPerson.fname}({data.getPerson.personID})  
Reason: {data.getReason}  
  
The Eclipse Foundation team will act on your request as soon as possible, and typically within two business days. We will reach out to you should there be any questions.  Also, please note that this action will not remove the individual from the Eclipse Foundation records, but rather will simply disassociate the individual with your organization.  
  
If you have not made this request, kindly reach out to membership.coordination@eclipse-foundation.org.  
  
Thank you,  
  
Membership Coordination Team.  

----------

This is an automatic message from https://membership.eclipse.org
Twitter: @EclipseFdn
