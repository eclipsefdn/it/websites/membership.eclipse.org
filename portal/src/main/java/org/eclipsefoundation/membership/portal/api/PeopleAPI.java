/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.jboss.resteasy.reactive.RestQuery;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.vertx.http.Compressed;
import jakarta.annotation.Nullable;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@OidcClientFilter
@Path("people")
@RegisterRestClient(configKey = "fdndb-api")
public interface PeopleAPI {

    @GET
    @Compressed
    RestResponse<List<PeopleData>> getPeople(@BeanParam BaseAPIParameters baseParams, @BeanParam PeopleRequestParams params);

    public class PeopleRequestParams {
        @Nullable
        @RestQuery
        public String email;

        @Nullable
        @RestQuery
        public String id;

        @Nullable
        @RestQuery
        public String firstName;

        @Nullable
        @RestQuery
        public String lastName;

        @Nullable
        @RestQuery
        public String organizationId;

        @Nullable
        @RestQuery
        public String projectRelation;

        @Nullable
        @RestQuery
        public List<String> ids;

        public PeopleRequestParams() {
        }

        public PeopleRequestParams(String email, String username, String firstName, String lastName, String organizationID, String relation,
                List<String> ids) {
            this.email = email;
            this.id = username;
            this.firstName = firstName;
            this.lastName = lastName;
            this.organizationId = organizationID;
            this.projectRelation = relation;
            this.ids = ids;
        }
    }
}
