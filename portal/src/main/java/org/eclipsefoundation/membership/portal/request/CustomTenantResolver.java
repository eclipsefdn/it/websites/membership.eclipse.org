/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.oidc.TenantResolver;
import io.vertx.ext.web.RoutingContext;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Required to handle multi-tenancy for enhanced organizations endpoint. Using annotations did not work with 3.8.6, and there is no
 * configuration for this property yet.
 */
@ApplicationScoped
public class CustomTenantResolver implements TenantResolver {
    public static final Logger LOGGER = LoggerFactory.getLogger(CustomTenantResolver.class);

    @Override
    public String resolve(RoutingContext context) {
        if (context.normalizedPath().equalsIgnoreCase("/api/organizations/enhanced")) {
            return "application";
        }
        return null;
    }
}
