/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.namespace;

import java.util.stream.Stream;

/**
 * Centralized formats for image store.
 * 
 * @author Martin Lowe
 *
 */
public interface ImageStoreFormat {

    String getName();

    public enum ImageStoreFormats implements ImageStoreFormat {
        SMALL, LARGE, PRINT, WEB, WEB_SRC;

        @Override
        public String getName() {
            return this.name().toLowerCase();
        }

        public static ImageStoreFormat getFormat(String name) {
            if (name == null) {
                return WEB;
            }
            return Stream.of(values()).filter(v -> name.equalsIgnoreCase(v.getName())).findFirst().orElse(WEB);
        }
    }
}
