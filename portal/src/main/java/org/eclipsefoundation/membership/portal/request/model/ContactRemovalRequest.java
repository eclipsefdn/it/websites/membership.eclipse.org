/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.request.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ContactRemovalRequest.Builder.class)
public abstract class ContactRemovalRequest {

    public abstract String getUsername();

    public abstract Integer getOrganizationId();

    public abstract String getReason();

    public static Builder builder() {
        return new AutoValue_ContactRemovalRequest.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setUsername(String username);

        public abstract Builder setOrganizationId(Integer organizationID);

        public abstract Builder setReason(String reason);

        public abstract ContactRemovalRequest build();
    }
}
