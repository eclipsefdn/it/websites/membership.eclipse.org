/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard.filters;

import org.eclipsefoundation.membership.portal.dtos.dashboard.ProjectCompanyActivity;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedMap;

@Singleton
public class ProjectCompanyActivityFilter implements DtoFilter<ProjectCompanyActivity> {

    @Inject
    ParameterizedSQLStatementBuilder builder;

    @Override
    public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
        ParameterizedSQLStatement stmt = builder.build(ProjectCompanyActivity.TABLE);
        if (isRoot) {
            // project check
            String project = params.getFirst(MembershipPortalParameterNames.PROJECT.getName());
            if (project != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(
                        ProjectCompanyActivity.TABLE.getAlias() + ".compositeId.project.id = ?", new Object[] { project }));
            }
            // organization ID check
            String organizationID = params.getFirst(MembershipPortalParameterNames.ORGANIZATION_ID.getName());
            if (organizationID != null) {
                stmt.addClause(
                        new ParameterizedSQLStatement.Clause(ProjectCompanyActivity.TABLE.getAlias() + ".compositeId.orgId = ?",
                                new Object[] { Integer.valueOf(organizationID) }));
            }
        }
        String hasOrganization = params.getFirst(MembershipPortalParameterNames.HAS_ORGANIZATION.getName());
        if (Boolean.parseBoolean(hasOrganization)) {
            stmt.addClause(new ParameterizedSQLStatement.Clause(ProjectCompanyActivity.TABLE.getAlias() + ".compositeId.orgId is not null",
                    new Object[] {}));
        }
        return stmt;
    }

    @Override
    public Class<ProjectCompanyActivity> getType() {
        return ProjectCompanyActivity.class;
    }

}