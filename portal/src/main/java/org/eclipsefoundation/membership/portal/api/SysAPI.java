/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.vertx.http.Compressed;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("sys")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
@OidcClientFilter
public interface SysAPI {

    @GET
    @Path("relations")
    @Compressed
    RestResponse<List<SysRelationData>> getSysRelations(@BeanParam BaseAPIParameters baseParams, @QueryParam("type") String type);

    @PUT
    @Path("mod_logs")
    @Compressed
    List<SysModLogData> postModLog(SysModLogData modLog);
}
