/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.api;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.vertx.http.Compressed;
import jakarta.annotation.Nullable;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@OidcClientFilter
@Path("organizations")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface OrganizationAPI {

    @GET
    @Path("contacts")
    @Compressed
    RestResponse<List<OrganizationContactData>> getOrganizationContacts(@BeanParam BaseAPIParameters baseParams,
            @BeanParam OrganizationRequestParams params);

    @PUT
    @Path("{id}/contacts")
    @Compressed
    OrganizationContactData updateOrganizationContacts(@PathParam("id") String orgId, OrganizationContactData contact);

    @GET
    @Path("{id}/contacts/{personID}")
    @Compressed
    RestResponse<List<OrganizationContactData>> getOrganizationContact(@PathParam("id") String orgId,
            @PathParam("personID") String personID, @BeanParam BaseAPIParameters baseParams, @BeanParam OrganizationRequestParams params);

    @DELETE
    @Path("{id}/contacts/{personID}/{relation}")
    @Compressed
    RestResponse<Void> removeOrganizationContacts(@PathParam("id") String orgId, @PathParam("personID") String personID,
            @PathParam("relation") String relation);

    public class OrganizationRequestParams {

        @Nullable
        @QueryParam("id")
        public String id;

        @Nullable
        @QueryParam("personID")
        public String personId;

        @Nullable
        @QueryParam("email")
        public String email;

        @Nullable
        @QueryParam("fName")
        public String firstName;

        @Nullable
        @QueryParam("lName")
        public String lastName;

        @Nullable
        @QueryParam("relation")
        public String relation;

        @Nullable
        @QueryParam("working_group")
        public String workingGroup;

        @Nullable
        @QueryParam("is_not_expired")
        public Boolean isNotExpired;

        @Nullable
        @QueryParam("ids")
        public List<String> ids;

        @Nullable
        @QueryParam("documentIDs")
        public List<String> documentIds;

        @Nullable
        @QueryParam("people_ids")
        public List<String> peopleIds;

        @Nullable
        @QueryParam("levels")
        public List<String> levels;

        public OrganizationRequestParams() {
            this.isNotExpired = true;
        }

        public boolean isEmpty() {
            return allListsAreEmpty() && StringUtils.isAllBlank(email, firstName, lastName, id, personId, relation);
        }

        private boolean allListsAreEmpty() {
            return CollectionUtils.isEmpty(ids) && CollectionUtils.isEmpty(documentIds) && CollectionUtils.isEmpty(peopleIds)
                    && CollectionUtils.isEmpty(levels);
        }

        /**
         * Generate a parameter map from the current request, passing through any set parameters to the bean mapping.
         * 
         * @param w the wrapper for the current request.
         * @return a bean mapping with values populated from the current request
         */
        public OrganizationRequestParams populateFromWrap(RequestWrapper w) {
            // default levels for membership calls
            List<String> membershipLevels = w.getParams(FoundationDBParameterNames.LEVELS);
            this.personId = w.getFirstParam(FoundationDBParameterNames.USER_NAME).orElseGet(() -> null);
            this.id = w.getFirstParam(DefaultUrlParameterNames.ID).orElseGet(() -> null);
            this.relation = w.getFirstParam(FoundationDBParameterNames.RELATION).orElseGet(() -> null);
            this.email = w.getFirstParam(FoundationDBParameterNames.EMAIL).orElseGet(() -> null);
            this.workingGroup = w.getFirstParam(FoundationDBParameterNames.WORKING_GROUP).orElseGet(() -> null);
            this.documentIds = w.getParams(FoundationDBParameterNames.DOCUMENT_IDS);
            this.levels = membershipLevels != null && !membershipLevels.isEmpty() ? membershipLevels
                    : Arrays.asList("SD", "AP", "AS", "OHAP");
            this.ids = w.getParams(DefaultUrlParameterNames.IDS);
            return this;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPersonId() {
            return personId;
        }

        public void setPersonId(String personId) {
            this.personId = personId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getRelation() {
            return relation;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }

        public String getWorkingGroup() {
            return workingGroup;
        }

        public void setWorkingGroup(String workingGroup) {
            this.workingGroup = workingGroup;
        }

        public Boolean getIsNotExpired() {
            return isNotExpired;
        }

        public void setIsNotExpired(Boolean isNotExpired) {
            this.isNotExpired = isNotExpired;
        }

        public List<String> getIds() {
            return ids;
        }

        public void setIds(List<String> ids) {
            this.ids = ids;
        }

        public List<String> getDocumentIds() {
            return documentIds;
        }

        public void setDocumentIds(List<String> documentIds) {
            this.documentIds = documentIds;
        }

        public List<String> getPeopleIds() {
            return peopleIds;
        }

        public void setPeopleIds(List<String> peopleIds) {
            this.peopleIds = peopleIds;
        }

        public List<String> getLevels() {
            return levels;
        }

        public void setLevels(List<String> levels) {
            this.levels = levels;
        }
    }
}
