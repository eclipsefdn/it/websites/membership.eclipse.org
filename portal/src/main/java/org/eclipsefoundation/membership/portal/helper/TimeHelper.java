/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.helper;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * Centralize the retrieval of date/time objects to synchronize usage of timezones across potentially varied
 * environments.
 * 
 * @author Martin Lowe
 *
 */
public final class TimeHelper {

    /**
     * Return the current instant as a datetime object zoned to UTC.
     * 
     * @return the current localdatetime in UTC
     */
    public static ZonedDateTime now() {
        return ZonedDateTime.now(ZoneOffset.UTC);
    }

    /**
     * Gets the current epoch milli according to UTC
     * 
     * @return the current epoch milli in UTC
     */
    public static long getMillis() {
        return now().toInstant().toEpochMilli();
    }

    /**
     * Returns the epoch milli of the time passed with UTC assumed.
     * 
     * @param time the time object to retrieve epoch millis from
     * @return the epoch millis of the passed time object in UTC
     */
    public static long getMillis(ZonedDateTime time) {
        return time.toInstant().toEpochMilli();
    }

    private TimeHelper() {
    }
}
