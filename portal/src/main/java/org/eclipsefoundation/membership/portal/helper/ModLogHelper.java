/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.helper;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.eclipsefoundation.membership.portal.api.SysAPI;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Helps format modlogs to be consistent and then posts them to the FoundationDB SYS API.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class ModLogHelper {
    public static final Logger LOGGER = LoggerFactory.getLogger(ModLogHelper.class);

    private final SysAPI sysAPI;

    public ModLogHelper(@RestClient SysAPI sysAPI) {
        this.sysAPI = sysAPI;
    }

    public SysModLogData postDeleteModLog(SecurityIdentity ident, DtoTable type, String pk1, String pk2) {
        return postDeleteModLog(ident, type.getType().getSimpleName(), pk1, pk2);
    }

    public SysModLogData postUpdateModLog(SecurityIdentity ident, DtoTable type, String pk1, String pk2) {
        return postUpdateModLog(ident, type.getType().getSimpleName(), pk1, pk2);
    }

    public SysModLogData postInsertModLog(SecurityIdentity ident, DtoTable type, String pk1, String pk2) {
        return postInsertModLog(ident, type.getType().getSimpleName(), pk1, pk2);
    }

    public SysModLogData postDeleteModLog(SecurityIdentity ident, String type, String pk1, String pk2) {
        return postModLog(ident, type, "DELETE", pk1, pk2);
    }

    public SysModLogData postUpdateModLog(SecurityIdentity ident, String type, String pk1, String pk2) {
        return postModLog(ident, type, "UPDATE", pk1, pk2);
    }

    public SysModLogData postInsertModLog(SecurityIdentity ident, String type, String pk1, String pk2) {
        return postModLog(ident, type, "INSERT", pk1, pk2);
    }

    public SysModLogData postCustomModLog(SecurityIdentity ident, String type, String pk1, String pk2, String action) {
        return postModLog(ident, type, action, pk1, pk2);
    }

    /**
     * Handles writes to the modlog for the user.
     * 
     * @param ident the user triggering the update
     * @param type the name of the table that is being updated
     * @param operation the type of operation (e.g. UPDATE or DELETE)
     * @param pk1 the primary key of the object that was updated
     * @param pk2 optional, an additional primary key to help represent composite key objects
     * @return the updated entry in foundation DB (sans logID as that is not available to the RT)
     */
    private SysModLogData postModLog(SecurityIdentity ident, String type, String operation, String pk1, String pk2) {
        SysModLogData data = new SysModLogData(null, type, pk1, pk2 == null ? "" : pk2, operation, ident.getPrincipal().getName(),
                DateTimeHelper.now());
        LOGGER.trace("ModLog: {}", data);
        List<SysModLogData> r = sysAPI.postModLog(data);
        if (r == null || r.isEmpty()) {
            return null;
        }
        return r.get(0);
    }
}
