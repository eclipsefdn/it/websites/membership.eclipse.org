/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.views.EnhancedOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.views.MemberOrganizationData;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI.OrganizationRequestParams;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.vertx.http.Compressed;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

@Path("views")
@OidcClientFilter
@RegisterRestClient(configKey = "fdndb-api")
public interface ComplexAPI {

    @GET
    @Path("members")
    @Compressed
    RestResponse<List<MemberOrganizationData>> getMembers(@BeanParam BaseAPIParameters params,
            @BeanParam OrganizationRequestParams orgParams);

    @GET
    @Path("contacts")
    @Compressed
    RestResponse<List<EnhancedOrganizationContactData>> getContacts(@BeanParam BaseAPIParameters params, @QueryParam("organization_id") String organizationId,
            @QueryParam("person_id") String personId);
}
