/**
 * Copyright (c) 2022, 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.service;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.efservices.api.models.InterestGroup;

/**
 * Service used to interact with interest groups.
 */
public interface InterestGroupService {

    /**
     * Fetches a list of all current Interest Groups.
     * 
     * @return A {@link List} of {@link InterestGroup} entities or an empty list
     */
    List<InterestGroup> getAll();

    /**
     * Returns an {@link Optional} containing an {@link InterestGroup} entity if it
     * exists
     * 
     * @param igId The given interest group id
     * @return An {@link Optional} containing the desired {@link InterestGroup} data
     *         if it exists.
     */
    Optional<InterestGroup> getById(String igId);
}
