/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.model.reports;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 
 */
@JsonPropertyOrder(value = { "organizationId", "organizationName", "repEmail", "alternateRepEmail", "delegateEmail", "numberOfProjects",
        "numberOfCommitters", "numberOfCommitsQuarter", "numberOfCommitsYear" })
public record MemberHighlightReportItem(int organizationId, String organizationName, String repEmail, String alternateRepEmail,
        String delegateEmail, int numberOfProjects, long numberOfCommitters, int numberOfCommitsQuarter, int numberOfCommitsYear) {

}
