/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.service.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationLevel;
import org.eclipsefoundation.efservices.services.WorkingGroupService;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.views.EnhancedOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.views.MemberOrganizationData;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.portal.api.ComplexAPI;
import org.eclipsefoundation.membership.portal.api.FoundationDBParameterNames;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI.OrganizationRequestParams;
import org.eclipsefoundation.membership.portal.api.PeopleAPI;
import org.eclipsefoundation.membership.portal.api.PeopleAPI.PeopleRequestParams;
import org.eclipsefoundation.membership.portal.api.ProjectAPI;
import org.eclipsefoundation.membership.portal.dtos.eclipse.OrganizationInformation;
import org.eclipsefoundation.membership.portal.model.EnhancedMemberOrganization;
import org.eclipsefoundation.membership.portal.model.EnhancedMemberOrganizationBuilder;
import org.eclipsefoundation.membership.portal.model.EnhancedMemberOrganizationLevelBuilder;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.MemberOrganization;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecordMemberOrganizationBuilder;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecordMemberOrganizationDescriptionBuilder;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecordMemberOrganizationLevelBuilder;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecordMemberOrganizationLogosBuilder;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecordOrganizationWGPABuilder;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat.ImageStoreFormats;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.service.ImageStoreService;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.control.ActivateRequestContext;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Builds a list of working group definitions from the FoundationDB, making use of some data from the FoundationDB system API to retrieve
 * common values such as relation descriptions.
 * 
 * @author Martin Lowe
 */
@Startup
@ApplicationScoped
public class FoundationDBOrganizationService implements OrganizationsService {
    public static final Logger LOGGER = LoggerFactory.getLogger(FoundationDBOrganizationService.class);

    private static final String ALL_LIST_CACHE_KEY = "allEntries";

    // internal service beans
    @Inject
    DefaultHibernateDao eDBdao;
    @Inject
    FilterService filters;
    @Inject
    ImageStoreService imageService;
    @Inject
    WorkingGroupService wgService;
    @Inject
    CachingService cache;
    @Inject
    RequestWrapper wrapper;

    // api interaction beans
    @Inject
    APIMiddleware middleware;
    @RestClient
    ComplexAPI complexAPI;
    @RestClient
    OrganizationAPI orgAPI;
    @RestClient
    PeopleAPI peopleAPI;
    @RestClient
    ProjectAPI projectsAPI;

    /**
     * On construct, start the loading cache for the full list of member organizations. This is a large call that we want to be highly
     * available, as attempting to call it without a cache would result in timeouts.
     */
    @PostConstruct
    void init() {
        LOGGER.info("Starting init of cached organizations");
        generateHACacheEntries();
    }

    @ActivateRequestContext
    public void generateHACacheEntries() {
        try {
            get();
        } catch (Exception e) {
            LOGGER.error("Error while populating HA cache, performance may be impacted", e);
        }
    }

    @Override
    public List<MemberOrganization> get() {
        // retrieve the results from the base cache rather than the HA cache
        return cache
                .get(ALL_LIST_CACHE_KEY, getCacheParams(new OrganizationRequestParams().populateFromWrap(wrapper)),
                        MemberOrganization.class, () -> getCachedRawOrgs().stream().map(this::convertToMemberOrganization).toList())
                .data()
                .orElseGet(Collections::emptyList);
    }

    @Override
    public Optional<MemberOrganization> getByID(String id) {
        return cache.get(id, new MultivaluedHashMap<>(), MemberOrganization.class, () -> {
            OrganizationRequestParams requestParams = new OrganizationRequestParams();
            requestParams.id = id;
            List<MemberOrganizationData> orgs = middleware.getAll(p -> complexAPI.getMembers(p, requestParams));
            if (orgs.isEmpty()) {
                return null;
            }
            return convertToMemberOrganization(orgs.get(0));
        }).data();
    }

    @Override
    public List<MemberOrganization> getByIDs(List<String> ids) {
        OrganizationRequestParams requestParams = new OrganizationRequestParams();
        requestParams.ids = ids;
        return cache
                .get(ids.toString(), new MultivaluedHashMap<>(), MemberOrganization.class,
                        () -> middleware
                                .getAll(b -> complexAPI.getMembers(null, requestParams))
                                .stream()
                                .map(this::convertToMemberOrganization)
                                .collect(Collectors.toList()))
                .data()
                .orElse(Collections.emptyList());

    }

    @Override
    public List<EnhancedMemberOrganization> getEnhanced() {
        // retrieve the results from the base cache rather than the HA cache
        return cache
                .get(ALL_LIST_CACHE_KEY, getCacheParams(new OrganizationRequestParams().populateFromWrap(wrapper)),
                        EnhancedMemberOrganization.class,
                        () -> getCachedRawOrgs().stream().map(org -> convertToEnhancedMemberOrganization(org, get())).toList())
                .data()
                .orElseGet(Collections::emptyList);
    }

    @Override
    public Optional<List<OrganizationContactData>> getOrganizationContacts(String orgID, String userName) {
        OrganizationRequestParams requestParams = new OrganizationRequestParams();
        requestParams.isNotExpired = false;
        return cache
                .get(orgID, new MultivaluedHashMap<>(), OrganizationContactData.class,
                        () -> middleware.getAll(i -> orgAPI.getOrganizationContact(orgID, userName, i, requestParams)))
                .data();
    }

    @Override
    public Optional<List<OrganizationContactData>> getOrganizationContacts(String userName) {
        OrganizationRequestParams requestParams = new OrganizationRequestParams();
        requestParams.isNotExpired = false;
        requestParams.personId = userName;
        return cache
                .get(userName, new MultivaluedHashMap<>(), OrganizationContactData.class,
                        () -> middleware.getAll(i -> orgAPI.getOrganizationContacts(i, requestParams)))
                .data();
    }

    @Override
    public void removeOrganizationContact(String orgID, String userName, String role) {
        clearContactCaches(orgID, userName);
        orgAPI.removeOrganizationContacts(orgID, userName, role);
    }

    @Override
    public OrganizationContactData updateOrganizationContact(String orgID, OrganizationContactData orgContact) {
        clearContactCaches(orgID, orgContact.personID());
        return orgAPI.updateOrganizationContacts(orgID, orgContact);
    }

    @Override
    public Optional<List<PeopleData>> getCommittersForOrganization(String id) {
        MultivaluedMap<String, String> cacheParams = new MultivaluedHashMap<>();
        cacheParams.add(FoundationDBParameterNames.RELATION.getName(), "CM");
        cacheParams.add(FoundationDBParameterNames.ORGANIZATION_ID.getName(), id);
        PeopleRequestParams requestParams = new PeopleRequestParams();
        requestParams.projectRelation = "CM";
        requestParams.organizationId = id;
        return cache
                .get(id + "-cm", cacheParams, PeopleData.class, () -> middleware.getAll(i -> peopleAPI.getPeople(i, requestParams)))
                .data();
    }

    @Override
    public List<OrganizationalUserType> getUserAccessRoles(String orgID, String userName) {
        MultivaluedMap<String, String> cacheParams = new MultivaluedHashMap<>();
        cacheParams.add(MembershipPortalParameterNames.USER_ID.getName(), userName);

        OrganizationRequestParams requestParams = new OrganizationRequestParams();
        requestParams.isNotExpired = false;
        return cache
                .get(orgID + "-access", cacheParams, OrganizationContactData.class,
                        () -> middleware.getAll(i -> orgAPI.getOrganizationContact(orgID, userName, i, requestParams)))
                .data()
                .orElse(Collections.emptyList())
                .stream()
                .map(c -> OrganizationalUserType.valueOfChecked(c.relation()))
                .collect(Collectors.toList());
    }

    private List<MemberOrganizationData> getCachedRawOrgs() {
        OrganizationRequestParams params = new OrganizationRequestParams();
        params.populateFromWrap(wrapper);
        LOGGER.debug("Hitting general cache for organizations data");

        // retrieve the results from the base cache rather than the HA cache
        return cache
                .get(ALL_LIST_CACHE_KEY, getCacheParams(params), MemberOrganizationData.class,
                        () -> middleware.getAll(p -> complexAPI.getMembers(p, params)))
                .data()
                .orElseGet(Collections::emptyList);
    }

    private MultivaluedMap<String, String> getCacheParams(OrganizationRequestParams reqParams) {
        if (reqParams == null || reqParams.isEmpty()) {
            return new MultivaluedHashMap<>();
        }
        // generate the param map for the cache key
        MultivaluedMap<String, String> cacheParams = new MultivaluedHashMap<>();
        cacheParams.add(DefaultUrlParameterNames.PAGE.getName(), wrapper.getFirstParam(DefaultUrlParameterNames.PAGE).orElseGet(() -> "1"));
        cacheParams
                .add(DefaultUrlParameterNames.PAGESIZE.getName(),
                        wrapper.getFirstParam(DefaultUrlParameterNames.PAGESIZE).orElseGet(() -> "25"));
        cacheParams.add(FoundationDBParameterNames.WORKING_GROUP.getName(), reqParams.workingGroup);
        cacheParams.put(FoundationDBParameterNames.LEVELS.getName(), reqParams.levels);

        return cacheParams;
    }

    /**
     * Using SysRelation data and the passed organization, creates a member organization object that can be returned which includes multiple
     * tables of data.
     * 
     * @param org base organization of the member organization.
     * @return a member organization object containing additional contextual data about the org
     */
    public MemberOrganization convertToMemberOrganization(MemberOrganizationData org) {
        MemberOrganizationRecordMemberOrganizationBuilder out = MemberOrganizationRecordMemberOrganizationBuilder.builder();
        out.organizationID(org.organizationID());
        out.name(org.name());
        out.memberSince(org.memberSince());
        out.renewalDate(calculateRenewalDate(org.organizationID(), org.latestMembershipStart()));
        out
                .levels(org
                        .relations()
                        .stream()
                        .map(membership -> MemberOrganizationRecordMemberOrganizationLevelBuilder
                                .builder()
                                .level(membership.relation())
                                .description(membership.description())
                                .sortOrder(membership.sortOrder())
                                .build())
                        .collect(Collectors.toList()));

        // Get org information from eclipse db
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), Integer.toString(org.organizationID()));
        RDBMSQuery<OrganizationInformation> q = new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                filters.get(OrganizationInformation.class), params);
        q.setRoot(false);
        // retrieve and handle the org info data
        List<OrganizationInformation> r = eDBdao.get(q);
        if (!r.isEmpty()) {
            OrganizationInformation info = r.get(0);
            // retrieve the descriptions of the organization
            out
                    .description(MemberOrganizationRecordMemberOrganizationDescriptionBuilder
                            .builder()
                            .longDescription(info.getLongDescription())
                            .build());
            try {
                new URI(info.getCompanyUrl());
                out.website(info.getCompanyUrl());
            } catch (URISyntaxException e) {
                LOGGER.debug("Error while converting URL for organization '{}', leaving blank", info.getOrganizationID(), e);
            }
        }

        // retrieve the logos of the organization
        MemberOrganizationRecordMemberOrganizationLogosBuilder logos = MemberOrganizationRecordMemberOrganizationLogosBuilder.builder();
        logos.web(imageService.retrieveImageUrl(Integer.toString(org.organizationID()), Optional.of(ImageStoreFormats.WEB)));
        logos.print(imageService.retrieveImageUrl(Integer.toString(org.organizationID()), Optional.of(ImageStoreFormats.PRINT)));
        out.logos(logos.build());

        // get org WGPA documents and convert to model to be returned with extra context
        out.wgpas(org.documents().stream().map(wgpa -> {
            MemberOrganizationRecordOrganizationWGPABuilder owgpa = MemberOrganizationRecordOrganizationWGPABuilder.builder();
            owgpa.documentID(wgpa.documentID());
            owgpa.level(wgpa.relation());

            // find the WG that has a matching ID for current document
            Optional<WorkingGroup> wg = wgService.getByAgreementId(wgpa.documentID());
            // if this is missing then we have a document that wasn't id'd as a WGPA doc
            if (wg.isPresent()) {
                Optional<WorkingGroupParticipationLevel> pl = wg
                        .get()
                        .levels()
                        .stream()
                        .filter(l -> l.relation().equals(wgpa.relation()))
                        .findFirst();
                pl.ifPresent(wgpl -> owgpa.description(wgpl.description()));
                owgpa.workingGroup(wg.get().alias());
            } else {
                owgpa.workingGroup("unknown");
            }
            return owgpa.build();
        }).collect(Collectors.toList()));
        return out.build();
    }

    /**
     * Using the already converted member organizations for standard return, build the list of enhanced members that adds data from the
     * original request for dues data.
     * 
     * @param org original upstream organization being converted
     * @param baseOrgs list of standard converted member organizations, used to reduce duplication 
     * @return the enhanced member organization with dues information
     */
    private EnhancedMemberOrganization convertToEnhancedMemberOrganization(MemberOrganizationData org, List<MemberOrganization> baseOrgs) {
        // get converted base from pre-fetched list, attempting to fetch again if missing
        MemberOrganization memberOrg = baseOrgs
                .stream()
                .filter(o -> o.organizationID() == org.organizationID())
                .findFirst()
                .orElseGet(() -> convertToMemberOrganization(org));

        // convert the standard member org to the enhanced format
        return EnhancedMemberOrganizationBuilder
                .builder()
                .organizationID(memberOrg.organizationID())
                .name(memberOrg.name())
                .description(memberOrg.description())
                .levels(memberOrg.levels().stream().map(level -> {
                    var memberLvl = org
                            .relations()
                            .stream()
                            .filter(dataLvl -> dataLvl.relation().equalsIgnoreCase(level.level()))
                            .findFirst()
                            .orElse(null);
                    return EnhancedMemberOrganizationLevelBuilder
                            .builder()
                            .description(level.description())
                            .duesAmount(memberLvl != null ? memberLvl.duesAmount() : null)
                            .duesTier(memberLvl != null ? memberLvl.duesTier() : null)
                            .level(level.level())
                            .sortOrder(level.sortOrder())
                            .build();
                }).toList())
                .logos(memberOrg.logos())
                .memberSince(memberOrg.memberSince())
                .renewalDate(memberOrg.renewalDate())
                .website(memberOrg.website())
                .wgpas(memberOrg.wgpas())
                .build();
    }

    /**
     * Clears caches of contact data for the given org containing the updated user and role.
     * 
     * @param org the org that is being updated
     * @param userName the user whos entry is being updated
     */
    private void clearContactCaches(String org, String userName) {
        // clear the org contacts that contain the removed user entry
        cache.fuzzyRemove(org, OrganizationContactData.class);
        cache.fuzzyRemove(userName, OrganizationContactData.class);
        cache.fuzzyRemove(org + "-cm", PeopleData.class);
        cache.fuzzyRemove(org + "-access", OrganizationContactData.class);
        cache.fuzzyRemove(org, EnhancedOrganizationContactData.class);
    }

    /**
     * Calculates the member renewal date by comparing the provided membership date with today's date.
     * 
     * @param orgId The org id for which the renewal is calculated
     * @param membershipDate The given membershp date to use in the calculation
     * @return The membership date with the year updated to this year or next year, depending on the day.
     */
    private LocalDate calculateRenewalDate(int orgId, LocalDate membershipDate) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("Calculating renewal date for org '{}' with date '{}'", orgId, membershipDate);
        }

        LocalDate now = LocalDate.now();
        int todayDayOfYear = now.getDayOfYear();
        int membershipDayOfYear = membershipDate.getDayOfYear();

        // If day is after today(or today), renewal is set to this year
        if (membershipDayOfYear >= todayDayOfYear) {
            return membershipDate.withYear(now.getYear());
        }

        // If the day is before today, we set the date to next year
        return membershipDate.withYear(now.getYear() + 1);
    }
}
