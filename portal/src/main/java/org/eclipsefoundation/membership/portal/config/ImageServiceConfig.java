/**
 * Copyright (c) 2022, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.config;

import jakarta.validation.constraints.Max;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Default required configurations for service that should be configured and used in implementations of the service for
 * baseline functionality.
 * 
 * @author Martin Lowe
 *
 */
@ConfigMapping(prefix = "eclipse.image-store")
public interface ImageServiceConfig {
    String filePath();

    String webRoot();

    @WithDefault("")
    String defaultImageUrl();

    MaxSizeInBytes maxSizeInBytes();

    Compression compression();

    @WithDefault("false")
    boolean persistToDb();

    @WithDefault("false")
    boolean legacyMigrationEnabled();

    /**
     * Contains the properties regarding max size of saved assets
     * 
     * @author Martin Lowe
     *
     */
    interface MaxSizeInBytes {
        @WithDefault("1048576")
        long web();

        @WithDefault("64000")
        long webPostCompression();

        @WithDefault("10485760")
        long print();
    }

    /**
     * Represents compression configuration settings.
     * 
     * @author Martin Lowe
     *
     */
    interface Compression {
        @WithDefault("true")
        boolean enabled();

        @WithDefault("200")
        int maxDimension();

        @WithDefault("0.80f")
        @Max(1)
        float factor();

        @WithDefault("20000")
        long thresholdInBytes();
    }
}