/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.model;

import java.time.LocalDate;
import java.util.List;

import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.MemberOrganizationDescription;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.MemberOrganizationLogos;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.OrganizationWGPA;

import io.soabase.recordbuilder.core.RecordBuilder;
import jakarta.annotation.Nullable;

/**
 * 
 */
@RecordBuilder
public record EnhancedMemberOrganization(int organizationID, String name, @Nullable LocalDate memberSince, @Nullable LocalDate renewalDate,
        @Nullable MemberOrganizationDescription description, @Nullable MemberOrganizationLogos logos, @Nullable String website,
        @Nullable List<Level> levels, List<OrganizationWGPA> wgpas) {

    /**
     * Level needs an override to add new dues fields
     */
    @RecordBuilder
    public record Level(@Nullable String level, @Nullable String description, @Nullable String sortOrder, @Nullable Integer duesTier,
            @Nullable Integer duesAmount) {
    }

}
