/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.membership.portal.api.ProjectAPI;
import org.eclipsefoundation.membership.portal.daos.DashboardPersistenceDAO;
import org.eclipsefoundation.membership.portal.dtos.dashboard.ProjectCompanyActivity;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.MemberOrganization;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.eclipsefoundation.membership.portal.service.ProjectsService;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

@ApplicationScoped
public class DefaultProjectsService implements ProjectsService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultProjectsService.class);

    private final DashboardPersistenceDAO dashDao;
    private final CachingService cache;
    private final FilterService filters;
    private final OrganizationsService orgService;

    private final APIMiddleware middleware;
    private final ProjectAPI projectsAPI;

    public DefaultProjectsService(DashboardPersistenceDAO dashDao, CachingService cache, FilterService filters,
            OrganizationsService orgService, APIMiddleware middleware, @RestClient ProjectAPI projectsAPI) {
        this.dashDao = dashDao;
        this.cache = cache;
        this.filters = filters;
        this.orgService = orgService;
        this.middleware = middleware;
        this.projectsAPI = projectsAPI;
    }

    @Override
    public List<MemberOrganization> getOrganizationsForProject(String projectId, RequestWrapper wrap) {
        // retrieve activity for a given project within last 3 months
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MembershipPortalParameterNames.PROJECT.getName(), projectId);
        params.add(MembershipPortalParameterNames.HAS_ORGANIZATION.getName(), "true");

        // Use cache to fetch batched results
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Looking up organizations for project '{}'", TransformationHelper.formatLog(projectId));
        }

        return cache
                .get(projectId, params, MemberOrganization.class, () -> fetchActiveMemberOrgsForProject(projectId, wrap, params))
                .data()
                .orElse(Collections.emptyList());
    }

    @Override
    public List<ProjectData> getProjectsForOrganization(String organizationId, RequestWrapper wrap) {
        return cache
                .get(organizationId, new MultivaluedHashMap<>(), ProjectData.class,
                        () -> middleware.getAll(i -> projectsAPI.getProjects(i, organizationId)))
                .data()
                .orElse(Collections.emptyList());
    }

    @Override
    public boolean projectExists(String projectId) {
        Optional<ProjectData> project = cache
                .get(projectId, new MultivaluedHashMap<>(), ProjectData.class,
                        () -> projectsAPI.getProject(new BaseAPIParameters(), projectId))
                .data();
        return project.isPresent();
    }

    /**
     * Fetches all member organizations for a given project as long as they have activity within the last 3 months.
     * 
     * @param projectId The current project
     * @param wrap The request wrapper.
     * @param params The params used for the DB query.
     * @return A list of MemberOrgs or an empty list if no activity was found for the given project.
     */
    private List<MemberOrganization> fetchActiveMemberOrgsForProject(String projectId, RequestWrapper wrap,
            MultivaluedMap<String, String> params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching fresh organization data for project {}", TransformationHelper.formatLog(projectId));
        }

        RDBMSQuery<ProjectCompanyActivity> q = new RDBMSQuery<>(wrap, filters.get(ProjectCompanyActivity.class), params);
        q.setUseLimit(false);
        // retrieve activity for a given project
        List<ProjectCompanyActivity> activity = dashDao.get(q);
        if (activity.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Found no activity associated with project '{}'", TransformationHelper.formatLog(projectId));
            }
            return Collections.emptyList();
        }

        // convert the stream activity to the contributing org, dropping any
        // contributions w/o org and deduplicating
        List<String> organizations = activity
                .stream()
                .map(a -> Integer.toString(a.getCompositeId().getOrgId()))
                .distinct()
                .collect(Collectors.toList());

        if (LOGGER.isDebugEnabled()) {
            LOGGER
                    .debug("Found {} organizations associated with project {}, fetching actual org data", organizations.size(),
                            TransformationHelper.formatLog(projectId));
        }
        // fetch member orgs, and shuffle results for output
        List<MemberOrganization> memberOrgs = new ArrayList<>(orgService.getByIDs(organizations));
        Collections.shuffle(memberOrgs);
        return memberOrgs;
    }
}
