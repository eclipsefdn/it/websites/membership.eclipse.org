/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.views.EnhancedOrganizationContactData;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.membership.portal.api.ComplexAPI;
import org.eclipsefoundation.membership.portal.api.PeopleAPI;
import org.eclipsefoundation.membership.portal.daos.DashboardPersistenceDAO;
import org.eclipsefoundation.membership.portal.dtos.dashboard.CommitterProjectActivity;
import org.eclipsefoundation.membership.portal.helper.ModLogHelper;
import org.eclipsefoundation.membership.portal.helper.UserAccessHelper;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.model.OrganizationActivity;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.CSRFHelper;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.eclipsefoundation.utils.model.AdditionalUserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Provides access to commonly required services and containers for REST request serving.
 *
 * @author Martin Lowe
 */
public abstract class AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractRESTResource.class);
    public static final String ALL_CACHE_PLACEHOLDER = "all";

    private static final int MONTHS_IN_A_YEAR = 12;
    private static final Pattern PERIOD_DATE_FORMAT = Pattern.compile("^[\\d]{4}(0[\\d]|1[0-2])$");

    @Inject
    FilterService filters;
    @Inject
    CachingService cache;

    @Inject
    RequestWrapper wrap;
    @Inject
    DashboardPersistenceDAO dashboardDao;

    @Inject
    CSRFHelper csrfHelper;
    @Inject
    ModLogHelper modLogHelper;
    @Inject
    AdditionalUserData aud;
    @Inject
    SecurityIdentity ident;
    @Inject
    UserAccessHelper accessHelper;

    @Inject
    OrganizationsService orgService;

    @RestClient
    @Inject
    PeopleAPI peopleAPI;
    @RestClient
    @Inject
    ComplexAPI complexAPI;
    @Inject
    APIMiddleware middle;

    /**
     * Builds a list of people data that includes name and relations for the users associated with the passed organization.
     * 
     * @param organizationID the organization to fetch users for
     * @param relations the relations that should be filtered to, if any
     * @return the list of people, filtered by relation type if provided.
     */
    protected List<EnhancedPersonData> getEnhancedPeopleDetails(String organizationID, List<OrganizationalUserType> userTypes) {
        return getEnhancedPeopleDetails(organizationID, userTypes, accessHelper.userCanViewProtectedField(organizationID));
    }

    /**
     * Builds a list of people data that includes name and relations for the users associated with the passed organization. Allows for
     * bypass of organization role check for sensitive fields, so should be used with caution.
     * 
     * @param organizationID the organization to fetch users for
     * @param relations the relations that should be filtered to, if any
     * @param canViewProtectedField whether sensitive fields like email are returned in output
     * @return the list of people, filtered by relation type if provided.
     */
    protected List<EnhancedPersonData> getEnhancedPeopleDetails(String organizationID, List<OrganizationalUserType> userTypes,
            boolean canViewProtectedField) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching org contacts for org '{}'", TransformationHelper.formatLog(organizationID));
        }

        // get the cached contacts data
        Optional<List<EnhancedOrganizationContactData>> ocs = cache
                .get(organizationID, null, EnhancedOrganizationContactData.class,
                        () -> middle.getAll(p -> complexAPI.getContacts(p, organizationID, null)))
                .data();
        if (ocs.isEmpty()) {
            return Collections.emptyList();
        }

        // get a reusable list for easier comparisons, null checking the param as well
        List<String> userTypeRaw = userTypes == null ? Collections.emptyList()
                : userTypes.stream().map(OrganizationalUserType::name).toList();
        // convert the contact data to a more usable format
        return ocs.get().stream().map(p -> {
            LOGGER
                    .debug("Converting '{}' from org '{}' to EnhancedPersonData", p.person().personID(),
                            TransformationHelper.formatLog(organizationID));

            // only expose email if user has elevated permissions
            return EnhancedPersonData
                    .builder()
                    .setEmail(canViewProtectedField ? p.person().email() : null)
                    .setFirstName(p.person().fname())
                    .setLastName(p.person().lname())
                    .setId(p.person().personID())
                    .setRelations(getActualRelationsForContact(p.relations(), userTypes, p.committer()))
                    .build();
        }).filter(contact -> userTypeRaw.isEmpty() || contact.getRelations().stream().anyMatch(userTypeRaw::contains)).toList();
    }

    protected List<OrganizationActivity> getCommitActivityForOrg(String organizationID, Optional<LocalDate> fromPeriod) {
        LocalDate d = LocalDate.now();
        // get the previous year of results
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, null,
                getPeriodStringForDate(fromPeriod.orElse(d.minusYears(1))));

        // group results by the activity period, then reduce the count into a single int
        // result, and transform into an output.
        List<OrganizationActivity> activityList = orgCountryActivity
                .stream()
                .collect(Collectors.groupingBy((CommitterProjectActivity cpa) -> cpa.getCompositeID().getPeriod()))
                .entrySet()
                .stream()
                .map(e -> OrganizationActivity
                        .builder()
                        .setPeriod(e.getKey())
                        .setCount(e.getValue().stream().reduce(0, (sub, el) -> sub + el.getCount(), Integer::sum))
                        .build())
                .collect(Collectors.toList());
        // backfill activity log for 1 year
        activityList = backfillHistoricActivityData(activityList, d);
        // sort the period, sorting from greatest (most recent) to smallest (least
        // recent)
        activityList.sort((o1, o2) -> o2.getPeriod().compareTo(o1.getPeriod()));
        return activityList;
    }

    /**
     * Retrieves all commit activity count entries for an organization, either for the given period, or from the second passed period.
     * 
     * @param organizationID organization to retrieve entries for
     * @param period the singular period to retrieve entries for (optional)
     * @param fromPeriod the starting period to retrieve entries from (optional)
     * @return list of committer activity entries.
     */
    protected List<CommitterProjectActivity> getCommitterActivity(String organizationID, String period, String fromPeriod) {
        // set up params for current call
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        // get the period parameter in use (period and from_period are not compatible
        // with each other)
        String paramName = determinePeriodParameterName(period, fromPeriod);
        if (paramName.equals(MembershipPortalParameterNames.FROM_PERIOD.getName())) {
            params.add(paramName, checkPeriodOrDefault(fromPeriod));
        } else {
            params.add(paramName, checkPeriodOrDefault(period));
        }
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), organizationID);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Retrieving organization activity with params {}", params);
        }

        RDBMSQuery<CommitterProjectActivity> q = new RDBMSQuery<>(wrap, filters.get(CommitterProjectActivity.class), params);
        q.setUseLimit(false);
        return dashboardDao.get(q);
    }

    protected String getPeriodStringForDate(LocalDate d) {
        // check that month is 2 characters
        String month = Integer.toString(d.getMonthValue());
        if (month.length() == 1) {
            month = "0" + month;
        }
        return Integer.toString(d.getYear()) + month;
    }

    /**
     * Determine the parameter name based on validity of period values, using the below priority:
     * 
     * period > from period > default (single period, this month)
     * 
     * @param period the proposed period value
     * @param fromPeriod the proposed from period value
     * @return either the {@linkplain MembershipPortalParameterNames.PERIOD} or {@linkplain MembershipPortalParameterNames.FROM_PERIOD}
     * parameter names.
     */
    protected String determinePeriodParameterName(String period, String fromPeriod) {
        if (isPeriodFormatValid(period)) {
            return MembershipPortalParameterNames.PERIOD.getName();
        } else if (isPeriodFormatValid(fromPeriod)) {
            return MembershipPortalParameterNames.FROM_PERIOD.getName();
        } else {
            return MembershipPortalParameterNames.PERIOD.getName();
        }
    }

    /**
     * Provides a checked period or the default, which is the current month
     * 
     * @param period the proposed time that should be used. Accurate to the month, in the format YYYYMM
     * @return the passed parameter if valid, otherwise returns default value.
     */
    protected String checkPeriodOrDefault(String period) {
        return isPeriodFormatValid(period) ? period : getPeriodStringForDate(LocalDate.now());
    }

    protected List<OrganizationActivity> backfillHistoricActivityData(List<OrganizationActivity> base, LocalDate from) {
        List<OrganizationActivity> activityList = new ArrayList<>(base);
        LocalDate d = from;
        for (int i = 0; i < MONTHS_IN_A_YEAR; i++) {
            // get current period string and update date string
            String currentPeriodString = getPeriodStringForDate(d);
            d = d.minus(1, ChronoUnit.MONTHS);

            // check if we need to back fill this period
            if (activityList.stream().noneMatch(oa -> oa.getPeriod().equals(currentPeriodString))) {
                activityList.add(OrganizationActivity.builder().setCount(0).setPeriod(currentPeriodString).build());
            }
        }
        return activityList;
    }

    /**
     * Converts all person's relations to a List of Strings and adds committer relation to list of relations if the user is a committer.
     * 
     * @param userRelations The person's SysRelationData records
     * @param userTypes The list of OrganizationalUserTypes mapped to this user
     * @param isCommitter The commmitter flag
     * @return The list of relations
     */
    private List<String> getActualRelationsForContact(List<SysRelationData> userRelations, List<OrganizationalUserType> userTypes,
            boolean isCommitter) {
        List<String> actualRels = userRelations
                .stream()
                .map(SysRelationData::relation)
                .filter(r -> userTypes.contains(OrganizationalUserType.valueOfChecked(r)))
                .collect(Collectors.toList());
        if (isCommitter) {
            actualRels.add("CM");
        }
        return actualRels;
    }

    private boolean isPeriodFormatValid(String proposedPeriod) {
        return StringUtils.isNotBlank(proposedPeriod) && PERIOD_DATE_FORMAT.matcher(proposedPeriod).matches();
    }
}
