/**
 * Copyright (c) 2022, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CR;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CRA;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.DE;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CM;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;
import org.eclipsefoundation.membership.portal.daos.REMPersistenceDAO;
import org.eclipsefoundation.membership.portal.dtos.rem.Sessions;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.MemberOrganization;
import org.eclipsefoundation.membership.portal.model.OrganizationActivity;
import org.eclipsefoundation.membership.portal.model.reports.MemberAdoptionReportItem;
import org.eclipsefoundation.membership.portal.model.reports.MemberHighlightReportItem;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.service.ProjectsService;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

@Path("reports")
@Authenticated
public class ReportsResource extends AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(ReportsResource.class);

    private final SimpleDateFormat simpleISODateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static final int SIX_MONTHS_AS_DAYS = 183;
    private static final String INVALID_DATE_MSG_FORMAT = "The provided date: '%s' is not a valid date. Expected format: 'yyyy-MM-dd'";
    private static final String TEXT_CSV_TYPE = "text/csv";

    @ConfigProperty(name = "eclipse.security.reports.allowed-users")
    List<String> allowedUsers;

    @Inject
    REMPersistenceDAO dao;
    @Inject
    ProjectsService projectsService;

    private static final CsvMapper CSV_MAPPER = (CsvMapper) new CsvMapper()
            .registerModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    @GET
    @Path("adoption_rate")
    @Produces(TEXT_CSV_TYPE)
    public Response getMemberAdoptionReport(@QueryParam("since") String since) throws IOException {
        // check that the user is in the allowed user list
        if (!allowedUsers.contains(ident.getPrincipal().getName())) {
            return Response.status(Response.Status.FORBIDDEN.getStatusCode()).build();
        }

        // Validate user input and generate search params
        MultivaluedMap<String, String> validatedParams = validateAndBuildSearchParams(since);

        try (StringWriter sw = new StringWriter()) {
            // create the CSV writer for membership forms
            SequenceWriter writer = CSV_MAPPER.writer(CSV_MAPPER.schemaFor(MemberAdoptionReportItem.class).withHeader()).writeValues(sw);

            // Fetch all sessions using the provided params and date string
            List<MemberAdoptionReportItem> out = getAdoptionReport(since, validatedParams);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Found {} report items for date '{}': {}", out.size(), TransformationHelper.formatLog(since), out);
            }

            writer.writeAll(out);

            // Allows the file to auto-download as an attachment
            String reportDate = simpleISODateFormat.format(Date.from(Instant.now()));
            String dispositionHeader = "attachment; filename=" + reportDate + "-portal-session-report.csv";

            return Response.ok(sw.toString()).header(HttpHeaders.CONTENT_DISPOSITION, dispositionHeader).build();
        }
    }

    @GET
    @Path("member-highlights")
    @Produces(TEXT_CSV_TYPE)
    public Response getMembershipHighlights() throws IOException {
        // check that the user is in the allowed user list
        if (!allowedUsers.contains(ident.getPrincipal().getName())) {
            return Response.status(Response.Status.FORBIDDEN.getStatusCode()).build();
        }

        try (StringWriter sw = new StringWriter()) {
            // create the CSV writer for membership forms
            SequenceWriter writer = CSV_MAPPER.writer(CSV_MAPPER.schemaFor(MemberHighlightReportItem.class).withHeader()).writeValues(sw);

            // Fetch all sessions using the provided params and date string
            List<MemberHighlightReportItem> out = getHighlights();

            writer.writeAll(out);

            // Allows the file to auto-download as an attachment
            String reportDate = simpleISODateFormat.format(Date.from(Instant.now()));
            String dispositionHeader = "attachment; filename=" + reportDate + "-member-highlights-report.csv";

            return Response.ok(sw.toString()).header(HttpHeaders.CONTENT_DISPOSITION, dispositionHeader).build();
        }
    }

    @GET
    @Path("member-highlights/{orgId}")
    public Response getMembershipHighlight(@PathParam("orgId") String orgId) throws IOException {
        // check that the user is in the allowed user list
        if (!allowedUsers.contains(ident.getPrincipal().getName())) {
            return Response.status(Response.Status.FORBIDDEN.getStatusCode()).build();
        }
        if (!StringUtils.isNumeric(orgId)) {
            throw new BadRequestException("Passed organization ID must be a number");
        }
        MemberOrganization org = orgService
                .getByID(orgId)
                .orElseThrow(() -> new NotFoundException("Could not find an organization with passed ID"));

        return Response.ok(getHighlight(org)).build();
    }

    /**
     * Builds the highlight report for all active organizations.
     * 
     * @return list of highlights for all active organizations
     */
    private List<MemberHighlightReportItem> getHighlights() {
        // get all orgs, and calculate the highlights for each
        return orgService.get().stream().map(this::getHighlight).toList();
    }

    /**
     * Get a membership highlight for the passed organization.
     *
     * @param o organization that should be used to generate a highlight report
     * @return highlight report for the given organization
     */
    private MemberHighlightReportItem getHighlight(MemberOrganization o) {
        return cache
                .get(Integer.toString(o.organizationID()), null, MemberHighlightReportItem.class, () -> getRawHighlight(o))
                .data()
                .orElse(null);
    }

    private MemberHighlightReportItem getRawHighlight(MemberOrganization o) {
        // get the list of representatives for the current org
        String orgId = Integer.toString(o.organizationID());
        List<EnhancedPersonData> reps = getEnhancedPeopleDetails(orgId, Arrays.asList(CR, CRA, DE, CM), true);
        // from the above list, extract people who are committers
        Stream<EnhancedPersonData> committers = getUsersByRelation(reps, CM);

        // get the projects by organization
        List<ProjectData> projects = projectsService.getProjectsForOrganization(orgId, wrap);

        // get the commits per organization
        List<OrganizationActivity> orgYearCommitterActivity = getCommitActivityForOrg(orgId, Optional.empty());
        // since list is organized by latest months to furthest, grab 3 most recent months
        List<OrganizationActivity> orgQuarterCommitterActivity = orgYearCommitterActivity.subList(0, 3);

        return new MemberHighlightReportItem(o.organizationID(), o.name(), getMailsByRelation(reps, CR), getMailsByRelation(reps, CRA),
                getMailsByRelation(reps, DE), projects.size(), committers.map(EnhancedPersonData::getId).distinct().count(),
                orgQuarterCommitterActivity.stream().map(a -> a.getCount()).reduce((o1, o2) -> o1 + o2).orElse(0),
                orgYearCommitterActivity.stream().map(a -> a.getCount()).reduce((o1, o2) -> o1 + o2).orElse(0));
    }

    private String getMailsByRelation(List<EnhancedPersonData> reps, OrganizationalUserType relation) {
        return StringUtils.join(getUsersByRelation(reps, relation).map(EnhancedPersonData::getEmail).toList(), ",");
    }

    private Stream<EnhancedPersonData> getUsersByRelation(List<EnhancedPersonData> users, OrganizationalUserType relation) {
        return users.stream().filter(p -> p.getRelations().stream().anyMatch(rel -> rel.equalsIgnoreCase(relation.name())));
    }

    /**
     * Retrieves the desired MemberAdoptionReportItem entities, filtered by the given params.
     * 
     * @param since The user provided input. Used as a cache key if it exists.
     * @param params The given params used for caching and querying DB results
     * @return A List of all desired MemberAdoptionReportItem if they exist.
     */
    private List<MemberAdoptionReportItem> getAdoptionReport(String since, MultivaluedMap<String, String> params) {
        String key = StringUtils.isNotBlank(since) ? since : "default";
        Optional<List<MemberAdoptionReportItem>> report = cache
                .get(key, params, MemberAdoptionReportItem.class, () -> {
                    RDBMSQuery<Sessions> query = new RDBMSQuery<>(wrap, filters.get(Sessions.class), params);
                    query.setUseLimit(false);
                    return dao.get(query).stream().map(this::mapSessiontoReportItem).toList();
                })
                .data();

        if (report.isEmpty() || report.get().isEmpty()) {
            return Collections.emptyList();
        }

        return report.get();
    }

    /**
     * Maps a Sessions entity to a MemberAdoptionReportItem. This replaces the orgId with the org name.
     * Used to allow more legibility to the report.
     * 
     * @param session The given Sessions entity to map
     * @return A mapped MemberAdoptionReportItem
     */
    private MemberAdoptionReportItem mapSessiontoReportItem(Sessions session) {
        // Pull member org and use the name if it exists
        Optional<MemberOrganization> memberOrg = Optional.empty();
        if (session.getOrganization() != null) {
            memberOrg = orgService.getByID(session.getOrganization().toString());
        }
        return new MemberAdoptionReportItem((int) session.getId(),
                memberOrg.isPresent() ? memberOrg.get().name() : null, session.getUsername(), session.getTime());
    }

    /**
     * Validates the provided date string by attempting to parse it as a Date object and builds a param Map containing the date argument. If
     * the provided date is null or is after today, a default time-slice of 6 months is used. Throws a BadRequestException if there is a
     * failure while parsing the date.
     * 
     * @param since The user provided date string
     * @return A populated Map containing a valid date filter value
     */
    private MultivaluedMap<String, String> validateAndBuildSearchParams(String since) {
        try {
            MultivaluedMap<String, String> params = new MultivaluedHashMap<>();

            // If the date provided is valid and before today, use it
            if (StringUtils.isNotBlank(since) && simpleISODateFormat.parse(since).before(new Date())) {
                params.add(MembershipPortalParameterNames.SINCE.getName(), since);
            } else {
                // If a date is not provided or is invalid, the default slice is 6 months ago
                // Instant.minus() does not support ChronoUnit.MONTHS :(
                params
                        .add(MembershipPortalParameterNames.SINCE.getName(),
                                simpleISODateFormat.format(Date.from(Instant.now().minus(SIX_MONTHS_AS_DAYS, ChronoUnit.DAYS))));
            }

            return params;
        } catch (ParseException e) {
            throw new BadRequestException(String.format(INVALID_DATE_MSG_FORMAT, since), e);
        }
    }
}
