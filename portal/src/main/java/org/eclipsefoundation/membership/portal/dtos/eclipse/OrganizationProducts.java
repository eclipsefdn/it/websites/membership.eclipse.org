/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.eclipse;

import java.util.Objects;

import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

@Table
@Entity
public class OrganizationProducts extends BareNode {
    public static final DtoTable TABLE = new DtoTable(OrganizationProducts.class, "op");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer productId;
    private Integer organizationId;
    private String name;
    @Column(length = 700)
    private String description;
    @Column(name = "product_url")
    private String productUrl;

    @Override
    public Object getId() {
        return getProductId();
    }

    /**
     * @return the productId
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * @return the organizationId
     */
    public Integer getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the productUrl
     */
    public String getProductUrl() {
        return productUrl;
    }

    /**
     * @param productUrl the productUrl to set
     */
    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(description, name, organizationId, productId, productUrl);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        OrganizationProducts other = (OrganizationProducts) obj;
        return Objects.equals(description, other.description) && Objects.equals(name, other.name)
                && Objects.equals(organizationId, other.organizationId) && Objects.equals(productId, other.productId)
                && Objects.equals(productUrl, other.productUrl);
    }

    @Override
    public String toString() {
        return "OrganizationProducts [productId=" + productId + ", organizationId=" + organizationId + ", name=" + name + ", description="
                + description + ", productUrl=" + productUrl + "]";
    }

    @Singleton
    public static class OrganizationProductsFilter implements DtoFilter<OrganizationProducts> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // organization ID check
                String orgId = params.getFirst(MembershipPortalParameterNames.ORGANIZATION_ID.getName());
                if (orgId != null) {
                    stmt
                            .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".organizationId = ?",
                                    new Object[] { Integer.valueOf(orgId) }));
                }
                // product ID check
                String productId = params.getFirst(MembershipPortalParameterNames.PRODUCT_ID.getName());
                if (productId != null) {
                    stmt
                            .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".productId = ?",
                                    new Object[] { Integer.valueOf(productId) }));
                }
            }

            return stmt;
        }

        @Override
        public Class<OrganizationProducts> getType() {
            return OrganizationProducts.class;
        }
    }
}
