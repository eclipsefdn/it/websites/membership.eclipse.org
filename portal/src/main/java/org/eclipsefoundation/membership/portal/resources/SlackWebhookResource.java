/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CR;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CRA;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.valueOfChecked;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.services.WorkingGroupService;
import org.eclipsefoundation.http.annotations.Csrf;
import org.eclipsefoundation.membership.portal.api.SlackAPI;
import org.eclipsefoundation.membership.portal.config.SlackConfig;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.MemberOrganization;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.MemberOrganizationLevel;
import org.eclipsefoundation.membership.portal.model.MemberOrganizationRecord.OrganizationWGPA;
import org.eclipsefoundation.membership.portal.model.SlackAttachment;
import org.eclipsefoundation.membership.portal.model.SlackResponse;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/webhook/slack")
@Produces({ MediaType.APPLICATION_JSON })
public class SlackWebhookResource extends AbstractRESTResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(SlackWebhookResource.class);

    private static final String MEMBERSHIP_PAGE_URL = "https://www.eclipse.org/membership/show-member/?member_id=";
    private static final String PROFILE_PAGE_URL = "https://accounts.eclipse.org/users/";
    private static final String TEXT_KEY = "text";
    private static final String RESPONSE_URL_KEY = "response_url";

    private static final int MIN_TERM_LENGTH = 3;

    @Inject
    SlackConfig config;

    @RestClient
    SlackAPI slackAPI;

    @Inject
    WorkingGroupService wgService;

    @Inject
    ManagedExecutor executor;

    /**
     * POST endpoint with CSRF disabled that accepts incoming 'slash-commands' from slack. Consumes 'application/x-www-form-urlencoded'
     * format. To ensure responsiveness, returns a 200 OK Response to Slack after triggering the member search in a separate thread.
     * 
     * @param body The body is kept as a string to allow for proper validation of content.
     * @return A 200 OK Response containing an error message or a confirmation message.
     */
    @POST
    @Path("m")
    @Csrf(enabled = false)
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    public Response searchMemberOrg(String body) {

        LOGGER.debug("INCOMING BODY: {}", body);

        // Ensure request came from Slack and is valid
        validateRequestIntegrity(body);

        Map<String, String> requestParams = convertBodyToMap(body);
        String org = requestParams.get(TEXT_KEY);

        // Enforce a minimum character limit on the filter when searching by name
        if (!StringUtils.isNumeric(org) && StringUtils.isBlank(org) || org.length() < MIN_TERM_LENGTH) {
            // Slack wants a 200 OK
            return Response.ok(buildBadRequestResponse(org)).build();
        }

        // Ensure response_url is present
        String responseUrl = requestParams.get(RESPONSE_URL_KEY);
        if (StringUtils.isBlank(responseUrl)) {
            return Response.ok(buildInvalidUrlResponse()).build();
        }

        // Extract dynamic path from response_url param and decode
        String decodedPath = decodeURLSegment(StringUtils.substringAfter(responseUrl, "commands"));
        if (StringUtils.isBlank(decodedPath)) {
            return Response.ok(buildInvalidUrlResponse()).build();
        }

        // Ensure orgs are present to propagate to the lookup thread
        List<MemberOrganization> allOrgs = orgService.get();
        if (allOrgs.isEmpty()) {
            return Response
                    .ok(new SlackResponse(config.responseType(),
                            "There is an error fetching Member Organizations. Please contact the webdev team.", Collections.emptyList()))
                    .build();
        }

        // Trigger the search Task and send ok to Slack.
        executor.execute(() -> performMemberSearch(org, decodedPath, allOrgs));
        return Response.ok().build();
    }

    /**
     * Performs the search for the desired member org, builds the response object with various member org data points, then posts the
     * results to the SlackAPI at the provided path.
     * 
     * https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org/-/issues/645. We're passing in all orgs to ensure they are
     * present within the context as it's possible for the request to close before the lookup is complete.
     * 
     * @param searchTerm The provided search param. Either an org id or part of an org name
     * @param responsePath The generated path used to respond to Slack.
     */
    private void performMemberSearch(String searchTerm, String responsePath, List<MemberOrganization> allOrgs) {
        // Do a regular id lookup if value provided is a number
        if (StringUtils.isNumeric(searchTerm)) {

            int orgId = Integer.parseInt(searchTerm);
            Optional<MemberOrganization> result = allOrgs.stream().filter(o -> o.organizationID() == orgId).findFirst();

            // Return a 'Not Found' message if id is invalid
            if (result.isEmpty()) {
                slackAPI.postSlackPayload(responsePath, buildNotFoundResponse(searchTerm));
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER
                            .debug("Org {} found - Posting message to slack at URL: {}", TransformationHelper.formatLog(searchTerm),
                                    responsePath);
                }
                slackAPI.postSlackPayload(responsePath, buildMemberResponse(searchTerm, Arrays.asList(result.get())));
            }
        } else {

            String orgName = searchTerm.replace('+', ' ');
            // Find any orgs containing the desired string in the name
            List<MemberOrganization> matchingOrgs = allOrgs
                    .stream()
                    .filter(o -> StringUtils.containsIgnoreCase(o.name(), orgName))
                    .toList();
            if (matchingOrgs == null || matchingOrgs.isEmpty()) {
                slackAPI.postSlackPayload(responsePath, buildNotFoundResponse(orgName));
            } else if (matchingOrgs.size() > config.maxResultSize()) {
                slackAPI.postSlackPayload(responsePath, buildBroadSearchResponse(orgName, matchingOrgs.size()));
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER
                            .debug("Org {} found - Posting message to slack at URL: {}", TransformationHelper.formatLog(orgName),
                                    responsePath);
                }
                slackAPI.postSlackPayload(responsePath, buildMemberResponse(orgName, matchingOrgs));
            }
        }
    }

    /**
     * Builds a SlackResponse object indicating the desired member org can't be found.
     * 
     * @param receivedValue The received value
     * @return A SlackResponse object with a "not found" message.
     */
    private SlackResponse buildNotFoundResponse(String receivedValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("No matching member orgs for {}", TransformationHelper.formatLog(receivedValue));
        }
        return new SlackResponse(config.responseType(), String.format("No matching member orgs for *%s*", receivedValue),
                Collections.emptyList());
    }

    /**
     * Builds a SlackResponse object indicating the desired search value is too short.
     * 
     * @param receivedValue The received value
     * @return A SlackResponse object with a "bad request" message.
     */
    private SlackResponse buildBadRequestResponse(String receivedValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER
                    .debug("Value '{}' is too short. Please add more characters to narrow the search",
                            TransformationHelper.formatLog(receivedValue));
        }
        return new SlackResponse(config.responseType(),
                String.format("Value *%s* is too short. Please add more characters to narrow the search", receivedValue),
                Collections.emptyList());
    }

    /**
     * Builds a SlackResponse object indicating the desired search term is too broad.
     * 
     * @param receivedValue The received value
     * @return A SlackResponse object with a "broad search" message.
     */
    private SlackResponse buildBroadSearchResponse(String receivedValue, int resultSize) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER
                    .debug("Value '{}' yields too many results ({}). Please refine the search terms",
                            TransformationHelper.formatLog(receivedValue), resultSize);
        }
        return new SlackResponse(config.responseType(),
                String.format("Value *%s* yields too many results (%d). Please refine the search terms", receivedValue, resultSize),
                Collections.emptyList());
    }

    /**
     * Builds a SlackResponse object indicating the response_url is missing/invalid.
     * 
     * @return A SlackResponse object with a "bad request" message.
     */
    private SlackResponse buildInvalidUrlResponse() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("'response_url' field is invalid. Could not complete the request");
        }
        return new SlackResponse(config.responseType(), "'response_url' field is invalid. Could not complete the request",
                Collections.emptyList());
    }

    /**
     * Builds a SlackResponse object containing a success message and all desired member org and contact data.
     * 
     * @param receivedValue The received search value.
     * @param matchingOrgs All orgs found matching the received value.
     * @return A SlackResponse object containing the matching member org and contact data.
     */
    private SlackResponse buildMemberResponse(String receivedValue, List<MemberOrganization> matchingOrgs) {
        return new SlackResponse(config.responseType(),
                String.format("*%d* member org(s) found matching *%s*", matchingOrgs.size(), receivedValue),
                matchingOrgs.stream().map(this::buildAttachment).toList());
    }

    /**
     * Builds a SlackAttachment object. Each attachment corresponds to a member org matching the desired search param. Each attachment title
     * is the member org name with the company website and the text is the org contact data.
     * 
     * @param org The org for which to create an attachment.
     * @return A SlackAttachment object corresponding to the member org.
     */
    private SlackAttachment buildAttachment(MemberOrganization org) {
        StringBuilder titleBuilder = new StringBuilder();
        titleBuilder.append(org.name());
        titleBuilder.append(" ");

        // Add all level descriptions to the org title
        if (!org.levels().isEmpty()) {
            List<String> memberships = org.levels().stream().map(MemberOrganizationLevel::description).toList();
            titleBuilder.append("(");
            titleBuilder.append(String.join(", ", memberships));
            titleBuilder.append(")");
        }

        String memberUrl = MEMBERSHIP_PAGE_URL + org.organizationID();

        StringBuilder attachmentBody = new StringBuilder();
        attachmentBody.append(buildOrgContactString(org.organizationID()));
        attachmentBody.append(buildAdditionalInfoString(org));

        // Build using the member page as the link, and the contact data as the text
        SlackAttachment attach = new SlackAttachment(titleBuilder.toString(), memberUrl, attachmentBody.toString());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Built attachment: {}", attach);
        }
        return attach;
    }

    /**
     * Builds a string containing org contact data corresponding to a given org. Used as text for a SlackAttachment. The data is limited to
     * first/last name and relation of company reps and alternate company reps.
     * 
     * @param orgId The id of the org used to find company reps
     * @return A StringBuilder containing company rep data.
     */
    private StringBuilder buildOrgContactString(int orgId) {
        StringBuilder repBuilder = new StringBuilder();

        // Only lookup company reps and alternates
        List<EnhancedPersonData> contacts = getEnhancedPeopleDetails(Integer.toString(orgId), Arrays.asList(CR, CRA));
        LOGGER.debug("Found {} contacts for org '{}'", contacts.size(), orgId);
        if (!contacts.isEmpty()) {
            contacts.forEach(contact -> {
                // Extract role if it exists (it should)
                contact
                        .getRelations()
                        .stream()
                        .filter(rel -> valueOfChecked(rel).equals(CR) || valueOfChecked(rel).equals(CRA))
                        .map(rel -> valueOfChecked(rel).getLabel())
                        .findFirst()
                        .ifPresent(role -> {
                            repBuilder.append("*");
                            repBuilder.append(role);
                            repBuilder.append(":* ");
                            repBuilder.append(contact.getFirstName());
                            repBuilder.append(" ");
                            repBuilder.append(contact.getLastName());
                            repBuilder.append(" (_");
                            repBuilder.append(contact.getId());
                            repBuilder.append("_) - ");
                            repBuilder.append(PROFILE_PAGE_URL + contact.getId());
                            repBuilder.append("\n");
                        });
            });
        }
        return repBuilder;
    }

    private StringBuilder buildAdditionalInfoString(MemberOrganization org) {
        StringBuilder builder = new StringBuilder();
        if (org.memberSince() != null) {
            builder.append("*Member Since:* ");
            builder.append(org.memberSince());
            builder.append("\n");
        }

        if (org.renewalDate() != null) {
            builder.append("*Renewal Date:* ");
            builder.append(org.renewalDate());
            builder.append("\n");
        }

        if (StringUtils.isNotBlank(org.logos().web())) {
            builder.append("*Logo - web:* ");
            builder.append(org.logos().web());
            builder.append("\n");
        }

        if (StringUtils.isNotBlank(org.logos().print())) {
            builder.append("*Logo - print:* ");
            builder.append(org.logos().print());
            builder.append("\n");
        }

        if (org.wgpas() != null) {
            builder.append("*WG Participation:*");
            List<OrganizationWGPA> activeWGPAs = org.wgpas().stream().filter(wgpa -> wgpa.level() != null).toList();
            if (activeWGPAs.isEmpty()) {
                builder.append(" None");
            } else {
                builder.append("\n");
                activeWGPAs.forEach(wgpa -> {
                    Optional<WorkingGroup> wg = wgService.getByAgreementId(wgpa.documentID());
                    if (wg.isPresent()) {
                        builder.append("- ");
                        builder.append(wg.get().title());
                        builder.append(" - ");
                        builder.append(wgpa.description());
                        builder.append(" (_");
                        builder.append(wgpa.level());
                        builder.append("_)\n");
                    }
                });
            }
        }

        return builder;
    }

    /**
     * Validates the incoming Slack request body by creating a hash of it's contents and comparing it against the hash sent in the header by
     * Slack. Throws a 403 Forbidden if they do not match.
     * 
     * For reference: https://api.slack.com/authentication/verifying-requests-from-slack
     * 
     * @param body The incoming request body from Slack.
     */
    private void validateRequestIntegrity(String body) {

        // Create the base signature using the same format as Slack
        String signatureBase = "v0:" + wrap.getHeader("x-slack-request-timestamp") + ":" + body;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Using '{}' as signature base", signatureBase);
        }

        // Initialiaze the HMAC using the signing secret and the base signature
        HMac sha256HMac = new HMac(new SHA256Digest());
        sha256HMac.init(new KeyParameter(config.signingSecret().getBytes()));
        sha256HMac.update(signatureBase.getBytes(), 0, signatureBase.getBytes().length);

        // Compute the final stage into a byte array
        byte[] hmacOut = new byte[sha256HMac.getMacSize()];
        sha256HMac.doFinal(hmacOut, 0);

        // Convert the byte array to it's hex equivalent as a string
        StringBuilder sb = new StringBuilder(hmacOut.length * 2);
        for (byte by : hmacOut) {
            sb.append(String.format("%02x", by));
        }

        // Format the computed hash in the same way as the one sent by Slack
        String computedSignature = "v0=" + sb.toString();
        String slackSignature = wrap.getHeader("x-slack-signature");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Computed '{}'. Comparing against '{}'", computedSignature, slackSignature);
        }

        if (!computedSignature.equals(slackSignature)) {
            throw new FinalForbiddenException("Invalid signing secret");
        }
    }

    /**
     * Simple util method that converts the params of a 'application/x-www-form-urlencoded' body to a Map.
     * 
     * @param body The param string.
     * @return A reference to a Map ocntaining all params int he body.
     */
    private Map<String, String> convertBodyToMap(String body) {
        Map<String, String> out = new HashMap<>();
        Arrays.asList(body.split("&")).stream().forEach(param -> {
            String[] keyValuePairs = param.split("=");
            if (keyValuePairs.length == 2) {
                out.put(keyValuePairs[0], keyValuePairs[1]);
            }
        });
        return out;
    }

    /**
     * Simple util method that attempts to decode an encoded URL segment. Used to decode the `response_url` field sent by Slack. The
     * RestClient bound to slack will encode the URL we provide it. This prevents the URL from being double-encoded
     * 
     * @param urlSegment The encoded URL segment to decode
     * @return A decoded URL segment or null if there was an error.
     */
    private String decodeURLSegment(String urlSegment) {
        try {
            return URLDecoder.decode(urlSegment, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error decoding URL segment: {}", urlSegment, e);
            return null;
        }
    }
}
