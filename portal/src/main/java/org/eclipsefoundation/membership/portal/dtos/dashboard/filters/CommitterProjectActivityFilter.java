/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard.filters;

import org.eclipsefoundation.membership.portal.dtos.dashboard.CommitterProjectActivity;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedMap;

@Singleton
public class CommitterProjectActivityFilter implements DtoFilter<CommitterProjectActivity> {
    @Inject
    ParameterizedSQLStatementBuilder builder;

    @Override
    public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
        ParameterizedSQLStatement stmt = builder.build(CommitterProjectActivity.TABLE);
        if (isRoot) {
            // login check
            String login = params.getFirst(MembershipPortalParameterNames.LOGIN.getName());
            if (login != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(
                        CommitterProjectActivity.TABLE.getAlias() + ".compositeID.committerAffiliation.committer.id = ?", new Object[] { login }));
            }
            // project check
            String project = params.getFirst(MembershipPortalParameterNames.PROJECT.getName());
            if (project != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(
                        CommitterProjectActivity.TABLE.getAlias() + ".compositeID.project.id = ?", new Object[] { project }));
            }
            // organization ID check
            String organizationID = params.getFirst(MembershipPortalParameterNames.ORGANIZATION_ID.getName());
            if (organizationID != null) {
                stmt.addClause(
                        new ParameterizedSQLStatement.Clause(CommitterProjectActivity.TABLE.getAlias() + ".compositeID.committerAffiliation.orgId = ?",
                                new Object[] { Integer.valueOf(organizationID) }));
            }
            // period check
            String period = params.getFirst(MembershipPortalParameterNames.PERIOD.getName());
            if (period != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(CommitterProjectActivity.TABLE.getAlias() + ".compositeID.period = ?",
                        new Object[] { period }));
            }

            // period after check - will have multiple entries, for each project + login as this isn't a projection
            // that we can add the fields for
            String fromPeriod = params.getFirst(MembershipPortalParameterNames.FROM_PERIOD.getName());
            if (fromPeriod != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(CommitterProjectActivity.TABLE.getAlias() + ".compositeID.period >= ?",
                        new Object[] { fromPeriod }));
            }
        }
        String hasOrganization = params.getFirst(MembershipPortalParameterNames.HAS_ORGANIZATION.getName());
        if (Boolean.parseBoolean(hasOrganization)) {
            stmt.addClause(new ParameterizedSQLStatement.Clause(
                    CommitterProjectActivity.TABLE.getAlias() + ".compositeID.committerAffiliation.orgId is not null", new Object[] {}));
        }
        return stmt;
    }

    @Override
    public Class<CommitterProjectActivity> getType() {
        return CommitterProjectActivity.class;
    }

}