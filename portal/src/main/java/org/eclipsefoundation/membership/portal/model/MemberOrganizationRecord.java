/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.model;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.soabase.recordbuilder.core.RecordBuilder;
import jakarta.annotation.Nullable;

public final class MemberOrganizationRecord {
    @RecordBuilder
    public record MemberOrganization(int organizationID, String name, @Nullable LocalDate memberSince, @Nullable LocalDate renewalDate,
            @Nullable MemberOrganizationDescription description, @Nullable MemberOrganizationLogos logos, @Nullable String website,
            @Nullable List<MemberOrganizationLevel> levels, List<OrganizationWGPA> wgpas) {
    }

    @RecordBuilder
    public record MemberOrganizationDescription(@Nullable @JsonProperty("long") String longDescription) {
    }

    @RecordBuilder
    public record MemberOrganizationLogos(@Nullable String print, @Nullable String web) {
    }

    @RecordBuilder
    public record MemberOrganizationLevel(@Nullable String level, @Nullable String description, @Nullable String sortOrder) {
    }

    @RecordBuilder
    public record OrganizationWGPA(String documentID, @Nullable String description, @Nullable String level, String workingGroup) {
    }
}
