CREATE TABLE `DistributedCSRFToken` (
  `token` varchar(100) NOT NULL,
  `ipAddress` varchar(64) NOT NULL,
  `userAgent` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`token`)
);

CREATE TABLE `Sessions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(63) NOT NULL,
  `time` datetime NOT NULL,
  `organization` int DEFAULT NULL
);
INSERT INTO `Sessions`(`username`, `time`, `organization`) 
  VALUES('opearson', CURRENT_TIMESTAMP(), 15);
INSERT INTO `Sessions`(`username`, `time`) 
  VALUES('test-user', CURRENT_TIMESTAMP());