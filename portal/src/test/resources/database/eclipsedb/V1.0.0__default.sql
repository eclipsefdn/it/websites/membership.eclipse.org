-- eclipse.OrganizationInformation definition

CREATE TABLE OrganizationInformation (
  OrganizationID int NOT NULL,
  short_description varchar(512) DEFAULT NULL,
  long_description varchar(512) DEFAULT NULL,
  company_url varchar(512) NOT NULL,
  small_mime varchar(32) DEFAULT NULL,
  small_logo blob DEFAULT NULL,
  small_width int NOT NULL,
  small_height int NOT NULL,
  large_mime varchar(32) DEFAULT NULL,
  large_logo blob DEFAULT NULL,
  large_width int NOT NULL,
  large_height int NOT NULL
);
INSERT INTO OrganizationInformation(OrganizationID,short_description,long_description,company_url,small_mime,small_width,small_height,large_mime,large_width,large_height)
  VALUES(15, 'some description', 'some longer description', 'some.company.url', 'image/jpg',300,300,'image/png',500,500);
  
CREATE TABLE `OrganizationProducts` (
  `ProductID` int NOT NULL AUTO_INCREMENT,
  `OrganizationID` int DEFAULT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `product_url` varchar(512) NOT NULL,
  PRIMARY KEY (`ProductID`)
);

CREATE TABLE `SYS_EvtLog` (
  `LogId` int NOT NULL AUTO_INCREMENT,
  `LogTable` varchar(100) DEFAULT NULL,
  `PK1` varchar(100) DEFAULT NULL,
  `PK2` varchar(100) DEFAULT NULL,
  `LogAction` varchar(100) DEFAULT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `EvtDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`LogId`)
);

INSERT INTO OrganizationProducts(ProductID,OrganizationID,name,description,product_url)
  VALUES(15,1,'name', 'description', 'url');