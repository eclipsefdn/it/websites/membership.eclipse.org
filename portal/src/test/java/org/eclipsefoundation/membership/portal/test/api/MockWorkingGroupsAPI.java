/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.WorkingGroupsAPI;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroupBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationAgreementBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationAgreementsBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationLevelBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupResourcesBuilder;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import io.smallrye.mutiny.Uni;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@Priority(1)
@ApplicationScoped
@RestClient
public class MockWorkingGroupsAPI implements WorkingGroupsAPI {
    private List<WorkingGroup> internal;

    public MockWorkingGroupsAPI() {
        this.internal = Arrays
                .asList(WorkingGroupBuilder
                        .builder()
                        .alias("adoptium")
                        .description("description")
                        .levels(Arrays
                                .asList(WorkingGroupWorkingGroupParticipationLevelBuilder.builder().description("sample").relation("WGSD").build()))
                        .logo("http://sample.com")
                        .parentOrganization("eclipse")
                        .resources(WorkingGroupWorkingGroupResourcesBuilder
                                .builder()
                                .charter("")
                                .contactForm("")
                                .members("members")
                                .website("website")
                                .sponsorship("sponsorship")
                                .participationAgreements(WorkingGroupWorkingGroupParticipationAgreementsBuilder
                                        .builder()
                                        .individual(WorkingGroupWorkingGroupParticipationAgreementBuilder
                                                .builder()
                                                .documentId("32c5a745df264aa2a809")
                                                .pdf("pdf.link")
                                                .build())
                                        .build())
                                .build())
                        .status("active")
                        .title("Sample wg")
                        .build());
    }

    @Override
    public Uni<RestResponse<List<WorkingGroup>>> get(BaseAPIParameters baseParams, List<String> statuses) {
        return Uni
                .createFrom()
                .item(MockDataPaginationHandler
                        .paginateData(baseParams,
                                internal.stream().filter(wg -> statuses == null || statuses.contains(wg.status())).toList()));
    }
}
