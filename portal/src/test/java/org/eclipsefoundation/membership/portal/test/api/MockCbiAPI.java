/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.Arrays;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.membership.portal.api.CbiAPI;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;

@Alternative()
@Priority(1)
@ApplicationScoped
@RestClient
public class MockCbiAPI implements CbiAPI {

    private static final int ORG_1 = 15;
    private static final int ORG_2 = 150;
    private static final String ORG_1_NAME = "sample";
    private static final String ORG_2_NAME = "sample 2";

    private static final String SAMPLE_PROJECT_1_NAME = "cool.project";
    private static final String SAMPLE_PROJECT_2_NAME = "sample.tck";
    private static final String SAMPLE_PROJECT_3_NAME = "other-stuff.tech";

    private final CBI data;

    public MockCbiAPI() {
        this.data = new CBI(
                Arrays
                        .asList(new MemberOrganizationsBenefits(ORG_1, ORG_1_NAME, 0, 0, 0, 0, "Some comment", 0, 0),
                                new MemberOrganizationsBenefits(ORG_2, ORG_2_NAME, 0, 0, 0, 0, "Some comment", 0, 0)),
                Arrays
                        .asList(new SponsoredProject(new ProjectId(SAMPLE_PROJECT_1_NAME),
                                Arrays.asList(generateSponsoringOrganization(ORG_1, ORG_1_NAME))),
                                new SponsoredProject(new ProjectId(SAMPLE_PROJECT_2_NAME),
                                        Arrays
                                                .asList(generateSponsoringOrganization(ORG_2, ORG_2_NAME),
                                                        generateSponsoringOrganization(ORG_1, ORG_1_NAME))),
                                new SponsoredProject(new ProjectId(SAMPLE_PROJECT_3_NAME),
                                        Arrays.asList(generateSponsoringOrganization(ORG_2, ORG_2_NAME)))));
    }

    @Override
    public CBI getSponsorships() {
        return data;
    }

    private SponsoringOrganization generateSponsoringOrganization(int id, String name) {
        return new SponsoringOrganization(id, name, 0, 0, 0, "", Arrays.asList(""));
    }
}
