/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;
import org.eclipsefoundation.membership.portal.api.ProjectAPI;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@Priority(1)
@ApplicationScoped
@RestClient
public class MockFdnProjectsAPI implements ProjectAPI {

    private List<ProjectData> internal;

    public MockFdnProjectsAPI() {
        internal = new ArrayList<>();
        internal
                .addAll(Arrays
                        .asList(new ProjectData("test-project", "Test project", 2, "eclipse", "description", "", "", new Date(), 0, true,
                                "", "Active", 4, false, false)));
    }

    @Override
    public RestResponse<List<ProjectData>> getProjects(BaseAPIParameters baseParams, String organizationID) {
        return MockDataPaginationHandler.paginateData(baseParams, internal);
    }

    @Override
    public ProjectData getProject(BaseAPIParameters baseParams, String projectID) {
        return internal.stream().filter(p -> p.projectID().equalsIgnoreCase(projectID)).findFirst().orElse(null);
    }
}
