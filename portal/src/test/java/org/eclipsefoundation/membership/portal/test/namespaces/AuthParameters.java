/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.test.namespaces;

/**
 * Auth parameters specific to the Portal testing framework.
 */
public class AuthParameters {

    public static final String TEST_ADMIN_USER_NAME = "adminUser";
    public static final String TEST_ELEVATED_USER_NAME = "staffMember";
    public static final String TEST_BOTH_ROLES_USER_NAME = "adminStaff";
    public static final String ELEVATED_EMAIL_CLAIM_VALUE = "staffmember@eclipse.org";
    public static final String ELEVATED_FAMILY_NAME_CLAIM_VALUE = "Staff";
    public static final String ELEVATED_GIVEN_NAME_CLAIM_VALUE = "Member";
    public static final String PORTAL_ADMIN_ROLE = "eclipsefdn_membership_portal_admin";
    public static final String PORTAL_ELEVATED_ROLE = "eclipsefdn_membership_portal_elevated_access";
    public static final String ADDITIONAL_ROLES = "additional_roles";
    public static final String ORGANIZATIONAL_ROLES = "organizational_roles";

    private AuthParameters() {
    }
}
