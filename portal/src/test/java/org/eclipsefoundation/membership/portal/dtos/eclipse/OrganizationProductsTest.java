/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.eclipse;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

@QuarkusTest
class OrganizationProductsTest {

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    void getByOrgId() {
        // Add a record for testing
        List<OrganizationProducts> addResults = persistOrganizationProducts(656, 11);
        Assertions.assertFalse(addResults.isEmpty(), "OrganizationProducts persistence failed unexpectedly, cannot perform test");

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), "656");

        List<OrganizationProducts> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(OrganizationProducts.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one OrganizationProducts record found");
        Assertions.assertEquals(addResults.get(0), getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid logId
        params = new MultivaluedHashMap<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), "42");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(OrganizationProducts.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 OrganizationProducts records found");
    }

    @Test
    void getByProductId() {
        // Add a record for testing
        List<OrganizationProducts> addResults = persistOrganizationProducts(17, null);
        Assertions.assertFalse(addResults.isEmpty(), "OrganizationProducts persistence failed unexpectedly, cannot perform test");

        // get the ID as its autoincrement, and cannot be manually set
        Integer persistedId = addResults.get(0).getProductId();

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MembershipPortalParameterNames.PRODUCT_ID.getName(), Integer.toString(persistedId));

        List<OrganizationProducts> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(OrganizationProducts.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one OrganizationProducts record found");
        Assertions.assertEquals(addResults.get(0), getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid logId
        params = new MultivaluedHashMap<>();
        params.add(MembershipPortalParameterNames.PRODUCT_ID.getName(), "9999");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(OrganizationProducts.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 OrganizationProducts records found");
    }

    private List<OrganizationProducts> persistOrganizationProducts(int orgId, Integer productId) {
        OrganizationProducts products = new OrganizationProducts();
        products.setOrganizationId(orgId);
        products.setProductId(productId);
        products.setDescription("Some desc");
        products.setName("Best product");
        products.setProductUrl("google.com");
        return dao.add(new RDBMSQuery<>(WRAP, filters.get(OrganizationProducts.class)), Arrays.asList(products));
    }
}
