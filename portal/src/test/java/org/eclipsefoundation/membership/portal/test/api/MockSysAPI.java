/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.membership.portal.api.SysAPI;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@Priority(1)
@ApplicationScoped
@RestClient
public class MockSysAPI implements SysAPI {

    private final List<SysRelationData> relations;

    public MockSysAPI() {
        this.relations = Arrays
                .asList(new SysRelationData("ABRT", "Aborted/Lost", true, "TS", "0"),
                        new SysRelationData("AC", "Architecture Council Representative", true, "CO", "0"),
                        new SysRelationData("AD", "Admin Contact", true, "CO", "0"),
                        new SysRelationData("AP", "Solutions Member", true, "ME", "0"),
                        new SysRelationData("AR", "Add-In Provider Representative", true, "GE", "0"),
                        new SysRelationData("AS", "Associate Member", true, "ME", "0"),
                        new SysRelationData("AZ", "Architecture Council Chair", true, "GE", "0"),
                        new SysRelationData("BITBK", "External ID: BitBucket", true, "ID", "0"),
                        new SysRelationData("BR", "Board Rep", true, "CO", "0"),
                        new SysRelationData("CA", "Accounting Contact", true, "CO", "0"),
                        new SysRelationData("CAD", "Canadian Dollars", true, "CR", "0"),
                        new SysRelationData("CB", "Committer Board Representative", true, "GE", "0"),
                        new SysRelationData("CC", "Committer Member", true, "CO", "0"),
                        new SysRelationData("CE", "Committer Emeritus", true, "PR", "0"),
                        new SysRelationData("CEPND", "Committer Emeritus - Pending", true, "PR", "0"),
                        new SysRelationData("CI", "IT Contact", true, "CO", "0"),
                        new SysRelationData("CL", "Legal Contact", true, "CO", "0"),
                        new SysRelationData("CM", "Committer", true, "PR", "0"), new SysRelationData("CN", "Contributor", true, "PR", "0"),
                        new SysRelationData("CO", "Contact", true, "GE", "0"),
                        new SysRelationData("CONFQ", "Eclipse Conference Organizer", true, "GE", "0"),
                        new SysRelationData("CR", "Company Representative", true, "CO", "0"),
                        new SysRelationData("CS", "Sales Contact", true, "CO", "0"), new SysRelationData("DE", "Delegate", true, "CO", "0"),
                        new SysRelationData("EA", "Appointed-Architecture Council", true, "GE", "0"));
    }

    @Override
    public RestResponse<List<SysRelationData>> getSysRelations(BaseAPIParameters baseParams, String type) {
        return MockDataPaginationHandler.paginateData(baseParams, relations.stream().filter(r -> r.type().equalsIgnoreCase(type)).toList());
    }

    @Override
    public List<SysModLogData> postModLog(SysModLogData modLog) {
        return Arrays.asList(modLog);
    }

}
