/**
 * Copyright (c) 2022, 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *          Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import org.eclipsefoundation.membership.portal.test.namespaces.SchemaNamespaces;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class InterestGroupOrganizationsResourceTest {
    public static final String IG_ORGS_BY_ID = "/interest_groups/{id}/organizations";

    public static final EndpointTestCase GET_SUCCESS = TestCaseHelper.buildSuccessCase(IG_ORGS_BY_ID,
            new String[] { "123456" }, SchemaNamespaces.ORGANIZATIONS_SCHEMA_PATH);

    @Test
    void getById_success() {
        EndpointTestBuilder.from(GET_SUCCESS).run();
    }

    @Test
    void getById_success_format() {
        EndpointTestBuilder.from(GET_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getById_success_schema() {
        EndpointTestBuilder.from(GET_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getById_failure_notFound() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(IG_ORGS_BY_ID, new String[] { "hello" }, null)).run();
    }

    @Test
    void getById_failure_badRequest() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(IG_ORGS_BY_ID, new String[] { "" }, null)).run();
    }
}
