/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import org.eclipsefoundation.membership.portal.test.namespaces.SchemaNamespaces;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class WorkingGroupLevelsResourceTest {
    public static final String WG_BASE_URL = "/working_groups";
    public static final String GET_WGS_URL = WG_BASE_URL + "?parent_organization={org}&status={statuses}";
    public static final String GET_WG_URL = WG_BASE_URL + "/{name}";

    public static final String SAMPLE_PARENT_ORG = "eclipse";
    public static final String SAMPLE_WG = "adoptium";

    public static final EndpointTestCase GET_WGS_CASE = TestCaseHelper
            .buildSuccessCase(GET_WGS_URL, new String[] { SAMPLE_PARENT_ORG, "active" }, SchemaNamespaces.WORKING_GROUPS_SCHEMA_PATH);

    public static final EndpointTestCase GET_WG_CASE = TestCaseHelper
            .buildSuccessCase(GET_WG_URL, new String[] { SAMPLE_WG }, SchemaNamespaces.WORKING_GROUP_SCHEMA_PATH);

    @Test
    void getWGs_success() {
        EndpointTestBuilder.from(GET_WGS_CASE).run();
    }

    @Test
    void getWGs_success_validateSchema() {
        EndpointTestBuilder.from(GET_WGS_CASE).andCheckSchema().run();
    }

    @Test
    void getWGs_success_vlaidateResponseFormat() {
        EndpointTestBuilder.from(GET_WGS_CASE).andCheckFormat().run();
    }

    @Test
    void getWG_success() {
        EndpointTestBuilder.from(GET_WG_CASE).run();
    }

    @Test
    void getWG_success_validateSchema() {
        EndpointTestBuilder.from(GET_WG_CASE).andCheckSchema().run();
    }

    @Test
    void getWG_success_vlaidateResponseFormat() {
        EndpointTestBuilder.from(GET_WG_CASE).andCheckFormat().run();
    }
}
