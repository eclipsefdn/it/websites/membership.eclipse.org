/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import org.eclipsefoundation.membership.portal.test.namespaces.SchemaNamespaces;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class ProjectsResourceTest {

    public static final String PROJECT_ORGS_URL = "/projects/{projectId}/organizations";

    public static final String PROJECT_SAMPLE_ID = "test-project";

    public static final EndpointTestCase GET_PROJECT_ORGS_SUCCESS = TestCaseHelper
            .buildSuccessCase(PROJECT_ORGS_URL, new String[] { PROJECT_SAMPLE_ID }, SchemaNamespaces.ORGANIZATIONS_SCHEMA_PATH);

    @Test
    void getProjectOrgs_success() {
        EndpointTestBuilder.from(GET_PROJECT_ORGS_SUCCESS).run();
    }

    @Test
    void getProjectOrgs_success_validateSchema() {
        EndpointTestBuilder.from(GET_PROJECT_ORGS_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getProjectOrgs_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_PROJECT_ORGS_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getProjectOrgs_failure_notFound() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(PROJECT_ORGS_URL, new String[] { "not-found" }, null)).run();
    }
}
