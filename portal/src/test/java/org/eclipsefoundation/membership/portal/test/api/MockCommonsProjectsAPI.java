/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.ProjectsAPI;
import org.eclipsefoundation.efservices.api.models.GitlabProjectBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.InterestGroupBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroupDescriptorBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroupInterestGroupParticipantBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroupOrganizationBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroupResourceBuilder;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@ApplicationScoped
@RestClient
public class MockCommonsProjectsAPI implements ProjectsAPI {

    private List<InterestGroup> igs;

    public MockCommonsProjectsAPI() {
        this.igs = Arrays
                .asList(InterestGroupBuilder
                        .builder()
                        .description(InterestGroupDescriptorBuilder.builder().full("full").summary("summary").build())
                        .projectId("foundationdbProjectId")
                        .gitlab(GitlabProjectBuilder
                                .builder()
                                .ignoredSubGroups(Collections.emptyList())
                                .projectGroup("projectGroup")
                                .build())
                        .id("123456")
                        .leads(Arrays.asList(InterestGroupInterestGroupParticipantBuilder.builder().url("url")
                                .username("opearson")
                                .fullName("Oli Pearson")
                                .organization(InterestGroupOrganizationBuilder.builder()
                                        .documents(Collections.emptyMap())
                                        .id("656")
                                        .name("org").build())
                                .build()))
                        .logo("logo")
                        .participants(Arrays.asList())
                        .scope(InterestGroupDescriptorBuilder.builder().full("full").summary("summary").build())
                        .state("active")
                        .title("Some title")
                        .shortProjectId("title")
                        .resources(InterestGroupResourceBuilder.builder().members("members").website("google.com").build())
                        .mailingList("mailinglist.com")
                        .build());
    }

    @Override
    public Uni<RestResponse<List<InterestGroup>>> getInterestGroups(BaseAPIParameters params) {
        return Uni.createFrom().item(MockDataPaginationHandler.paginateData(params, igs));
    }

    @Override
    public Uni<RestResponse<List<Project>>> getProjects(BaseAPIParameters arg0, int arg1) {
        throw new UnsupportedOperationException("Unimplemented method 'getProjects'");
    }
}
