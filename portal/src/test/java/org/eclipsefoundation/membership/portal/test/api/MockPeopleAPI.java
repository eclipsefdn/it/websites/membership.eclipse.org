/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.membership.portal.api.PeopleAPI;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockPeopleAPI extends CommonAPIMock implements PeopleAPI {

    List<PeopleData> internal;

    public MockPeopleAPI() {
        internal = new ArrayList<>();
        internal
                .addAll(Arrays
                        .asList(createPerson("opearson", "oli.pearson@eclipse.org"),
                                createPerson("opearson2", "oli.pearson2@eclipse.org")));
    }

    @Override
    public RestResponse<List<PeopleData>> getPeople(BaseAPIParameters baseParams, PeopleRequestParams params) {
        List<PeopleData> results = internal.stream().filter(p -> p.personID().equalsIgnoreCase(params.id)).collect(Collectors.toList());
        return MockDataPaginationHandler.paginateData(baseParams, results);
    }

}
