/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.config;

import java.time.LocalDate;

import io.quarkus.logging.Log;

/**
 * 
 */
public class H2DBFunctions {

    public static String getPeriodStringFromLocalDate(LocalDate d) {
        // check that month is 2 characters
        String month = Integer.toString(d.getMonthValue());
        if (month.length() == 1) {
            month = "0" + month;
        }
        Log.errorf("%s", Integer.toString(d.getYear()) + month);
        return Integer.toString(d.getYear()) + month;
    }

    public static String getPeriodStringForNow() {
        return getPeriodStringFromLocalDate(LocalDate.now());
    }
}
