/**
 * Copyright (c) 2022, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.test.api;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.views.EnhancedOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.views.MemberOrganizationData;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.eclipsefoundation.membership.portal.api.ComplexAPI;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI.OrganizationRequestParams;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@ApplicationScoped
@RestClient
public class MockComplexAPI extends CommonAPIMock implements ComplexAPI {

    private List<MemberOrganizationData> memberOrgs;
    private List<EnhancedOrganizationContactData> orgContacts;

    @PostConstruct
    void init() {
        this.memberOrgs = Arrays
                .asList(new MemberOrganizationData(15, "Sample member org", LocalDate.now(), LocalDate.now(), Collections.emptyList(),
                        Collections.emptyList()));

        this.orgContacts = Arrays
                .asList(new EnhancedOrganizationContactData(15, createPerson("opearson", "some_email@dummy.org"),
                        Arrays.asList(new SysRelationData("CR", "some desc", true, "CO", "")), true),
                        new EnhancedOrganizationContactData(15, createPerson("rdex", "some_email2@dummy.org"),
                                Arrays.asList(new SysRelationData("EMPLY", "some desc", true, "CO", "")), true),
                        new EnhancedOrganizationContactData(15, createPerson("jsheppard", "some_email3@dummy.org"),
                                Arrays.asList(new SysRelationData("EMPLY", "some desc", true, "CO", "")), false));
    }

    @Override
    public RestResponse<List<EnhancedOrganizationContactData>> getContacts(BaseAPIParameters params, String orgId, String personId) {
        return MockDataPaginationHandler
                .paginateData(params,
                        orgContacts.stream().filter(c -> c.organizationId() == Integer.valueOf(orgId)).collect(Collectors.toList()));
    }

    @Override
    public RestResponse<List<MemberOrganizationData>> getMembers(BaseAPIParameters params, OrganizationRequestParams orgParams) {
        List<MemberOrganizationData> results = memberOrgs
                .stream()
                .filter(m -> orgParams.id == null || m.organizationID() == Integer.valueOf(orgParams.id))
                .toList();
        RestResponse<List<MemberOrganizationData>> response = RestResponse.ok(results);
        response.getHeaders().add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, 25);
        response.getHeaders().add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, memberOrgs.size());
        return response;
    }
}
