/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BeanParam;

@Mock
@Priority(1)
@ApplicationScoped
@RestClient
public class MockOrganizationsAPI extends CommonAPIMock implements OrganizationAPI {

    public static final String ORGANIZATION_CONTACT_USER_NAME = "opearson";
    public static final String ORGANIZATION_CONTACT_USER_NAME_DELETE = "opearson2";
    public static final String ORGANIZATION_SAMPLE_ID = "15";
    public static final String SECOND_ORGANIZATION_SAMPLE_ID = "20";

    private List<PeopleData> people;
    private List<OrganizationData> orgs;
    private List<OrganizationContactData> contacts;

    public MockOrganizationsAPI() {
        this.people = new ArrayList<>();
        this.people.addAll(Arrays.asList(createPerson(ORGANIZATION_CONTACT_USER_NAME, "some_email@dummy.org")));

        this.orgs = new ArrayList<>();
        this.orgs
                .addAll(Arrays
                        .asList(createOrganization(Integer.valueOf(ORGANIZATION_SAMPLE_ID), "sample"),
                                createOrganization(Integer.valueOf(SECOND_ORGANIZATION_SAMPLE_ID), "sample2")));

        this.contacts = new ArrayList<>();
        this.contacts
                .addAll(Arrays
                        .asList(new OrganizationContactData(Integer.valueOf(ORGANIZATION_SAMPLE_ID), ORGANIZATION_CONTACT_USER_NAME, "CR",
                                "", "", "Sample"),
                                new OrganizationContactData(Integer.valueOf(ORGANIZATION_SAMPLE_ID), ORGANIZATION_CONTACT_USER_NAME_DELETE,
                                        "CR", "", "", "Sample"),
                                new OrganizationContactData(Integer.valueOf(ORGANIZATION_SAMPLE_ID), ORGANIZATION_CONTACT_USER_NAME_DELETE,
                                        "DE", "", "", "Sample")));
    }

    @Override
    public RestResponse<List<OrganizationContactData>> getOrganizationContacts(@BeanParam BaseAPIParameters baseParams,
            OrganizationRequestParams params) {
        return MockDataPaginationHandler
                .paginateData(baseParams, contacts.stream().filter(c -> c.personID().equalsIgnoreCase(params.personId)).toList());
    }

    @Override
    public OrganizationContactData updateOrganizationContacts(String id, OrganizationContactData contact) {
        contacts
                .removeIf(c -> c.organizationID() == Integer.valueOf(id) && c.personID().equalsIgnoreCase(contact.personID())
                        && c.relation().equalsIgnoreCase(contact.relation()));
        contacts.add(contact);
        return contact;
    }

    @Override
    public RestResponse<Void> removeOrganizationContacts(String id, String personID, String relation) {
        List<OrganizationContactData> results = contacts
                .stream()
                .filter(c -> c.organizationID() == Integer.valueOf(id) && c.personID().equalsIgnoreCase(personID)
                        && c.relation().equalsIgnoreCase(relation))
                .collect(Collectors.toList());
        contacts.removeAll(results);
        return RestResponse.ok();
    }

    @Override
    public RestResponse<List<OrganizationContactData>> getOrganizationContact(String id, String personID,
            @BeanParam BaseAPIParameters baseParams, OrganizationRequestParams params) {
        List<OrganizationContactData> results = contacts
                .stream()
                .filter(c -> c.personID().equalsIgnoreCase(personID) && c.organizationID() == Integer.valueOf(id))
                .collect(Collectors.toList());
        return MockDataPaginationHandler.paginateData(baseParams, results);
    }

    private OrganizationData createOrganization(int orgId, String name) {
        return new OrganizationData(orgId, name, null, null, null, null, new Date(), null, null, new Date());
    }
}
