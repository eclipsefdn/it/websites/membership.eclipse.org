/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.membership.portal.test.api;

import java.util.Date;

import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;

/**
 * 
 */
public abstract class CommonAPIMock {
    PeopleData createPerson(String uname, String email) {
        return new PeopleData(uname, "first", "last", "XX", false, email, "", "", "", "", "", false, false, "", new Date(), null, null,
                null, null);
    }
}
