/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import static io.restassured.RestAssured.given;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.membership.portal.test.namespaces.AuthParameters;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

@QuarkusTest
class ReportsResourceTest {
    public static final String REPORTS_URL = "/reports";
    public static final String ADOPTION_RATE_URL = REPORTS_URL + "/adoption_rate";
    public static final String ADOPTION_RATE_SINCE_URL = ADOPTION_RATE_URL + "?since={param}";
    public static final String MEMBER_HIGHLIGHTS_URL = REPORTS_URL + "/member-highlights";
    public static final String MEMBER_HIGHLIGHT_URL = MEMBER_HIGHLIGHTS_URL + "/{orgId}";

    private static final String VALID_ORG_ID = "15";

    // Generate expected Content-Disposition header using today's date
    private static final String EXPECTED_DISP_HEADER_VALUE = "attachment; filename="
            + new SimpleDateFormat("yyyy-MM-dd").format(Date.from(Instant.now())) + "-portal-session-report.csv";

    //
    // GET /reports/adoption_rate
    //
    @Test
    void getReports_failure_requireAuth() {
        given().auth().none().when().get(ADOPTION_RATE_URL).then().statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }

    @Test
    @TestSecurity(user = "invalidUser", roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getReports_failure_invaliduser() {
        given().when().get(ADOPTION_RATE_URL).then().statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getReports_failure_elevatedAccess() {
        given().when().get(ADOPTION_RATE_URL).then().statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getReports_success() {
        given()
                .when()
                .get(ADOPTION_RATE_URL)
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .contentType("text/csv")
                .header(HttpHeaders.CONTENT_DISPOSITION, EXPECTED_DISP_HEADER_VALUE);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getReportsSince_success() {
        given()
                .when()
                .get(ADOPTION_RATE_SINCE_URL, "2024-01-01")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .contentType("text/csv")
                .header(HttpHeaders.CONTENT_DISPOSITION, EXPECTED_DISP_HEADER_VALUE);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getReportsSince_failure_invalidDateValue() {
        given().when().get(ADOPTION_RATE_SINCE_URL, "invalid").then().statusCode(Response.Status.BAD_REQUEST.getStatusCode());
        given().when().get(ADOPTION_RATE_SINCE_URL, "0").then().statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }

    //
    // Tests for /reports/member-highlights
    //
    @Test
    void getMembershipHighlights_failure_requireAuth() {
        given().auth().none().when().get(MEMBER_HIGHLIGHTS_URL).then().statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }

    @Test
    @TestSecurity(user = "webdev")
    void getMembershipHighlights_failure_requireAuth_reportsRole() {
        given().auth().none().when().get(MEMBER_HIGHLIGHTS_URL).then().statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getMembershipHighlights_success() {
        given().when().get(MEMBER_HIGHLIGHTS_URL).then().statusCode(Response.Status.OK.getStatusCode());
    }

    //
    // Tests for /reports/member-highlights/{orgId}
    //
    @Test
    void getMembershipHighlight_failure_requireAuth() {
        given()
                .auth()
                .none()
                .when()
                .get(MEMBER_HIGHLIGHT_URL, VALID_ORG_ID)
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }

    @Test
    @TestSecurity(user = "webdev")
    void getMembershipHighlight_failure_requireAuth_reportsRole() {
        given().auth().none().when().get(MEMBER_HIGHLIGHT_URL, VALID_ORG_ID).then().statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getMembershipHighlight_success() {
        given()
                .when()
                .get(MEMBER_HIGHLIGHT_URL, VALID_ORG_ID)
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("organization_id", Matchers.equalTo(15), "organization_name", Matchers.equalTo("Sample member org"),
                        "number_of_committers", Matchers.equalTo(2));
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getMembershipHighlight_failure_noMatchingOrg() {
        given().when().get(MEMBER_HIGHLIGHT_URL, "500").then().statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getMembershipHighlight_failure_invalidOrg() {
        given().when().get(MEMBER_HIGHLIGHT_URL, "001-invalid").then().statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }
}
