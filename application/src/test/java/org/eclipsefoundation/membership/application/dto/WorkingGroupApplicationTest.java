/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.membership.application.dto;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplication;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * 
 */
@QuarkusTest
class WorkingGroupApplicationTest {

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    void retrieve_success_byId() {
        // random string to stop collision of tests
        String username = UUID.randomUUID().toString();
        List<WorkingGroupApplication> apps = persistApplication(username, FormState.INPROGRESS);
        Assertions.assertFalse(apps.isEmpty());
        WorkingGroupApplication expected = apps.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), expected.getId().toString());

        List<WorkingGroupApplication> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(WorkingGroupApplication.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one WorkingGroupApplication record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");
    }

    @Test
    void retrieve_success_byFormState() {
        // random string to stop collision of tests
        String username = UUID.randomUUID().toString();
        List<WorkingGroupApplication> apps = persistApplication(username, FormState.COMPLETE);
        Assertions.assertFalse(apps.isEmpty());
        WorkingGroupApplication expected = apps.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), FormState.COMPLETE.name());
        // username needed to make it unique enough to find
        params.add(MembershipFormAPIParameterNames.USER_ID.getName(), username);

        List<WorkingGroupApplication> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(WorkingGroupApplication.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one WorkingGroupApplication record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");
    }

    private List<WorkingGroupApplication> persistApplication(String username, FormState state) {
        return dao
                .add(new RDBMSQuery<>(WRAP, filters.get(WorkingGroupApplication.class)),
                        Arrays.asList(DtoHelper.generateWgApplication(username, 15, state)));
    }
}
