/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.resources;

import java.util.Map;

import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;

@QuarkusTest
class OIDCResourceTest {
    public static final String CSRF_URL = "/csrf";
    public static final String USER_INFO_URL = "/userinfo";

    private static final Map<String, Object> USER_INFO_SUCCESS_BODY_PARAMS = Map
            .of(AuthHelper.GIVEN_NAME_CLAIM_KEY, AuthHelper.GIVEN_NAME_CLAIM_VALUE, AuthHelper.FAMILY_NAME_CLAIM_KEY,
                    AuthHelper.FAMILY_NAME_CLAIM_VALUE, "name", AuthHelper.TEST_USER_NAME);

    public static final EndpointTestCase CSRF_CASE_SUCCESS = TestCaseHelper.prepareTestCase(CSRF_URL, new String[] {}, null).build();

    public static final EndpointTestCase USER_INFO_AUTH_SUCCESS = TestCaseHelper
            .buildSuccessCase(USER_INFO_URL, new String[] {}, SchemaNamespaceHelper.INFO_WRAPPER_SCHEMA_PATH);

    public static final EndpointTestCase USER_INFO_AUTH_SUCCESS_BODY = TestCaseHelper
            .prepareTestCase(USER_INFO_URL, new String[] {}, null)
            .setBodyValidationParams(USER_INFO_SUCCESS_BODY_PARAMS)
            .build();

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void ensureCSRFEndpoint() {
        EndpointTestBuilder.from(CSRF_CASE_SUCCESS).run();
    }

    @Test
    void ensureCSRFEndpoint_noAuth() {
        EndpointTestBuilder.from(CSRF_CASE_SUCCESS).run();
    }

    @Test
    void userInfo_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_INFO_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.NO_CONTENT.getStatusCode())
                        .build())
                .run();
    }

    @Test
    void userInfo_noauth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_INFO_URL, new String[] {}, null)
                        .setDisableCsrf(true)
                        .setStatusCode(Response.Status.NO_CONTENT.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void userInfo_auth() {
        EndpointTestBuilder.from(USER_INFO_AUTH_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void userInfo_auth_validateSchema() {
        EndpointTestBuilder.from(USER_INFO_AUTH_SUCCESS).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void userInfo_auth_validateResponseFormat() {
        EndpointTestBuilder.from(USER_INFO_AUTH_SUCCESS).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void userInfo_auth_passedData() {
        EndpointTestBuilder.from(USER_INFO_AUTH_SUCCESS_BODY).run();
    }
}
