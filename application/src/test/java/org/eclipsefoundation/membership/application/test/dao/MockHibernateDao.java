/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.test.dao;

import jakarta.enterprise.context.ApplicationScoped;

import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;

import io.quarkus.runtime.Startup;
import io.quarkus.test.Mock;

/**
 * To keep tests separate from datastore, set up a dummy endpoint that returns copies of static data.
 *
 * @author Martin Lowe
 */
@Mock
@Startup
@ApplicationScoped
public class MockHibernateDao extends DefaultHibernateDao {

    /*
     * Test helper methods for quick access to IDs
     */
    public String getFormID() {
        return "form-uuid";
    }

    public String getFormOrgID() {
        return "15";
    }

    public String getFormContactID() {
        return "opearson";
    }

    public String getFormWGID() {
        return "working_group";
    }
}
