/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.resources;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.test.dao.MockHibernateDao;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

@QuarkusTest
class FormOrganizationsResourceTest {
    public static final String FORM_ORGANIZATION_BASE_URL = MembershipFormResourceTest.FORMS_BY_ID_URL + "/organizations";
    public static final String FORM_ORGANIZATION_BY_ID_URL = FORM_ORGANIZATION_BASE_URL + "/{organizationId}";

    public static final String FORM_ID = "form-uuid";
    public static final String FORM_ORG_ID = "15";

    public static final EndpointTestCase GET_FORM_ORGS_CASE = TestCaseHelper
            .buildSuccessCase(FORM_ORGANIZATION_BASE_URL, new String[] { FORM_ID }, SchemaNamespaceHelper.FORM_ORGANIZATIONS_SCHEMA_PATH);

    public static final EndpointTestCase GET_FORM_ORG_BY_ID_CASE = TestCaseHelper
            .buildSuccessCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, FORM_ORG_ID },
                    SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH);

    public static final EndpointTestCase POST_FORM_ORG_CASE = TestCaseHelper
            .buildSuccessCase(FORM_ORGANIZATION_BASE_URL, new String[] { FORM_ID }, SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH);

    @Inject
    ObjectMapper om;
    @Inject
    MockHibernateDao mockDao;

    //
    // GET /form/{id}/organizations
    //
    @Test
    void getFormOrganizations_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BASE_URL, new String[] { FORM_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizations_success() {
        EndpointTestBuilder.from(GET_FORM_ORGS_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizations_success_validateSchema() {
        EndpointTestBuilder.from(GET_FORM_ORGS_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizations_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_FORM_ORGS_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizations_failure_invalidFormId() {
        EndpointTestBuilder.from(TestCaseHelper.buildBadRequestCase(FORM_ORGANIZATION_BASE_URL, new String[] { "nope" }, null)).run();
    }

    //
    // POST /form/{id}/organizations
    //
    @Test
    void postFormOrganization_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BASE_URL, new String[] { FORM_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormOrganization_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BASE_URL, new String[] { FORM_ID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormOrganization_success() {
        EndpointTestBuilder.from(POST_FORM_ORG_CASE).doPost(generateSample()).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormOrganization_success_validateRequestSchema() {
        String request = generateSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH).matches(request));

        EndpointTestBuilder.from(POST_FORM_ORG_CASE).doPost(request).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormOrganization_success_validateSchema() {
        EndpointTestBuilder.from(POST_FORM_ORG_CASE).doPost(generateSample()).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormOrganization_success_validateResponseFormat() {
        EndpointTestBuilder.from(POST_FORM_ORG_CASE).doPost(generateSample()).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormOrganization_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_ORGANIZATION_BASE_URL, new String[] { "nope" }, null))
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormOrganization_failure_emptyBody() {
        EndpointTestBuilder
                .from(TestCaseHelper.prepareTestCase(FORM_ORGANIZATION_BASE_URL, new String[] { FORM_ID }, null).setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build())
                .doPost(null)
                .run();
    }

    //
    // GET /form/{id}/organizations/{organizationId}
    //
    @Test
    void getFormOrganizationByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, FORM_ORG_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizationByID_success() {
        EndpointTestBuilder.from(GET_FORM_ORG_BY_ID_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizationByID_success_validateSchema() {
        EndpointTestBuilder.from(GET_FORM_ORG_BY_ID_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizationByID_success_validateResponse() {
        EndpointTestBuilder.from(GET_FORM_ORG_BY_ID_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizationByID_failure_orgNotFound() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, "nope" }, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormOrganizationByID_failure_invalidformId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { "nope", FORM_ORG_ID }, null))
                .run();
    }

    //
    // PUT /form/{id}/organizations/{organizationId}
    //
    @Test
    void putFormOrganizationByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, FORM_ORG_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormOrganizationByID_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, FORM_ORG_ID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormOrganizationByID_failure_emptyBody() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, FORM_ORG_ID }, null)
                        .setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                        .build())
                .doPut(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormOrganizationByID_success() {
        EndpointTestBuilder.from(GET_FORM_ORG_BY_ID_CASE).doPut(generateSample()).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormOrganizationByID_success_validateSchema() {
        EndpointTestBuilder.from(GET_FORM_ORG_BY_ID_CASE).doPut(generateSample()).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormOrganizationByID_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_FORM_ORG_BY_ID_CASE).doPut(generateSample()).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormOrganizationByID_success_validateRequestFormat() {
        String request = generateSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH).matches(request));

        EndpointTestBuilder.from(GET_FORM_ORG_BY_ID_CASE).doPut(request).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormOrganizationByID_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { "nope", FORM_ORG_ID }, null))
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormOrganizationByID_failure_invalidOrgId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, "nope" }, null))
                .doPut(generateSample())
                .run();
    }

    //
    // DELETE /form/{id}/organizations/{organizationId}
    //
    @Test
    void deleteFormOrganizationByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, FORM_ORG_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteFormOrganizationByID_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { FORM_ID, FORM_ORG_ID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteFormOrganizationByID_success() {
        EndpointTestBuilder.from(GET_FORM_ORG_BY_ID_CASE).doDelete(null).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteFormOrganizationByID_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_ORGANIZATION_BY_ID_URL, new String[] { "nope", FORM_ORG_ID }, null))
                .doDelete(null)
                .run();
    }

    private FormOrganization generateSampleRaw() {
        return DtoHelper.generateOrg(mockDao.getReference(FORM_ID, MembershipForm.class));
    }

    private String generateSample() {
        try {
            return om.writeValueAsString(generateSampleRaw());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
