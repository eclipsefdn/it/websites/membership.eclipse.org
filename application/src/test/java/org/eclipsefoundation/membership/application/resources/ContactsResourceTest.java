/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.resources;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.model.ContactData;
import org.eclipsefoundation.membership.application.model.mappers.ContactMapper;
import org.eclipsefoundation.membership.application.namespace.ContactTypes;
import org.eclipsefoundation.membership.application.test.dao.MockHibernateDao;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

@QuarkusTest
class ContactsResourceTest {
    public static final String CONTACTS_BASE_URL = MembershipFormResourceTest.FORMS_BY_ID_URL + "/contacts";
    public static final String CONTACTS_BY_ID_URL = CONTACTS_BASE_URL + "/{contactId}";

    public static final String FORM_ID = "form-uuid";
    public static final String DELETE_TEST_FORM_ID = "delete-test-uuid";
    public static final String FORM_ORG_ID = "15";
    public static final String FORM_CONTACT_ID = "opearson";

    public static final EndpointTestCase GET_CONTACTS_CASE = TestCaseHelper
            .buildSuccessCase(CONTACTS_BASE_URL, new String[] { FORM_ID }, SchemaNamespaceHelper.CONTACTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_BY_ID_CASE = TestCaseHelper
            .buildSuccessCase(CONTACTS_BY_ID_URL, new String[] { FORM_ID, FORM_CONTACT_ID }, SchemaNamespaceHelper.CONTACT_SCHEMA_PATH);

    public static final EndpointTestCase POST_CONTACT_CASE = TestCaseHelper
            .buildSuccessCase(CONTACTS_BASE_URL, new String[] { FORM_ID }, SchemaNamespaceHelper.CONTACT_SCHEMA_PATH);

    public static final EndpointTestCase PUT_CONTACT_CASE = TestCaseHelper
            .buildSuccessCase(CONTACTS_BY_ID_URL, new String[] { FORM_ID, FORM_CONTACT_ID }, SchemaNamespaceHelper.CONTACTS_SCHEMA_PATH);

    @Inject
    ObjectMapper om;
    @Inject
    MockHibernateDao mockDao;
    @Inject
    ContactMapper cMap;

    //
    // GET /form/{id}/contacts
    //
    @Test
    void getContacts_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BASE_URL, new String[] { FORM_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContacts_success() {
        EndpointTestBuilder.from(GET_CONTACTS_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContacts_success_validateSchema() {
        EndpointTestBuilder.from(GET_CONTACTS_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContacts_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_CONTACTS_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContacts_failure_invalidFormId() {
        EndpointTestBuilder.from(TestCaseHelper.buildBadRequestCase(CONTACTS_BASE_URL, new String[] { "nope" }, null)).run();
    }

    //
    // POST /form/{id}/contacts
    //
    @Test
    void postContact_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BASE_URL, new String[] { FORM_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postContact_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BASE_URL, new String[] { FORM_ID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postContact_success() {
        EndpointTestBuilder.from(POST_CONTACT_CASE).doPost(generateSample()).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postContact_success_validateSchema() {
        EndpointTestBuilder.from(POST_CONTACT_CASE).doPost(generateSample()).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postContact_success_validateResponseFormat() {
        EndpointTestBuilder.from(POST_CONTACT_CASE).doPost(generateSample()).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postContact_success_validateRequestSchema() {
        String request = generateSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.CONTACT_SCHEMA_PATH).matches(request));

        EndpointTestBuilder.from(POST_CONTACT_CASE).doPost(request).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postContact_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(CONTACTS_BASE_URL, new String[] { "nope" }, null))
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postContact_failure_emptyBody() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BASE_URL, new String[] { FORM_ID }, null)
                        .setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                        .build())
                .doPost(null)
                .run();
    }

    //
    // GET /form/{id}/contacts/{contactId}
    //
    @Test
    void getContactByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BY_ID_URL, new String[] { FORM_ID, FORM_CONTACT_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContactByID_success() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContactByID_success_validateSchema() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContactByID_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContactByID_failure_contactNotFound() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(CONTACTS_BY_ID_URL, new String[] { FORM_ID, "nope" }, null)).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getContactByID_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(CONTACTS_BY_ID_URL, new String[] { "Nope", FORM_CONTACT_ID }, null))
                .run();
    }

    //
    // PUT /form/{id}/contacts/{contactId}
    //
    @Test
    void putContactByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BY_ID_URL, new String[] { FORM_ID, FORM_CONTACT_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putContactByID_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BY_ID_URL, new String[] { FORM_ID, FORM_CONTACT_ID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putContactByID_failure_emptyBody() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BY_ID_URL, new String[] { FORM_ID, FORM_CONTACT_ID }, null)
                        .setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                        .build())
                .doPut(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putContactByID_success() {
        EndpointTestBuilder.from(PUT_CONTACT_CASE).doPut(generateSample()).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putContactByID_success_validateRequestSchema() {
        String request = generateSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.CONTACT_SCHEMA_PATH).matches(request));

        EndpointTestBuilder.from(PUT_CONTACT_CASE).doPut(request).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putContactByID_success_validateSchema() {
        EndpointTestBuilder.from(PUT_CONTACT_CASE).doPut(generateSample()).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putContactByID_success_validateResponseFormat() {
        EndpointTestBuilder.from(PUT_CONTACT_CASE).doPut(generateSample()).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putContactByID_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(CONTACTS_BY_ID_URL, new String[] { "nope", FORM_CONTACT_ID }, null))
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putContactByID_failure_invalidContactId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(CONTACTS_BY_ID_URL, new String[] { FORM_ID, "nope" }, null))
                .doPut(generateSample())
                .run();
    }

    //
    // DELETE /form/{id}/contacts/{contactId}
    //
    @Test
    void deleteContactByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BY_ID_URL, new String[] { DELETE_TEST_FORM_ID, FORM_CONTACT_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteContactByID_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(CONTACTS_BY_ID_URL, new String[] { DELETE_TEST_FORM_ID, FORM_CONTACT_ID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteContactByID_success() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(CONTACTS_BY_ID_URL, new String[] { DELETE_TEST_FORM_ID, "test" }, null))
                .doDelete(null)
                .run();
    }

    private String generateSample() {
        ContactData out = cMap
                .toModel(DtoHelper
                        .generateContact(mockDao.getReference(FORM_ID, MembershipForm.class), Optional.of(ContactTypes.ACCOUNTING)));
        try {
            return om.writeValueAsString(out);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
