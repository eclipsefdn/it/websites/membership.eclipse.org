/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.config;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.membership.application.test.api.MockFoundationAPI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;

/**
 * 
 */
@QuarkusTest
public class EclipseQuteTemplateExtensionsTest {

    @Inject
    @RestClient
    MockFoundationAPI mock;

    @Test
    void getRelationLabelByCode_success() {
        Assertions.assertEquals("Test label", EclipseQuteTemplateExtensions.getRelationLabelByCode("test", mock.relations));
    }

    @Test
    void getRelationLabelByCode_success_caseInsensitive() {
        Assertions.assertEquals("Test label", EclipseQuteTemplateExtensions.getRelationLabelByCode("Test", mock.relations));
    }
    @Test
    void getRelationLabelByCode_success_noMatch() {
        Assertions.assertNull(EclipseQuteTemplateExtensions.getRelationLabelByCode("no match", mock.relations));
    }
}
