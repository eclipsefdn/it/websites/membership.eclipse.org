/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.membership.application.api.FoundationAPI;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import jakarta.annotation.PostConstruct;

@Mock
@RestClient
public class MockFoundationAPI implements FoundationAPI {
    public List<OrganizationContactData> contacts;
    public Map<String, PeopleData> people;
    public List<OrganizationData> orgs;
    public List<SysRelationData> relations;

    @PostConstruct
    public void init() {

        // organizations setup
        this.contacts = new ArrayList<>();
        this.contacts.add(generateOrgContact(1, "da_wizz", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "da_wizz", "CC"));
        this.contacts.add(generateOrgContact(2, "barshal_blathers", "EMPLY"));
        this.contacts.add(generateOrgContact(2, "barshal_blathers", "CC"));
        this.contacts.add(generateOrgContact(1, "grunt", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "grunt", "CC"));
        this.contacts.add(generateOrgContact(3, "opearson", "EMPLY"));
        this.contacts.add(generateOrgContact(3, "opearson", "CC"));
        this.contacts.add(generateOrgContact(1, "doofenshmirtz", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "oouilson", "CC"));

        // people setup
        this.people = new HashMap<>();
        this.people.put("opearson", createSamplePerson("opearson", "Oli", "Pearson"));
        this.people.put("da_wizz", createSamplePerson("da_wizz", "Da", "Wizz"));
        this.people.put("doofenshmirtz", createSamplePerson("doofenshmirtz", "Dr", "Doofenshmirtz"));
        this.people.put("barshal_blathers", createSamplePerson("barshal_blathers", "Barshal", "Blathers"));
        this.people.put("grunt", createSamplePerson("grunt", "Grunt", "Grunt"));
        this.people.put("newbie", createSamplePerson("newbie", "New", "Bie"));

        this.orgs = new ArrayList<>();
        this.orgs.add(generateOrganization(15, "sample"));

        this.relations = new ArrayList<>();
        this.relations.add(new SysRelationData("test", "Test label", true, "", ""));
        this.relations.add(new SysRelationData("test2", "Second Test label", true, "", ""));
    }

    @Override
    public RestResponse<List<OrganizationData>> getAllOrganizations(BaseAPIParameters params) {
        return MockDataPaginationHandler.paginateData(params, orgs);
    }

    @Override
    public RestResponse<List<OrganizationContactData>> getAllOrganizationContacts(Integer orgId, String personId,
            BaseAPIParameters params) {
        return MockDataPaginationHandler.paginateData(params, contacts);
    }

    @Override
    public Optional<PeopleData> getPerson(String personId) {
        return Optional.ofNullable(people.get(personId));
    }

    @Override
    public List<PeopleData> updatePeople(PeopleData person) {
        people.put(person.personID(), person);
        return Arrays.asList(people.get(person.personID()));
    }

    @Override
    public RestResponse<List<SysRelationData>> getSysRelations(BaseAPIParameters baseParams) {
        return MockDataPaginationHandler.paginateData(baseParams, relations);
    }

    private OrganizationData generateOrganization(int orgId, String name) {
        return new OrganizationData(orgId, name, null, null, null, null, new Date(), null, null, new Date());
    }

    private OrganizationContactData generateOrgContact(Integer organizationId, String username, String relation) {
        return new OrganizationContactData(organizationId, username, relation, "", "", "");
    }

    private PeopleData createSamplePerson(String personId, String fname, String lname) {
        return new PeopleData(personId, "", "", "", false, "", "", "", "", "", "", false, false, "", null, null, null, null, null);
    }
}
