/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.resources;

import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;

import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class EmailResourceTest {

    public static final String EMAIL_BASE_URL = "/email";
    public static final String OPT_OUT_NO_PARAM_URL = EMAIL_BASE_URL + "/optout/{id}/{transactionId}";
    public static final String OPT_OUT_URL_NO_TOKEN = OPT_OUT_NO_PARAM_URL + "?email={email}";
    public static final String OPT_OUT_URL_NO_EMAIL = OPT_OUT_NO_PARAM_URL + "?token={token}";
    public static final String OPT_OUT_URL = OPT_OUT_NO_PARAM_URL + "?token={token}&email={email}";

    public static final EndpointTestCase EMAIL_SUCCESS_CASE = TestCaseHelper.buildSuccessCase(
            OPT_OUT_URL, new String[] { "opearson", "666", "other-token", "opearson@sample.co" }, null);

    public static final EndpointTestCase EMAIL_ALREADY_OPTED_CASE = TestCaseHelper.buildSuccessCase(
            OPT_OUT_URL, new String[] { "optedout", "17", "jrr-token", "fake@address.ca" }, null);

    public static final EndpointTestCase TRANSACTION_NOT_FOUND_CASE = TestCaseHelper.buildNotFoundCase(
            OPT_OUT_URL, new String[] { "contact", "70", "token", "opearson@sample.co" }, null);

    public static final EndpointTestCase NO_EMAIL_CASE = TestCaseHelper.buildBadRequestCase(
            OPT_OUT_URL_NO_EMAIL, new String[] { "opearson", "666", "other-token" },
            SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase NO_TOKEN_CASE = TestCaseHelper.buildBadRequestCase(
            OPT_OUT_URL_NO_TOKEN, new String[] { "opearson", "666", "opearson@sample.co" },
            SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase INVALID_TRANSACTION_CASE = TestCaseHelper.buildNotFoundCase(
            OPT_OUT_URL, new String[] { "opearson", "sixety-nine", "other-token", "opearson@sample.co" }, null);

    @Test
    void optout_success() {
        EndpointTestBuilder.from(EMAIL_SUCCESS_CASE).run();
    }

    @Test
    void optout_success_alreadyOpted() {
        EndpointTestBuilder.from(EMAIL_ALREADY_OPTED_CASE).run();
    }

    @Test
    void optout_failure_transactionNotfound() {
        EndpointTestBuilder.from(TRANSACTION_NOT_FOUND_CASE).run();
    }

    @Test
    void optout_failure_missingEmail() {
        EndpointTestBuilder.from(NO_EMAIL_CASE).run();
    }

    @Test
    void optout_failure_missingEmail_validFormat() {
        EndpointTestBuilder.from(NO_EMAIL_CASE).andCheckSchema().run();
    }

    @Test
    void optout_failure_missingToken() {
        EndpointTestBuilder.from(NO_TOKEN_CASE).run();
    }

    @Test
    void optout_failure_missingToken_validFormat() {
        EndpointTestBuilder.from(NO_TOKEN_CASE).andCheckSchema().run();
    }

    @Test
    void optout_failure_invalidTransactionType() {
        EndpointTestBuilder.from(INVALID_TRANSACTION_CASE).run();
    }
}
