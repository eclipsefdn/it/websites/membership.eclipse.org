/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.test.api;

import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.WorkingGroupsAPI;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroupBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationAgreementBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationAgreementsBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationLevelBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupResourcesBuilder;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.smallrye.mutiny.Uni;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;

@Alternative()
@Priority(1)
@ApplicationScoped
@RestClient
public class MockWorkingGroupsAPI implements WorkingGroupsAPI {
    private List<WorkingGroup> wgs;

    public MockWorkingGroupsAPI() {
        this.wgs = Arrays
                .asList(WorkingGroupBuilder
                        .builder()
                        .alias("sdv")
                        .description("description")
                        .levels(Arrays
                                .asList(WorkingGroupWorkingGroupParticipationLevelBuilder.builder().description("sample").relation("WGAPS").build()))
                        .logo("http://sample.com")
                        .parentOrganization("eclipse")
                        .resources(WorkingGroupWorkingGroupResourcesBuilder
                                .builder()
                                .charter("")
                                .contactForm("")
                                .members("members")
                                .website("website")
                                .sponsorship("sponsorship")
                                .participationAgreements(WorkingGroupWorkingGroupParticipationAgreementsBuilder
                                        .builder()
                                        .individual(WorkingGroupWorkingGroupParticipationAgreementBuilder
                                                .builder()
                                                .documentId("32c5a745df264aa2a809")
                                                .pdf("pdf.link")
                                                .build())
                                        .build())
                                .build())
                        .status("active")
                        .title("SDV")
                        .build(),
                        WorkingGroupBuilder
                                .builder()
                                .alias("ecri")
                                .description("description")
                                .levels(Arrays
                                        .asList(WorkingGroupWorkingGroupParticipationLevelBuilder
                                                .builder()
                                                .description("sample")
                                                .relation("WGAPS")
                                                .build(),
                                                WorkingGroupWorkingGroupParticipationLevelBuilder
                                                        .builder()
                                                        .description("sample")
                                                        .relation("ABCDE")
                                                        .build()))
                                .logo("http://sample.com")
                                .parentOrganization("eclipse")
                                .resources(WorkingGroupWorkingGroupResourcesBuilder
                                        .builder()
                                        .charter("")
                                        .contactForm("")
                                        .members("members")
                                        .website("website")
                                        .sponsorship("sponsorship")
                                        .participationAgreements(WorkingGroupWorkingGroupParticipationAgreementsBuilder
                                                .builder()
                                                .individual(WorkingGroupWorkingGroupParticipationAgreementBuilder
                                                        .builder()
                                                        .documentId("32c5a745df264aa2a809")
                                                        .pdf("pdf.link")
                                                        .build())
                                                .build())
                                        .build())
                                .status("active")
                                .title("ECRI")
                                .build(),
                                WorkingGroupBuilder
                                .builder()
                                .alias("dataspace")
                                .description("description")
                                .levels(Arrays
                                        .asList(WorkingGroupWorkingGroupParticipationLevelBuilder
                                                .builder()
                                                .description("sample")
                                                .relation("WGSD")
                                                .build()))
                                .logo("http://sample.com")
                                .parentOrganization("eclipse")
                                .resources(WorkingGroupWorkingGroupResourcesBuilder
                                        .builder()
                                        .charter("")
                                        .contactForm("")
                                        .members("members")
                                        .website("website")
                                        .sponsorship("sponsorship")
                                        .participationAgreements(WorkingGroupWorkingGroupParticipationAgreementsBuilder
                                                .builder()
                                                .individual(WorkingGroupWorkingGroupParticipationAgreementBuilder
                                                        .builder()
                                                        .documentId("32c5a745df264aa2a809")
                                                        .pdf("pdf.link")
                                                        .build())
                                                .build())
                                        .build())
                                .status("active")
                                .title("dataspace")
                                .build());
    }

    @Override
    public Uni<RestResponse<List<WorkingGroup>>> get(BaseAPIParameters baseParams, List<String> statuses) {
        return Uni
                .createFrom()
                .item(MockDataPaginationHandler
                        .paginateData(baseParams,
                                wgs.stream().filter(wg -> statuses == null || statuses.contains(wg.status())).toList()));
    }
}
