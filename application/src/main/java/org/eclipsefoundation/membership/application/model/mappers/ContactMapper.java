/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.model.mappers;

import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.model.ContactData;
import org.eclipsefoundation.persistence.config.QuarkusMappingConfig;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.mapper.BaseEntityMapper;
import org.mapstruct.Context;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class)
public interface ContactMapper extends BaseEntityMapper<Contact, ContactData> {

    @Mapping(source = "form.id", target = "formId")
    @Mapping(source = "lName", target = "lastName")
    @Mapping(source = "fName", target = "firstName")
    @Mapping(source = "title", target = "jobTitle")
    ContactData toModel(Contact dtoEntity);

    @Mapping(target = "cloneTo", ignore = true)
    @InheritInverseConfiguration
    Contact toDTO(ContactData modelEntity, @Context PersistenceDao repo);
}
