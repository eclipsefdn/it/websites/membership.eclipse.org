/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.request;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipsefoundation.membership.application.config.ApplicationBaseConfig;
import org.eclipsefoundation.membership.application.config.FormStateConfig;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.jboss.resteasy.reactive.server.ServerRequestFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.identity.SecurityIdentity;
import io.vertx.core.http.HttpMethod;
import jakarta.enterprise.inject.Instance;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.ext.Provider;

/**
 * Stops requests to update form objects when the form has already been submitted. Currently reacts to all mutation events under /form/{id}.
 * 
 * @author Martin Lowe
 *
 */
@Provider
public class FormStateFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(FormStateFilter.class);

    private static final Pattern SPECIFIC_FORM_URI_PATTERN = Pattern.compile("^\\/form\\/((?!state)[^\\/]+)\\/?.*");

    private final Instance<ApplicationBaseConfig> baseConfig;
    private final Instance<FormStateConfig> filterConfig;
    private final DefaultHibernateDao dao;
    private final SecurityIdentity identity;

    public FormStateFilter(Instance<ApplicationBaseConfig> baseConfig, Instance<FormStateConfig> filterConfig, DefaultHibernateDao dao,
            SecurityIdentity identity) {
        this.baseConfig = baseConfig;
        this.filterConfig = filterConfig;
        this.dao = dao;
        this.identity = identity;
    }

    @ServerRequestFilter
    public void filter(ContainerRequestContext requestContext, UriInfo uriInfo) {
        // if current logged in user is an admin, do not stop request
        if (filterConfig.get().allowedAdminRoles().stream().anyMatch(identity::hasRole)) {
            return;
        }

        // check if path indicates a specific form
        Matcher m = SPECIFIC_FORM_URI_PATTERN.matcher(uriInfo.getPath());
        if (m.matches()) {
            // get form object to check if it has been submitted
            String formID = m.group(1);

            // get the forms matching ID and check if request should continue
            MembershipForm results = dao.getReference(formID, MembershipForm.class);
            if (Boolean.TRUE.equals(filterConfig.get().enabled()) && results != null
                    && !baseConfig.get().applicationGroup().equals(results.getApplicationGroup())) {
                LOGGER
                        .error("Form with ID '{}' cannot be accessed as it belongs to another group. ('{}'){}",
                                TransformationHelper.formatLog(formID), results.getApplicationGroup(), results);
                throw new BadRequestException("Form cannot be accessed as it belongs to another group");
            } else if (isMutationAction(requestContext) && results != null && !FormState.INPROGRESS.equals(results.getState())) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER
                            .debug("Form with ID '{}' was not updated as it is not in progress. ('{}')",
                                    TransformationHelper.formatLog(formID), results.getState());
                }

                throw new BadRequestException("Form should not be updated if it is not inprogress");
            }
        }
    }

    private boolean isMutationAction(ContainerRequestContext requestContext) {
        String httpMethod = requestContext.getMethod();
        return httpMethod.equalsIgnoreCase(HttpMethod.POST.toString()) || httpMethod.equalsIgnoreCase(HttpMethod.PUT.toString())
                || httpMethod.equalsIgnoreCase(HttpMethod.DELETE.toString());
    }
}
