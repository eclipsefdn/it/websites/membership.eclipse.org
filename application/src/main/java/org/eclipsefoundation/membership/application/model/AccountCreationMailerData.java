/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.model;

import org.eclipsefoundation.membership.application.dto.membership.BaseContact;
import org.eclipsefoundation.membership.application.namespace.TransactionType;

import io.quarkus.qute.TemplateData;

@TemplateData
public class AccountCreationMailerData {
    public final String applicantName;
    public final BaseContact memberRep;
    public final TransactionType type;

    public AccountCreationMailerData(String applicantName, BaseContact memberRep, TransactionType type) {
        this.applicantName = applicantName;
        this.memberRep = memberRep;
        this.type = type;
    }
}
