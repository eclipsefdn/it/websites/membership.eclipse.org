/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.model;

import jakarta.annotation.Nullable;

import org.eclipsefoundation.membership.application.namespace.ContactTypes;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ContactData.Builder.class)
public abstract class ContactData {
    @Nullable
    public abstract String getId();
    @Nullable
    public abstract String getFormId();
    public abstract String getFirstName();
    public abstract String getLastName();
    public abstract String getJobTitle();
    public abstract String getEmail();
    public abstract ContactTypes getType();
    
    public static Builder builder() {
        return new AutoValue_ContactData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(@Nullable String id);
        public abstract Builder setFormId(@Nullable String formId);
        public abstract Builder setFirstName(String firstName);
        public abstract Builder setLastName(String lastName);
        public abstract Builder setJobTitle(String jobTitle);
        public abstract Builder setEmail(String email);
        public abstract Builder setType(ContactTypes type);
        public abstract ContactData build();
    }
}
