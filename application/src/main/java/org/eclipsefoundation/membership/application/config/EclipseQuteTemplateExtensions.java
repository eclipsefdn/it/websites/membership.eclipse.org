/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.config;

import java.util.List;

import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.membership.application.dto.membership.BaseContact;

import io.quarkus.qute.TemplateExtension;

@TemplateExtension
public class EclipseQuteTemplateExtensions {

    /**
     * Checks whether the contact is listed as a prospective user. Returns an asterisk if the contact is a propective user. Otherwise
     * returns an empty string.
     * 
     * How to use: {contact.isProspectiveUser(prospectiveUsers)}
     * 
     * @param contact The current contact
     * @param prospectiveUsers The prospective users
     * @return An asterisk if contact is a prospective user. Otherwise returns an empty string
     */
    static String isProspectiveUser(BaseContact contact, List<BaseContact> prospectiveUsers) {
        return prospectiveUsers.stream().noneMatch(u -> u.getEmail().equalsIgnoreCase(contact.getEmail())) ? "" : "*";
    }

    static String getWorkingGroupTitleByAlias(String alias, List<WorkingGroup> workingGroups) {
        return workingGroups
                .stream()
                .filter(wg -> wg.alias().equalsIgnoreCase(alias))
                .map(WorkingGroup::title)
                .findFirst()
                .orElse(null);
    }

    /**
     * Using the base internal code and the list of available relations, convert the code to the human-readable label.
     * 
     * @param code the internal code for the relation
     * @param relations list of relations from the Foundation DB
     * @return the label for the given code if available, otherwise null
     */
    static String getRelationLabelByCode(String code, List<SysRelationData> relations) {
        return relations
                .stream()
                .filter(wg -> wg.relation().equalsIgnoreCase(code))
                .map(SysRelationData::description)
                .findFirst()
                .orElse(null);
    }

    private EclipseQuteTemplateExtensions() {
    }
}
