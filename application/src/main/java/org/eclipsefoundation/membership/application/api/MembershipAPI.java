/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.api;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.membership.application.api.models.MemberOrganizationRecord.MemberOrganization;

import io.quarkus.vertx.http.Compressed;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

@Path("api")
@RegisterRestClient(configKey = "membership-api")
public interface MembershipAPI {

    @GET
    @Path("organizations/{id}")
    @Compressed
    MemberOrganization getOrganization(@PathParam("id") int organizationId);
}
