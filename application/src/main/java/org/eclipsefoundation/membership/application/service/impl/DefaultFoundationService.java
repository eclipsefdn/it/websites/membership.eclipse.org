/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service.impl;

import java.util.Collections;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;
import org.eclipsefoundation.membership.application.api.FoundationAPI;
import org.eclipsefoundation.membership.application.service.FoundationService;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MultivaluedHashMap;

/**
 * Base implementation for foundation service, using internal cache and pagination 
 */
@ApplicationScoped
public class DefaultFoundationService implements FoundationService {

    private final CachingService cache;
    private final FoundationAPI fdn;
    private final APIMiddleware middle;

    public DefaultFoundationService(CachingService cache, APIMiddleware middle, @RestClient FoundationAPI fdn) {
        this.cache = cache;
        this.fdn = fdn;
        this.middle = middle;
    }

    @Override
    public List<SysRelationData> getRelations() {
        return cache
                .get("all", new MultivaluedHashMap<>(), SysRelationData.class, () -> middle.getAll(fdn::getSysRelations))
                .data()
                .orElse(Collections.emptyList());
    }
}
