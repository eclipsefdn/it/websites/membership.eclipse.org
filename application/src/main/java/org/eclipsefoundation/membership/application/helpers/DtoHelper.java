/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.helpers;

import java.util.List;

import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.ws.rs.core.MultivaluedMap;

/**
 * 
 */
public final class DtoHelper {

    public static ParameterizedSQLStatement getBaseStatementNumericIds(MultivaluedMap<String, String> params, boolean isRoot, DtoTable table,
            ParameterizedSQLStatementBuilder builder) {
        ParameterizedSQLStatement stmt = builder.build(table);
        if (isRoot) {
            // ID check
            String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
            if (id != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(table.getAlias() + ".id = ?", new Object[] { Long.parseLong(id) }));
            }
        }
        // IDs check
        List<String> ids = params.get(DefaultUrlParameterNames.IDS.getName());
        if (ids != null && !ids.isEmpty()) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause(table.getAlias() + ".id IN ?",
                            new Object[] { ids.stream().map(Long::parseLong).toList() }));
        }
        return stmt;
    }
    
    public static ParameterizedSQLStatement getBaseStatement(MultivaluedMap<String, String> params, boolean isRoot, DtoTable table,
            ParameterizedSQLStatementBuilder builder) {
        ParameterizedSQLStatement stmt = builder.build(table);
        if (isRoot) {
            // ID check
            String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
            if (id != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(table.getAlias() + ".id = ?", new Object[] { id }));
            }
        }
        // IDs check
        List<String> ids = params.get(DefaultUrlParameterNames.IDS.getName());
        if (ids != null && !ids.isEmpty()) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause(table.getAlias() + ".id IN ?",
                            new Object[] { ids }));
        }
        return stmt;
    }
    
    public static ParameterizedSQLStatement getBaseFormStatement(MultivaluedMap<String, String> params, boolean isRoot, DtoTable table,
            ParameterizedSQLStatementBuilder builder) {
        ParameterizedSQLStatement stmt = getBaseStatement(params, isRoot, table, builder);
        // form ID check
        String formId = params.getFirst(MembershipFormAPIParameterNames.FORM_ID.getName());
        if (formId != null) {
            stmt.addClause(new ParameterizedSQLStatement.Clause(table.getAlias() + ".form.id = ?", new Object[] { formId }));
        }
        // form IDs check
        List<String> formIds = params.get(MembershipFormAPIParameterNames.FORM_IDS.getName());
        if (formIds != null) {
            stmt.addClause(new ParameterizedSQLStatement.Clause(table.getAlias() + ".form.id IN ?", new Object[] { formIds }));
        }
        return stmt;
    }

    private DtoHelper() {
    }
}
