/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.resources;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.http.namespace.RequestHeaderNames;

import io.quarkus.security.Authenticated;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Handles OIDC routing for the request.
 *
 * @author Martin Lowe
 */
@Path("")
public class OIDCResource extends AbstractRESTResource {

    @ConfigProperty(name = "eclipse.membership.base-url", defaultValue = "/")
    String baseUrl;

    @GET
    @Authenticated
    @Path("/login")
    public Response routeLogin() throws URISyntaxException {
        return redirect(baseUrl);
    }

    /**
     * While OIDC plugin takes care of actual logout, a route is needed to properly reroute anon user to home page.
     *
     * @throws URISyntaxException
     */
    @GET
    @Path("/logout")
    public Response routeLogout() throws URISyntaxException {
        return redirect(baseUrl);
    }

    @GET
    @Path("userinfo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserInfo(@HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {

        if (!ident.isAnonymous()) {
            // cast the principal to a JWT token (which is the type for OIDC)
            DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
            // create wrapper around data for output and return
            return Response
                    .ok(new InfoWrapper(defaultPrin.getName(), defaultPrin.getClaim("given_name"), defaultPrin.getClaim("family_name"),
                            defaultPrin.getClaim("email"), defaultPrin.getExpirationTime()))
                    .build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("csrf")
    public Response generateCSRF() {
        return Response.ok().build();
    }

    private Response redirect(String location) throws URISyntaxException {
        return Response.temporaryRedirect(new URI(location)).build();
    }

    public static record InfoWrapper(String name, String givenName, String familyName, String email, long exp) {
    }
}
