/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service;

import java.util.List;

import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationData;

/**
 * Simple service to help cache some foundation operation results
 */
public interface FoundationService {

    /**
     * Retrieve full list of relations available to the system using pagination + caching.
     * 
     * @return list of relation data, or empty list if none can be retrieved.
     */
    List<SysRelationData> getRelations();
}
