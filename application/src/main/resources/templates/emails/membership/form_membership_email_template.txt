{#include emails/membership_header /}
New form submission by: {data.name} ({data.form.userID})

Organization
Name: {data.org.legalName}
Twitter: {data.org.twitterHandle}
Requires purchase order: {data.form.purchaseOrderRequired}
VAT number: {data.form.vatNumber}  
VAT Registration country: {data.form.registrationCountry}  
Aggregate Revenue: {data.org.aggregateRevenue}
Employee count: {data.org.employeeCount}
Organization Type: {data.org.organizationType}
 
Address
{data.org.address.addressLine1}
{data.org.address.addressLine2}
{data.org.address.locality}, {data.org.address.administrativeArea}
{data.org.address.country}
{data.org.address.postalCode}
  

Key Contacts

NOTE: To facilitate interactions with Eclipse Foundation, all key contacts listed below agree to maintain an Eclipse account (which can be created here<https://accounts.eclipse.org/user/register>), and agree to the Eclipse.org Terms of Use<https://www.eclipse.org/legal/terms-of-use/>,Privacy Policy<https://www.eclipse.org/legal/privacy/>, and Code of Conduct<https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php>.


Contacts
{#for contact in data.contacts.orEmpty}

Name: {contact.fName} {contact.lName}
Email: {contact.email}{contact.isProspectiveUser(data.prospectiveUsers.orEmpty)} 
Title: {contact.title}
Contact type: {contact.type}
userID: {contact.id}
{/for}

*: Indicates an email address for which an Eclipse Foundation account cannot be found. This user will receive an
email to reset their password on the account generated for them by the Eclipse Foundation.

Working Groups

{#for wg in data.wgs.orEmpty}
Working Group: {wg.workingGroupID}
Participation Level: {wg.participationLevel}
Effective Date: {wg.effectiveDate}
Contact Name: {wg.contact.fName} {wg.contact.lName}

{/for}
{#include emails/membership_footer /}