
Form submission by {application.user} for {organization.name}({organization.organizationID})

Working Groups
{#for contact in contacts.orEmpty}

Working Group: {contact.workingGroupAlias ? contact.workingGroupAlias.getWorkingGroupTitleByAlias(working_groups) : 'N/A'}
Participation Level: {#if contact.workingGroupLevel}{contact.workingGroupLevel.getRelationLabelByCode(relations)} ({contact.workingGroupLevel}){#else}N/A{/if}
Contact: {contact.fName} {contact.lName} ({contact.email}){contact.isProspectiveUser(prospectiveUsers.orEmpty)}. Title: {contact.jobTitle}
{/for}

*: Indicates an email address for which an Eclipse Foundation account cannot be found. This user will receive an
email to reset their password on the account generated for them by the Eclipse Foundation.