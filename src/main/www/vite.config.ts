import { defineConfig } from 'vite';
import path from 'path';
import react from '@vitejs/plugin-react';
import environment from 'vite-plugin-environment';

export default defineConfig({
  base: '/',
  build: {
    outDir: './build',
    target: 'es2015',
    sourcemap: false,
  },
  server: {
    port: 3000,
  },
  resolve: {
    alias: {
      '@components': path.resolve(__dirname, './src/components'),
    }
  },
  plugins: [
    react(),
    // Ensures that we can access env variables with process.env instead of
    // import.meta.env. This is needed for Jest compatibility.
    environment(['NODE_ENV']),
  ],
});
