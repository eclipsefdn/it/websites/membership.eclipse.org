/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import useForm from './useForm';
import useHashScrolling from './useHashScrolling';
import useImageFile from './useImageFile';
import { RootState, AppDispatch } from '../store';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'; 

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export { 
  useForm, 
  useHashScrolling,
  useImageFile,
};


