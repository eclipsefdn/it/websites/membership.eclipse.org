/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useField } from 'formik';

/** A custom hook which returns the error and touched state of a form field. */
const useFormField = (fieldName: string = ''): { error?: string, touched: boolean } => {
  const [,meta] = useField(fieldName);
  return meta;
};

export default useFormField;
