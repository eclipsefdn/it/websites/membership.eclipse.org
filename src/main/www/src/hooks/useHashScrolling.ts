/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

/**
  * A custom hook which adds smooth scrolling to the hash of the current URL.
  * Calling this hook will automatically support scrolling to ids on the page.
  *
  * @example
  * const MyComponent = () => {
  *   useHashScrolling();
  *   return (
  *     <div>
  *       <h1 id="my-heading">My Heading</h1>
  *     </div>
  *   );
  * }
  */
const useHashScrolling = () => {
  const history = useHistory();

  useEffect(() => {
    const scrollToHash = () => {
      // Get the hash id from the URL without the query string.
      const hash = history.location.hash.split('?')[0];
      if (!hash) return;

      // Scroll to the element with the id from the hash.
      const element = document.querySelector(hash);
      if (element) element.scrollIntoView({ behavior: 'smooth' });
    };

    scrollToHash();

    // Listen for changes to the history and scroll to the hash when it
    // changes.
    const unlisten = history.listen(scrollToHash);

    // Call the unlisten function on cleanup.
    return unlisten;
  }, [history]);
};

export default useHashScrolling;
