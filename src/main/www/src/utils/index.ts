/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import _ from 'lodash';
import { User, OrganizationalRole } from '../types';
import { LOGIN_FROM_KEY } from '../Constants/Constants';
import permissions from '../Constants/permissions';
import { checkPermission } from '../utils/portalFunctionHelpers';

interface SelectRandomOptions {
  min?: number; 
  max?: number;
}

export const selectRandom = <T>(array: T[], options: SelectRandomOptions | number): T[] | T => {
  let min = 1;
  let max = 1;
  
  // If the second argument is a number, it is the amount of elements we want
  // to pick.
  if (typeof options === 'number') {
    max = options;
    min = options;
  }

  const count = min === max ? max : Math.floor(Math.random() * (max - min + 1)) + min;
  
  if (count === 1) {
    return array[Math.floor(Math.random() * array.length)];
  }

  // Randomly shuffle the array and grab the first N elements, where N is
  // `count`.
  const shuffled = array.slice().sort(() => Math.random() - 0.5);
  return shuffled.slice(0, count);
};

/** Make a deep merge of one or more objects.
  *
  * @param destinationObject - The destination object.
  * @param [sources] - The source objects
  * @returns A deeply merged object.
  */
export const deepMerge = (destinationObject: any, ...sources: any[]): any => {
  return _.merge(destinationObject, ...sources);
};


/** Generates a function which creates a React key based on the object
  * reference. This should only be used when you have no unique identifiers in
  * your list elements.
  */
const createKeyGenerator = () => {
  const counts = new WeakMap()
  
  /** Generates a React key based on the given object's reference. This should
    * only be used when you have no unique identifiers to work with.
    */
  return (key: any): React.Key => {
    const newCount = (counts.get(key) ?? 0) + 1
    counts.set(key, newCount)
    return `${key.toString()}-${newCount}`
  }
};

export const generateKey = createKeyGenerator();

/** Formats a date into a long date string. e.g. 1 January 1970 
  * @param date - The date to format
  * @returns A formatted date string
  */
export const formatToLongDate = (date: Date | string): string => {
  const dateObject = typeof date === 'string' ? new Date(date) : date;

  const day = dateObject.getDate();
  const month = dateObject.toLocaleString('default', { month: 'long' });
  const year = dateObject.getFullYear();

  return `${day} ${month} ${year}`;
}


// A set of organizational roles we care to display in the contact management
// table.
export const applicableRoles = new Set<OrganizationalRole>(['CR', 'CRA', 'DE', 'MA', 'CM', 'CN']);

// A set of organizational roles that we allow users to edit.
export const editableRoles = new Set<OrganizationalRole>(['CR', 'CRA', 'DE', 'MA']);

/** Filters out roles which do not matter in the context of the portal. */
export const applicableRolesFilter = (role: OrganizationalRole) => {
  return applicableRoles.has(role);
}

export type LoginEntryPoint = 'portal' | 'form' | 'ready-to-join';

/** Sends the user to the API's login endpoint. */
export const login = (from: LoginEntryPoint) => {
  // Do not redirect to /api/login in dev mode. Otherwise, we'll lose our
  // connection to MSW .
  if (process.env.NODE_ENV === 'development') {
    return;
  }

  localStorage.setItem(LOGIN_FROM_KEY, from);
  window.location.assign('/api/login');
};

/**
  * Determines whether the user is authenticated and authorized to access the
  * membership portal. 
  *
  * @param user - the user to test 
  * @returns true or false whether or not they are authorized
  */
export const isUserAuthorizedToAccessPortal = (user: User): boolean => {
  return checkPermission(user.roles, permissions.accessPortal) || checkPermission(user.relation, permissions.accessPortal);
}

