/*!
 * Copyright (c) 2021, 2022, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { errMsgForGetRequest, errMsgForSessionExpired } from '../Constants/Constants';

/**
 * This will randomly pick a certain amount of items from a specified array.
 * 
 * @param originalData - The array which contains the items need to be randomly picked from.
 * @param i - The amount of items that need to be randomly picked.
 **/
export const pickRandomItems = (originalData: Array<any>, i: number) => {
  // Make sure the loop times will be always less than or equal to the length of the data array
  i = originalData.length < i ? originalData.length : i;
  let tempArray = originalData.map((item) => item);
  const randomItems = [];
  while (i--) {
    const randomIndex = Math.floor(Math.random() * tempArray.length);
    randomItems.push(tempArray[randomIndex]);
    tempArray.splice(randomIndex, 1);
  }
  return randomItems;
};

/**
 * This checks if current user has permission for a certain page, section and or action. Will return true if yes, and false if no.
 * 
 * @param rolesCanAccess - An array of roles that have permission of a certain page, section and or action.
 * @param userRoles - An array of roles of current user.
 **/
export const checkPermission = (rolesCanAccess: Array<string>, userRoles: Array<string>) => 
  userRoles && (
    userRoles.some((role) => rolesCanAccess.includes(role))
  );

/**
 * This renders the helper text for table component. 
 * It will return "No data" if the "data" is empty and return a predefined error message if there is any error related to the fetch.
 * 
 * @param data - The data value for the table.
 * @param isFetching - A boolean which indicates whether it's still fetching or not.
 **/
export const renderTableHelperText = (data: Array<any> | null, isFetching: boolean) => {
  if (data && data.length === 0) {
    return 'No data';
  }

  if (data === null && !isFetching) {
    return errMsgForGetRequest;
  }
};

/**
 * This will show error messages based on different error codes. 
 * When the "displayPeriod" is set to 0, it will use the default value which is 4 seconds for displaying the message.
 * 
 * @param errCode - Error code got from the response of a fetch.
 * @param errHandler - The callback function for displaying error message for a specified time period.
 * @param customErrMsg - Custom error message.
 **/
export const showErrMsg = (
  errCode: string | number,
  errHandler: (errMsg: string, displayPeriod: number) => void,
  customErrMsg?: string | undefined
) => {
  switch (errCode) {
    case 403:
      errHandler(errMsgForSessionExpired, 8000);
      break;
    case 413:
      errHandler(customErrMsg || '', 0);
      break;
    default:
      errHandler('', 0);
      break;
  }
};

/**
 * This will check if current browser is IE and will return true if so, and return false if not.
 **/
export const isIE = () => navigator.userAgent.indexOf('MSIE') > -1 || navigator.userAgent.indexOf('Trident') > -1;

