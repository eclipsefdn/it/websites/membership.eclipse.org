/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import {
  FETCH_METHOD,
  CONTACT_TYPE,
  API_PREFIX_FORM,
  FETCH_HEADER,
  getCurrentMode,
  MODE_REACT_ONLY,
  MODE_REACT_API,
  api_prefix,
  HAS_TOKEN_EXPIRED,
  FETCH_HEADER_WITHOUT_CSRF,
  application_api_prefix,
} from '../Constants/Constants';

export const isProd = window.location.href.includes('//membership.eclipse.org/');

/**
 * checkSameContact
 *
 * @param compnayRep - The company representative contact info object
 * @param otherContact - The representative contact info object that used to compare with company representative, which normally are
 * marketing representative, and accounting representative
 */
function checkSameContact(compnayRep, otherContact) {
  if (!otherContact || !compnayRep) {
    return false;
  }

  const keyArray = Object.keys(compnayRep);

  // Check contacts' name, email and jobtitle to check if they are the same
  for (let i = 0; i < keyArray.length; i++) {
    if (keyArray[i] !== 'id' && keyArray[i] !== 'type' && compnayRep[keyArray[i]] !== otherContact[keyArray[i]]) {
      return false;
    }
  }

  return true;
}

/**
 * Returns an empty string if the input value is nullish
 * @param {*} potentialNullValue 
 * @returns The parameter value if not nullish, otherwise ''
 */
function assignEmptyStringIfNullish(potentialNullValue) {
  return potentialNullValue || '';
}

/**
 * @param currentContact - The representative contact info object that used to compare with company representative, which normally are
 * marketing representative, and accounting representative
 * @param companyContact - The company representative contact info object
 */
export function assignContactData(currentContact, companyContact) {
  currentContact.firstName = companyContact.firstName;
  currentContact.lastName = companyContact.lastName;
  currentContact.jobtitle = companyContact.jobtitle;
  currentContact.email = companyContact.email;
}

//== Transform data from backend to match my form model

/**
 * Notes:
 * The match data functions look repeated on the properties, because of the naming convention is somehow different in JS and the data object retrieved from backend. JS uses camelCase.
 * Another reason I am making complicated `legalName` and `country` match, is due to the library React-Select. It needs to use the `{label: foo, value: foo}` format to be able shown on the select input fields.
 * Please refer to: https://github.com/formium/formik/discussions/2954
 * and https://github.com/JedWatson/react-select/issues/3761
 * and https://github.com/JedWatson/react-select/issues/4321
 *
 * https://github.com/JedWatson/react-select/issues/3761 shows how you can use without mapping as a label, but I wanted to use the label to be shown in the preview, thus, still used { label: value: } format.
 *
 */

/**
 * @param existingOrganizationData -
 * Existing Organization data, fetched from server
 */
export function matchCompanyFields(existingOrganizationData) {
  return {
    // Step1: company Info
    id: assignEmptyStringIfNullish(existingOrganizationData?.id),
    legalName: assignEmptyStringIfNullish(existingOrganizationData?.legal_name),
    revenue: assignEmptyStringIfNullish(existingOrganizationData?.aggregate_revenue),
    employeeCount: assignEmptyStringIfNullish(existingOrganizationData?.employee_count),
    type: assignEmptyStringIfNullish(existingOrganizationData?.organization_type),
    address: {
      id: assignEmptyStringIfNullish(existingOrganizationData?.address?.id),
      street: assignEmptyStringIfNullish(existingOrganizationData?.address?.address_line_1),
      streetTwo: assignEmptyStringIfNullish(existingOrganizationData?.address?.address_line_2),
      city: assignEmptyStringIfNullish(existingOrganizationData?.address?.locality),
      provinceOrState: assignEmptyStringIfNullish(existingOrganizationData?.address?.administrative_area),
      country: assignEmptyStringIfNullish(existingOrganizationData?.address?.country),
      'country-label': {
        label: assignEmptyStringIfNullish(existingOrganizationData?.address?.country),
        value: assignEmptyStringIfNullish(existingOrganizationData?.address?.country),
      },
      postalCode: assignEmptyStringIfNullish(existingOrganizationData?.address?.postal_code),
    },
    twitterHandle: assignEmptyStringIfNullish(existingOrganizationData?.twitter),
  };
}

/**
 * @param existingPurchasingAndVATData -
 * Existing purchasing process and VAT data, fetched from server
 */
export function mapPurchasingAndVAT(existingPurchasingAndVATData) {
  return {
    // Step1: purchasing process and VAT Info
    id:assignEmptyStringIfNullish(existingPurchasingAndVATData?.id),
    isRegistered: !!existingPurchasingAndVATData?.registration_country,
    purchasingProcess: assignEmptyStringIfNullish(existingPurchasingAndVATData?.purchase_order_required),
    vatNumber: assignEmptyStringIfNullish(existingPurchasingAndVATData?.vat_number),
    countryOfRegistration: assignEmptyStringIfNullish(existingPurchasingAndVATData?.registration_country),
  };
}

/**
 * @param existingContactData -
 * Existing Contacts data, fetched from server
 * **/
export function matchContactFields(existingContactData) {
  let existingCompanyContact = existingContactData.find((el) => el?.type === CONTACT_TYPE.COMPANY);
  let existingMarketingContact = existingContactData.find((el) => el?.type === CONTACT_TYPE.MARKETING);
  let existingAccoutingContact = existingContactData.find((el) => el?.type === CONTACT_TYPE.ACCOUNTING);
  let existingSigningContact = existingContactData.find((el) => el?.type === CONTACT_TYPE.SIGNING);

  return {
    organizationContacts: {
      member: {
        id: assignEmptyStringIfNullish(existingCompanyContact?.id),
        firstName: assignEmptyStringIfNullish(existingCompanyContact?.first_name),
        lastName: assignEmptyStringIfNullish(existingCompanyContact?.last_name),
        jobtitle: assignEmptyStringIfNullish(existingCompanyContact?.job_title),
        email: assignEmptyStringIfNullish(existingCompanyContact?.email),
      },

      marketing: {
        id: assignEmptyStringIfNullish(existingMarketingContact?.id),
        firstName: assignEmptyStringIfNullish(existingMarketingContact?.first_name),
        lastName: assignEmptyStringIfNullish(existingMarketingContact?.last_name),
        jobtitle: assignEmptyStringIfNullish(existingMarketingContact?.job_title),
        email: assignEmptyStringIfNullish(existingMarketingContact?.email),
        sameAsCompany: checkSameContact(existingCompanyContact, existingMarketingContact),
      },

      accounting: {
        id: assignEmptyStringIfNullish(existingAccoutingContact?.id),
        firstName: assignEmptyStringIfNullish(existingAccoutingContact?.first_name),
        lastName: assignEmptyStringIfNullish(existingAccoutingContact?.last_name),
        jobtitle: assignEmptyStringIfNullish(existingAccoutingContact?.job_title),
        email: assignEmptyStringIfNullish(existingAccoutingContact?.email),
        sameAsCompany: checkSameContact(existingCompanyContact, existingAccoutingContact),
      },
    },

    signingAuthorityRepresentative: {
      id: assignEmptyStringIfNullish(existingSigningContact?.id),
      firstName: assignEmptyStringIfNullish(existingSigningContact?.first_name),
      lastName: assignEmptyStringIfNullish(existingSigningContact?.last_name),
      jobtitle: assignEmptyStringIfNullish(existingSigningContact?.job_title),
      email: assignEmptyStringIfNullish(existingSigningContact?.email),
      sameAsCompany: checkSameContact(existingCompanyContact, existingSigningContact),
    },
  };
}

/**
 * @param existingworkingGroupData -
 * Existing working groups data, fetched from server
 * @param workingGroupsOptions -
 * Options of working groups to select, fetched from server
 * @param existingCompanyContact
 * The info of company rep in step 1
 */
export function matchWorkingGroupFields(existingworkingGroupData, workingGroupsOptions, existingCompanyContact) {
  var res = [];
  // Array
  existingworkingGroupData.forEach((item) => {
    let wg = workingGroupsOptions?.find((el) => el.label === item?.working_group);
    const basicRepInfo = {
      firstName: assignEmptyStringIfNullish(item?.contact?.first_name),
      lastName: assignEmptyStringIfNullish(item?.contact?.last_name),
      jobtitle: assignEmptyStringIfNullish(item?.contact?.job_title),
      email: assignEmptyStringIfNullish(item?.contact?.email),
    };
    res.push({
      id: assignEmptyStringIfNullish(item?.id),
      workingGroup:
        {
          label: wg?.label,
          value: item?.working_group,
          participation_levels: wg?.participation_levels,
        } || '',
      participationLevel: assignEmptyStringIfNullish(item?.participation_level),
      effectiveDate: assignEmptyStringIfNullish(item?.effective_date?.substring(0, 10)),
      workingGroupRepresentative: {
        ...basicRepInfo,
        id: assignEmptyStringIfNullish(item?.contact?.id),
        sameAsCompany: checkSameContact(existingCompanyContact, basicRepInfo),
      },
    });
  });

  return res;
}

//== Transform data from my form model to PUT or POST for backend

/**
 * @param organizationData -
 * Filled Organization data, stored in formik context
 * @param formId -
 * Form Id fetched from the server, sotored in membership context, used for calling APIs
 * **/
export function matchCompanyFieldsToBackend(organizationData, formId) {
  var org = {
    address: {
      locality: organizationData.address.city,
      country: organizationData.address.country,
      postal_code: assignEmptyStringIfNullish(organizationData.address.postalCode),
      administrative_area: assignEmptyStringIfNullish(organizationData.address.provinceOrState),
      address_line_1: organizationData.address.street,
      address_line_2: organizationData.address.streetTwo,
    },
    form_id: formId,
    id: organizationData.id,
    legal_name: organizationData.legalName,
    twitter: assignEmptyStringIfNullish(organizationData.twitterHandle),
    aggregate_revenue: organizationData.revenue,
    employee_count: organizationData.employeeCount,
    organization_type: organizationData.type,
  };

  if (organizationData.address.id) {
    org.address.id = organizationData.address.id;
  }

  return org;
}

/**
 * @param membershipLevel - Filled membership level data, stored in formik context
 * @param formId - Form Id fetched from the server, sotored in membership context, used for calling APIs
 * @param userId - User Id fetched from the server when sign in, sotored in membership context, used for calling APIs
 */
export function matchMembershipLevelFieldsToBackend(membershipLevelFormData, formId, userId) {
  return {
    id: formId,
    user_id: userId,
    membership_level: membershipLevelFormData.membershipLevel,
    signing_authority: true, //what does this do?
    purchase_order_required: membershipLevelFormData.purchasingAndVAT.purchasingProcess,
    vat_number: membershipLevelFormData.purchasingAndVAT.vatNumber,
    registration_country: membershipLevelFormData.purchasingAndVAT.countryOfRegistration,
    state: 'INPROGRESS',
  };
}

/**
 * @param contactData -Filled contacts data, stored in formik context
 * @param contactType - WORKING_GROUP, MARKETING, ACCOUNTING, COMPNAY, one of the types
 * @param formId - Form Id fetched from the server, sotored in membership context, used for calling APIs
 */
export function matchContactFieldsToBackend(contactData, contactType, formId) {
  if (contactType === CONTACT_TYPE.WORKING_GROUP && !contactData.id) {
    return {
      form_id: formId,
      first_name: contactData.firstName,
      last_name: contactData.lastName,
      job_title: contactData.jobtitle,
      email: contactData.email,
      type: contactType,
    };
  }

  return {
    id: contactData.id,
    form_id: formId,
    first_name: contactData.firstName,
    last_name: contactData.lastName,
    job_title: contactData.jobtitle,
    email: contactData.email,
    type: contactType,
  };
}

/**
 * @param eachWorkingGroupData - Filled working group data, stored in formik context
 * @param formId - Form Id fetched from the server, sotored in membership context, used for calling APIs
 */
export function matchWGFieldsToBackend(eachWorkingGroupData, formId) {
  var wg_contact = matchContactFieldsToBackend(
    eachWorkingGroupData.workingGroupRepresentative,
    CONTACT_TYPE.WORKING_GROUP,
    formId
  );

  const theDate = new Date();

  return {
    id: eachWorkingGroupData?.id,
    working_group: eachWorkingGroupData?.workingGroup.value,
    participation_level: eachWorkingGroupData?.participationLevel,
    effective_date: theDate.toISOString().replace(/.\d+Z$/g, 'Z'),
    contact: {
      ...wg_contact,
    },
  };
}

/**
 * This is a fetch wrapper function and can be used for most POST/PUT API calls related to application form.
 * @param {string} formId - Form Id fetched from the server, sotored in membership context, used for calling APIs
 * @param {Object} dataBody - The data to be used for the API call
 * @param {Function} [onSuccess] - (optional) The success callback function of the API call.
 * @param {string} endpoint - The endpoint of the API call
 */
export function fetchWrapperForm(formId, dataBody, onSuccess, endpoint = '') {
  const entityId = dataBody.id ? dataBody.id : '';
  const method = dataBody.id ? FETCH_METHOD.PUT : FETCH_METHOD.POST;

  let url = API_PREFIX_FORM + `/${formId}`;

  if (endpoint) {
    url = API_PREFIX_FORM + `/${formId}/${endpoint}`;
  }

  if (entityId && entityId !== formId) {
    url = API_PREFIX_FORM + `/${formId}/${endpoint}/${entityId}`;
  }

  // Remove the id without impact on source/original data
  const { id, ...sanitizedBody } = dataBody;

  if (getCurrentMode() === MODE_REACT_ONLY && !isProd) {
    console.log(`You called ${url} with Method ${method} and data body is:`);
    console.log(JSON.stringify(sanitizedBody));
  }

  if (getCurrentMode() === MODE_REACT_API) {
    fetch(url, {
      method: method,
      headers: FETCH_HEADER,
      body: JSON.stringify(sanitizedBody),
    })
      .then((res) => {
        if (res.ok) {
          const isJson = res.headers.get('content-type')?.includes('application/json');
          return isJson ? res.json() : res;
        }

        requestErrorHandler(res.status);
        throw res.status;
      })
      .then((data) => {
        onSuccess && onSuccess(data);
      })
      .catch((err) => {
        console.log(err);
        // This will make sure when "then" is skipped, we could still handle the error
        requestErrorHandler(err);
      });
  }
}

/**
 * DELETE
 *
 * @param {string} formId - Form Id fetched from the server, sotored in membership context, used for calling APIs
 * @param {string} endpoint - To which endpoint the fetch is calling to backend:
 * /form/{id}, /form/{id}/organizations/{id}, /form/{id}/contacts/{id}, /form/{id}/working_groups/{id}
 * @param {string} entityId - The Id of organizations, or contacts, or working groups entry;
 * If empty, is creating a new entity, use POST method;
 * If has value, is fetched from server, use PUT or DELETE;
 * @param {Function | ''} [callback] - (optional) Callback function, called when fetch resolved
 * @param {number} index - Typically for working groups, which one is deleted.
 * Typically is used by the callback function from WorkingGroup Component (arrayhelpers.remove())
 */
export function deleteData(formId, endpoint, entityId, callback, index) {
  // If the added field array is not in the server, just remove it from frontend
  if (!entityId) {
    callback && callback(index);
  }

  // If the not using java server, just remove it from frontend
  if (getCurrentMode() === MODE_REACT_ONLY && index) {
    callback && callback(index);
  }

  // If removing existing working_group
  if (entityId) {
    let url = API_PREFIX_FORM + `/${formId}`;
    if (endpoint) {
      url = API_PREFIX_FORM + `/${formId}/${endpoint}`;
    }
    if (entityId && entityId !== formId) {
      url = API_PREFIX_FORM + `/${formId}/${endpoint}/${entityId}`;
    }
    fetch(url, {
      method: FETCH_METHOD.DELETE,
      headers: FETCH_HEADER,
    })
      .then((res) => {
        if (res.ok) {
          // Remove from frontend
          callback && callback(index);
          return Promise.resolve(res);
        }

        requestErrorHandler(res.status);
        throw res.status;
      })
      .catch((err) => {
        console.log(err);
        requestErrorHandler(err);
      });
  }
}

/**
 * Handle New Form
 *
 * @param {Function} setCurrentFormId - setCurrentFormId function from membership context
 * @param {Function} goToCompanyInfoStep - Go to the next step and add this step to complete set, passed from FormikStepper Component
 *
 * The logic:
 * - POST a new form and returned the new form Id
 * - Store the returned new form Id in my FormId Context
 * - Send the API calls to organizations and contacts
 **/
export function handleNewForm(setCurrentFormId, goToCompanyInfoStep) {
  goToCompanyInfoStep();

  if (getCurrentMode() === MODE_REACT_API) {
    var dataBody = {
      membership_level: '',
      signing_authority: false,
      purchase_order_required: '',
      state: 'INPROGRESS',
    };

    fetch(API_PREFIX_FORM, {
      method: FETCH_METHOD.POST,
      headers: FETCH_HEADER,
      body: JSON.stringify(dataBody),
    })
      .then((res) => {
        if (res.ok) return res.json();

        requestErrorHandler(res.status);
        throw res.status;
      })
      .then((data) => {
        !isProd && console.log('Start with a new form:', data);
        setCurrentFormId(data[0]?.id);
      })
      .catch((err) => {
        console.log(err);
        requestErrorHandler(err);
      });
  }

  // Probably Also need to delete the old form Id, or keep in the db for 30 days
}

/**
 * This is the error handler for API calls for application form. 
 * It will take user to a specific error page based on the status code we get
 * 
 * @param statusCode - The status got from the response of a fetch
 *
 **/
export function requestErrorHandler(statusCode) {
  const origin = window.location.origin;

  switch (statusCode) {
    case 404:
      window.location.assign(origin + '/404');
      break;
    case 500:
      window.location.assign(origin + '/50x');
      break;
    case 401:
      sessionStorage.setItem(HAS_TOKEN_EXPIRED, 'true');
      window.location.assign(origin + '/');
      break;
    case 499:
      sessionStorage.setItem(HAS_TOKEN_EXPIRED, 'true');
      window.location.assign(origin + '/');
      break;
    default:
      window.location.assign(origin + '/50x');
      break;
  }
}

/**
 * This will scroll to the top of current page.
 **/
export function scrollToTop() {
  window.scrollTo({ top: 0, behavior: 'smooth' });
}

/**
 * This will check if any property in the object and or any property in any nested object has an empty string or false as the value. 
 * It will return false if all properties have valid value and return true if not.
 * 
 * @param {object} obj - Can be any object that need to be checked. 
 * Currently it's mainly for the value saved in Formik for current page/step.
 * Note: "id" and "allWorkingGroups" properties won't be checked.
 **/
export function isObjectEmpty(obj) {
  for (const key in obj) {
    // Do not need to check the value of id or allWorkingGroups, as they are not provided by users
    if (key === 'id' || key === 'allWorkingGroups') {
      continue;
    }

    const element = obj[key];
    if (typeof element === 'object') {
      if (!isObjectEmpty(element)) {
        return false;
      }
    } else if (element !== '' && element !== false) {
      return false;
    }
  }
  return true;
}

/**
 * This will pop up a modal window if the validation failed and or the "isEmpty" is true.
 * When "isEmpty" is false and validation is passed, 
 * it will trigger "submitForm" to update current step and "navigate" to go back to previous step.
 * 
 * @param {boolean} isEmpty - A boolean indicates if there is any empty field in current step.
 * @param {object} result - An object returned by formik.validateForm(), which will contain any fields that failed to pass validation.
 * @param {Function} submitForm - A function that will trigger an API call to update current step.
 * @param {Function} setTouched - It needs to be formik.setTouched, which will record the fields with errors in Formik.
 * @param {Function} setShouldOpen - A setState method that will update the "shouldOpen" state to true to open the modal window.
 * @param {Function} navigate - It will be triggered to go back to previous step, if "isEmpty" is false and "result" is an empty object.
 * @param {boolean} isNotFurthestPage - A boolean indicates if current step/page is the furthest one user can go for now.
 **/
export function validateGoBack(isEmpty, result, submitForm, setTouched, setShouldOpen, navigate, isNotFurthestPage) {
  // Save values on current step if it's NOT empty and passes validation
  if (!isEmpty && Object.keys(result).length <= 0) {
    submitForm();
  }

  // Open modal window if it's NOT empty and fails to pass validation
  // OR open it if it's empty and NOT the furthest page
  if ((!isEmpty && Object.keys(result).length > 0) || (isEmpty && isNotFurthestPage)) {
    setTouched(result);
    setShouldOpen(true);
    return;
  }

  navigate();
}

/**
 * This will check if current step/page is the furthest one the user can go for now.
 * It will return true if it's not the furthest, and return false if it is.
 * 
 * @param {number} currentIndex - The index number of current step/page. 
 * @param {number} furthestIndex - The index number of the furthest step/page the user can go for now. 
 **/
export function checkIsNotFurthestPage(currentIndex, furthestIndex) {
  if (currentIndex === 3) {
    // For wg/3rd step, it can be empty, and clear and remove operation will update the database when user does so
    // So, no need to roll back the data
    return false;
  }
  return currentIndex < furthestIndex;
}

/**
 * This function will trigger the logout action.
 **/
export const logout = () => {
  let url = application_api_prefix();
  if (window.location.pathname.includes('/portal')) {
    url = api_prefix();
  }
  window.location.assign(`${url}/logout`);
};

/**
 * This function will do the validation without submitting the form.
 * 
 * @param ev - The event object which takes place in the DOM.
 **/
export const checkValidityWithoutSubmitting = (ev) => {
  // Do checkValidity without submitting will make Select component in Material-UI get focused if it's invalid without refreshing the page
  ev.preventDefault();
  ev.currentTarget.checkValidity();
};

/**
 * This function will scroll the page to the 1st invalid field if there is any.
 **/
export const focusOnInvalidField = () => {
  const firstInvalidField = document.querySelector('input[aria-invalid="true"]');
  firstInvalidField && firstInvalidField.focus();
};

/**
 * This is a fetch wrapper function that is mainly for Portal app.
 * 
 * @param {string} url - The URL of the resource you want to fetch.
 * @param {string} method - The request method, e.g., GET, POST, etc..
 * @param {Function | ''} callbackFunc - The success callback function. Will be triggered after a successful fetch.
 * @param {object | string | undefined} [dataBody] - The value of request body.
 * @param {Function | ''} [errCallbackFunc] - The failed callback function. Will be triggered after a failed fetch.
 **/
export const fetchWrapper = (url, method, callbackFunc, dataBody, errCallbackFunc) => {
  const shouldIncludeCSRF = url[0] === '/';
  let requestHeader = shouldIncludeCSRF ? FETCH_HEADER : FETCH_HEADER_WITHOUT_CSRF;

  if (url.includes('/logos') && method === 'POST') {
    requestHeader = { 'x-csrf-token': FETCH_HEADER['x-csrf-token'] };
  }

  fetch(url, {
    method: method,
    headers: requestHeader,
    body: dataBody || null,
  })
    .then((res) => {
      if (res.ok) {
        // DELETE and 204 won't return response data, so don't do json()
        return method === 'DELETE' || method === 'POST' || res.status === 204 ? res : res.json();
      }
      throw res.status;
    })
    .then((data) => callbackFunc && callbackFunc(data))
    .catch((err) => {
      console.log(err);
      if (errCallbackFunc) {
        errCallbackFunc(err);
      }
    });
};

/**
 * This will execute fetch with pagination.
 * 
 * @param {string} url - The URL of the resource you want to fetch.
 * @param {number} i - The start page number.
 * @param {Function | ''} callbackFunc - The success callback function. Will be triggered after a successful fetch.
 * @param {Function | ''} [errHandler] - The failed callback function. Will be triggered after a failed fetch.
 **/
export const fetchWrapperPagination = async (url, i, callbackFunc, errHandler) => {
  let data = [];
  let hasError = false;
  const shouldIncludeCSRF = url[0] === '/';
  const requestHeader = shouldIncludeCSRF ? FETCH_HEADER : FETCH_HEADER_WITHOUT_CSRF;

  const getData = async () => {
    let response = await fetch(`${url}${i}`, {
      headers: requestHeader,
    });

    if (response.ok) {
      let newData = await response.json();
      if (url.includes('https://newsroom.eclipse.org/api/resources')) {
        data = [...data, ...newData.resources];
      } else {
        data = [...data, ...newData];
      }

      if (response.headers.get('link')?.includes('rel="next"')) {
        i++;
        await getData();
      } else {
        i = -1;
      }
    } else {
      hasError = true;
    }
  };

  await getData();

  if (hasError) {
    errHandler && errHandler();
  } else {
    callbackFunc(data);
  }
};
