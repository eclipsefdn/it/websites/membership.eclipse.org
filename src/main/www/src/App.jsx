/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { useEffect, useState } from 'react';
import './App.css';
import AppTemplate from './components/UIComponents/Templates/AppTemplate';
import MembershipContext from './Context/MembershipContext';
import { useGetUserInfoQuery } from './api/membership';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Application from './components/Application/Application';
import Portal from './components/Portal/Portal';
import NotFound404 from './components/ErrorPages/NotFound404';
import InternalError50x from './components/ErrorPages/InternalError50x';
import {
  api_prefix,
  FETCH_HEADER,
  LOGIN_FROM_KEY,
  ROUTE_SIGN_IN,
} from './Constants/Constants';
import GlobalContext from './Context/GlobalContext';
import { ThemeProvider } from './Context/theme';
import TopSlideMsg from './components/UIComponents/Notifications/TopSlideMsg';
import Loading from './components/UIComponents/Loading/Loading';

const App = () => {
  const { data: user, isLoading: isUserLoading } = useGetUserInfoQuery();
  const [currentUserForm, setCurrentUserForm] = useState(null);
  const [currentFormId, setCurrentFormId] = useState('');
  const [furthestPage, setFurthestPage] = useState({
    index: 0,
    pathName: ROUTE_SIGN_IN,
  });
  const [needLoadingSignIn, setNeedLoadingSignIn] = useState(true);
  const [currentStepIndex, setCurrentStepIndex] = useState(0);
  const [orgId, setOrgId] = useState(0);
  const [contactFilterRole, setContactFilterRole] = useState('');
  const [gotCSRF, setGotCSRF] = useState(false);
  const [shouldShowMsg, setShouldShowMsg] = useState(false);
  const [displayPeriod, setDisplayPeriod] = useState(4000);
  const [msgContent, setMsgContent] = useState('');
  const [isError, setIsError] = useState(false);

  const failedToExecute = (msg, displayPeriod) => {
    setDisplayPeriod(displayPeriod);
    setShouldShowMsg(true);
    setMsgContent(msg || 'Something went wrong, please try again later.');
    setIsError(true);
  };
  const succeededToExecute = (msg) => {
    setIsError(false);
    setShouldShowMsg(true);
    setMsgContent(msg || 'Saved successfully!');
  };

  const globalContext = {
    gotCSRF,
    setGotCSRF,
    failedToExecute,
    succeededToExecute,
  };
  const membershipContextValue = {
    currentFormId,
    setCurrentFormId,
    furthestPage,
    setFurthestPage,
    needLoadingSignIn,
    setNeedLoadingSignIn,
    currentStepIndex,
    setCurrentStepIndex,
    orgId,
    setOrgId,
    contactFilterRole,
    setContactFilterRole,
    currentUserForm,
    setCurrentUserForm,
  };

  useEffect(() => {
    fetch(`${api_prefix()}/csrf`, { headers: FETCH_HEADER })
      .then((res) => {
        FETCH_HEADER['x-csrf-token'] = res.headers.get('x-csrf-token');
        FETCH_HEADER['x-csrf-token'] && setGotCSRF(true);
      })
      .catch((err) => console.log(err));
  }, []);

  const getRedirectUrl = () => {
    const loginFrom = localStorage.getItem(LOGIN_FROM_KEY);

    switch (loginFrom) {
      case 'portal':
        return '/portal/login';
      case 'ready-to-join':
        // If the user has an entry in allRelations, it means they have an
        // organization. Redirect them to the portal's join working group page.
        if (user && Object.keys(user.allRelations) > 0) {
          return '/portal/join-working-group';
        } else {
          // If the user has no organization, redirect them to the application.
          return '/application';
        }
      default:
        return '/application';
    }
  }

  return (
    <div className="App">
      <ThemeProvider>
        <GlobalContext.Provider value={globalContext}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/">
                { isUserLoading ? null : <Redirect to={getRedirectUrl()} /> }
              </Route>

              <Route path="/application">
                <MembershipContext.Provider value={membershipContextValue}>
                  <AppTemplate>
                    {gotCSRF ? <Application /> : <Loading />}
                  </AppTemplate>
                </MembershipContext.Provider>
              </Route>

              <Route exact path="/404">
                <AppTemplate>
                  <NotFound404 />
                </AppTemplate>
              </Route>

              <Route exact path="/50x">
                <AppTemplate>
                  <InternalError50x />
                </AppTemplate>
              </Route>

              <Route path="/portal">
                <Portal />
              </Route>

              {/* Redirect user to 404 page for all the unknown pathnames/urls */}
              <Redirect to="404" />
            </Switch>
          </BrowserRouter>

          <TopSlideMsg
            shouldShowUp={shouldShowMsg}
            setShouldShowUp={setShouldShowMsg}
            isError={isError}
            msgContent={msgContent}
            displayPeriod={displayPeriod}
          />
        </GlobalContext.Provider>
      </ThemeProvider>
    </div>
  );
};

export default App;
