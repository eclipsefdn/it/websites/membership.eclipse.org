import { configureStore } from '@reduxjs/toolkit';
import organizationReducer from './features/organization-slice';
import { membershipAPI } from './api/membership';
import { applicationAPI } from './api/application';
import { accountAPI } from './api/account';
import { workingGroupsAPI } from './api/working-groups';
import { newsroomAPI } from './api/newsroom';

export const setupStore = () => {
  return configureStore({
    reducer: {
      organization: organizationReducer,
      [accountAPI.reducerPath]: accountAPI.reducer,
      [applicationAPI.reducerPath]: applicationAPI.reducer,
      [membershipAPI.reducerPath]: membershipAPI.reducer, 
      [newsroomAPI.reducerPath]: newsroomAPI.reducer,
      [workingGroupsAPI.reducerPath]: workingGroupsAPI.reducer,
    },
    middleware: (getDefaultMiddleware) => {
      return getDefaultMiddleware()
        .concat(accountAPI.middleware)
        .concat(applicationAPI.middleware)
        .concat(membershipAPI.middleware)
        .concat(newsroomAPI.middleware)
        .concat(workingGroupsAPI.middleware);
    },
  });
};

export type AppDispatch = ReturnType<typeof setupStore>['dispatch'];
export type RootState = ReturnType<ReturnType<typeof setupStore>['getState']>;
