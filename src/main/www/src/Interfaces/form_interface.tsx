/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
export interface FormValue {
  organization: {
    id: string;
    legalName: string;
    type: string;
    twitterHandle: string;
    revenue: string;
    employeeCount: string;
    address: {
      id: string;
      street: string;
      city: string;
      provinceOrState: string;
      country: string;
      'country-label': string;
      postalCode: string;
    };
  };
  representative: {
    member: {
      id: string;
      firstName: string;
      lastName: string;
      jobtitle: string;
      email: string;
    };
    marketing: {
      sameAsCompany: boolean;
      id: string;
      firstName: string;
      lastName: string;
      jobtitle: string;
      email: string;
    };
    accounting: {
      sameAsCompany: boolean;
      id: string;
      firstName: string;
      lastName: string;
      jobtitle: string;
      email: string;
    };
  };
  purchasingAndVAT: {
    purchasingProcess: string;
    vatNumber: number | string;
    countryOfRegistration: string;
  };
  'membershipLevel-label': { label: string; value: string };
  membershipLevel: string;
  workingGroups: [WorkingGroups];
  signingAuthorityRepresentative: {
    firstName: string;
    lastName: string;
    email: string;
    jobtitle: string;
    id: string;
  };
}

export interface WorkingGroups {
  id: string;
  workingGroup: {
    label: string;
    value: string;
    participationLevel: [string];
  };
  participationLevel: string;
  effectiveDate: string;
  workingGroupRepresentative: {
    firstName: string;
    lastName: string;
    jobtitle: string;
    email: string;
    id: string;
  };
}
