/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// Utility Types
export type DeepPartial<T> = T extends object ? {
    [P in keyof T]?: DeepPartial<T[P]>;
} : T;

// User Models
export type UserRole = 'admin' | 'eclipsefdn_membership_portal_elevated_access';
export type OrganizationalRole = 
    | 'CA'
    | 'AD'
    | 'AC'
    | 'BR'
    | 'BA'
    | 'BABE'
    | 'EMPLY'
    | 'SDCP'
    | 'DE'
    | 'BRBE'
    | 'BRUS'
    | 'CC'
    | 'CI'
    | 'CL'
    | 'CM'
    | 'CN'
    | 'MA'
    | 'CR'
    | 'CRA'
    | 'PC'
    | 'TWGR'
    | 'WGR'
    | 'WGSC';
export type Role = UserRole | OrganizationalRole;
export interface User {
  username: string;
  firstName: string;
  lastName: string;
  relation: string[];
  allRelations: Record<number, string[]>;
  roles: Role[];
};

export interface Profile {
  username: string;
  picture: string;
  firstName: string;
  lastName: string;
  jobTitle: string;
}


export interface BasicContact {
  firstName: string;
  lastName: string;
  email: string;
}

// Organization Models
export type OrganizationLevel = 'SD' | 'OHAP' | 'AP' | 'AS';
export interface OrganizationLevelDetailed {
  level: OrganizationLevel;
  description: string;
  sortOrder: number;
}
export interface ParticipationAgreement {
  documentId: string;
  description: string | null;
  level: string | null;
  workingGroup: string;
}

export interface Organization {
  organizationId: number;
  name: string;
  description: {
    long: string;
  } | null;
  logos: {
    print: string | null;
    web: string | null;
  };
  website: string | null;
  levels: OrganizationLevelDetailed[];
  participationAgreements: ParticipationAgreement[];
  /** The date since the organization joined as a member. */
  memberSince: string;
  /** The renewal date of the organization's membership. */
  renewalDate: string;
}

export type SlimOrganization = Pick<Organization, 'organizationId' | 'name' | 'logos'>;

export interface OrganizationProduct {
  /** The product ID */
  id: number;
  name: string;
  description: string;
  /** The URL of the product's website */
  url: string;
}

export interface OrganizationInfo {
  organizationId: number;
  description?: string;
  url?: string;
}

export type LogoFormat = 'SMALL' | 'LARGE' | 'PRINT' | 'WEB';

// Statistic Models
export interface CBIAllocation {
  inUse: number;
  allocated: number;
}

export interface CBIData {
  resourcePacks: CBIAllocation,
  dedicatedAgents: CBIAllocation,
  runners: {
    githubLargeRunners: CBIAllocation,
  },
}

export interface OrganizationContact extends BasicContact {
  id: string;
  relations: OrganizationalRole[]
}

export interface OrganizationContactDetailed extends OrganizationContact {
  jobTitle: string;
  comments: string;
}

export interface OrganizationRepresentative {
  personId: string;
  firstName: string;
  lastName: string;
  relations: OrganizationalRole[];
}

export interface Committer {
  personId: string;
  firstName: string;
  lastName: string;
  email: string;
}

export interface CommitterData {
  activity: Array<{ count: number, period: { year: string, month: string }}>;
}

export type Contributor = Committer;

export interface WorkingGroupRelationLevels {
  relation: string;
  description: string;
}
export interface WorkingGroup {
  alias: string;
  title: string;
  status: 'active' | 'inactive' | 'proposal';
  logo?: string;
  description?: string;
  parentOrganization: string;
  resources: {
    charter: string;
    website: string;
    members: string;
    sponsorship: string;
    contactForm: string;
    participationAgreements: {
      individual?: unknown;
      organization: {
        pdf: string;
        documentId: string;
      }
    }
  },
  levels: WorkingGroupRelationLevels[];
}

export interface WorkingGroupApplication {
  /** Unique ID for the working group application. Can be ommitted for new
    * applications. */
  id: number | null;
  user: string;
  created: Date;
  updated: Date;
  state: 'INPROGRESS';
  organizationId: number;
  contacts: Array<BasicContact & {
    user: string | null,
    jobTitle: string;
    isSigningAuthority: boolean;
    workingGroup?: {
      alias: string;
      level: string;
    };
  }>;
};

// Project Models
export interface PMIUser {
  username: string;
  fullName: string;
  url: string;
};

export interface ProjectRelease {
  name: string;
  url: string;
  date: Date;
};

export interface BaseProject {
  projectId: string;
  name: string;
  summary: string;
  pmiUrl: string;
  committers: PMIUser[];
  contributors: PMIUser[];
  projectLeads: PMIUser[];
  industryCollaborations: Array<{
    name: string;
    id: string;
  }>;
};

export interface Project extends BaseProject {
  status: 'incubating' | 'regular' | 'archived';
  logo: string;
  websiteUrl: string;
  tags: string[];
  releases: ProjectRelease[];
};

// The partial response type of the endpoint for an organization's projects.
export interface ProjectSlim {
  id: string;
  name: string;
  description: string;
  active: boolean;
}

// Forms
export interface FormStepBaseProps {
  /** The submit handler for this form step */
  onSubmit: (...args: any) => any;
}

/** Top-level errors for a form */
export interface FormStatusError {
  severity: 'error' | 'warning' | 'info';
  message: string;
}

// Newsroom Models
export type NewsroomResourceType = 'survey-report' | 'white-paper' | 'market-report' | 'case-study' | 'social-media-kit';

export interface NewsroomResource {
  id: string;
  title: string;
  date: string;
  directLink: string;
}

