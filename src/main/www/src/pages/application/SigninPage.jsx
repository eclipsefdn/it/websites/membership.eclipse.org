import { useOutletContext } from 'react-router-dom';
import SignInIntroduction from '@components/Application/SignIn/SignInIntroduction';
import SignIn from '@components/Application/SignIn/SignIn';

const SigninPage = () => {
  const { 
    formik,
    renderStepper, 
    setIsStartNewForm, 
    updatedFormValues, 
    setUpdatedFormValues 
  } = useOutletContext();

  return (
    <>
      <SignInIntroduction />
      {renderStepper()}
      <SignIn
        setIsStartNewForm={setIsStartNewForm}
        resetForm={formik.resetForm}
        updatedFormValues={updatedFormValues}
        setUpdatedFormValues={setUpdatedFormValues}
      />
    </>
  );
}

export default SigninPage;
