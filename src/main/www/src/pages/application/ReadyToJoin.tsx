/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Button, Box, Typography } from '@material-ui/core';
import Spacer from '@components/Spacer';
import { login } from '../../utils';

/**
  * This page is one of the entry points to the Membership Application &
  * Portal. From this page, the user should be able to log in and be redirected
  * to either the application or the portal's join working group form.
  */
const ReadyToJoinPage: React.FC = () => {
  // Direct the user to the Eclipse account register page on
  // accounts.eclipse.org.
  const handleCreateAccountClick = () => {
    window.location.replace('https://accounts.eclipse.org/user/register');
  };

  // The event handler when the user clicks the login button.
  const handleLoginClick = () => {
    login('ready-to-join');
  };

  return (
    <Box textAlign="center">
      <Spacer y={8} />
      <Typography component="h1" variant="h4">
        Membership and Working Group Application Form
      </Typography>
      <Spacer y={1} />
      <Typography>
        Please sign in to complete the Membership Form or to add a Working
        Group to your current membership.
      </Typography>
      <Spacer y={4} />
      <Box 
        display="flex" 
        justifyContent="center" 
        gridGap="2rem" 
        maxWidth="36rem"
        marginX="auto" 
      >
        <Button 
          color="primary" 
          variant="contained" 
          onClick={handleLoginClick}
          fullWidth
        >
          Log In
        </Button>
        <Button 
          color="primary" 
          variant="contained" 
          onClick={handleCreateAccountClick}
          fullWidth
        >
          Create an Account
        </Button>
      </Box>
      <Spacer y={24} />
    </Box>
  );
}

export default ReadyToJoinPage;

