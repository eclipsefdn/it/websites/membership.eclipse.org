/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

import { useState } from 'react';
import { CircularProgress, createStyles, makeStyles, IconButton, Popover, Theme, Typography } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { BusinessCenter as BusinessCenterIcon, HelpOutline as HelpIcon } from '@material-ui/icons';
import { DataGrid, GridColDef, GridRenderCellParams, GridColumnHeaderTitle } from '@mui/x-data-grid';
import {
  borderRadiusSize,
  brightOrange,
  iconGray,
} from '../../Constants/Constants';
import { renderTableHelperText } from '../../utils/portalFunctionHelpers';
import CustomToolbar from '@components/UIComponents/Button/CustomToolbar';
import Spacer from '@components/Spacer';
import FullLayout from '../../layouts/FullLayout';
import { useAppSelector } from '../../hooks';
import { useGetOrganizationProjectsQuery } from '../../api/membership';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageTitle: {
      margin: theme.spacing(0.5, 0, 2),
    },
    filterText: {
      backgroundColor: brightOrange,
      color: 'white',
      padding: theme.spacing(1),
      borderTopLeftRadius: borderRadiusSize,
      borderTopRightRadius: borderRadiusSize,
    },
    tableContainer: {
      minHeight: 330,
      height: 'calc(100vh - 455px)',
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    table: {
      borderTopRightRadius: 0,
      borderTopLeftRadius: 0,
    },
    noRowsOverlay: {
      position: 'absolute',
      top: 56,
      bottom: 0,
      left: 0,
      right: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    helpIcon: {
      width: '15.4px', // same size as the sort icon
      height: '15.4px', 
      color: 'rgba(0, 0, 0, 0.54)', // similar color as sort icon
    },
  })
);

const columns: GridColDef[] = [
  {
    field: 'name',
    headerName: 'Project Name',
    minWidth: 220,
    flex: 2,
    renderCell: (params: GridRenderCellParams) => {
      const data = params.row;
      return data.status === 'Active' ? (
        <a href={data.url} target="_blank" rel="noreferrer">
          {data.name}
        </a>
      ) : (
        <p>{data.name}</p>
      );
    },
  },
  {
    field: 'id',
    headerName: 'Project ID',
    minWidth: 200,
    flex: 1,
    renderCell: (params: GridRenderCellParams) => {
      const data = params.row;
      return data.status === 'Active' ? (
        <a href={data.url} target="_blank" rel="noreferrer">
          {data.id}
        </a>
      ) : (
        <p>{data.id}</p>
      );
    },
  },
  {
    field: 'status',
    flex: 1,
    minWidth: 100,
    renderHeader: () => <StatusHeader />,
  },
];

export default function YourProjects() {
  const classes = useStyles();
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: organizationProjects, isLoading } = useGetOrganizationProjectsQuery(organizationId!);

  const projects = organizationProjects?.map((project) => {
    return {
      id: project.id,
      name: project.name,
      url: `https://projects.eclipse.org/projects/${project.id}`,
      status: project.active ? 'Active' : 'Inactive',
    }
  }) || [];

  return (
    <FullLayout>
      <BusinessCenterIcon className={classes.headerIcon} />
      <Typography className={classes.pageTitle} variant="h4" component="h1">
        Your Projects
      </Typography>
      { projects.length >= 1 
        ? <PreambleAlert />
        : null
      }
      <Spacer y={2} />
      <Typography variant="body1" className={classes.filterText}>
        Projects
      </Typography>
      <div className={classes.tableContainer}>
        { isLoading ? (
          <CircularProgress />
        ) : (
          <DataGrid
            className={classes.table}
            rows={projects}
            columns={columns}
            rowHeight={55}
            rowsPerPageOptions={[5, 10, 100]}
            disableSelectionOnClick
            components={{
              NoRowsOverlay: () => (
                <div className={classes.noRowsOverlay}>
                  {renderTableHelperText(projects, isLoading)}
                </div>
              ),
              Toolbar: () => <CustomToolbar fileName="your_projects_data" />,
            }}
          />
        )}
      </div>
    </FullLayout>
  );
}

/**
  * The component for the Project Table's "status" header. It is a normal
  * column header but it has a popover with help text.
  */
const StatusHeader: React.FC = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

  const handlePopoverClick: React.MouseEventHandler = (event) => {
    // Must stop propagation so the sort doesn't get fired.
    event.stopPropagation();
    setAnchorEl(event.currentTarget as HTMLElement);
  }

  const handlePopoverClose: React.MouseEventHandler = (event) => {
    // Must stop propagation so the sort doesn't get fired.
    event.stopPropagation();
    setAnchorEl(null);
  }

  const isPopoverOpen = Boolean(anchorEl);
  const id = isPopoverOpen ? 'status-popover' : undefined;

  return (
    <>
      <GridColumnHeaderTitle label="Status" columnWidth={1} />
      <IconButton size="small" onClick={handlePopoverClick} aria-described={id}>
        <HelpIcon className={classes.helpIcon} />
      </IconButton>
      <Spacer x={1} />
      <Popover 
        id={id}
        open={isPopoverOpen}
        anchorEl={anchorEl}
        onClose={handlePopoverClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        PaperProps={{
          style: { padding: '2rem', maxWidth: '32rem' },
        }}
      >
        <Typography>
          <em>Status</em> indicates the project's lifecycle state and
          is not indicative of the organisation's current commit activity
          within the project. 
        </Typography>
      </Popover>
    </>
  )
}

const PreambleAlert: React.FC = () => {
  return (
    <Alert severity="info">
      <AlertTitle>Why am I seeing these projects?</AlertTitle>
      <Typography>
        Any project which was contributed to by members of your organisation
        will be present in this list.
      </Typography>
    </Alert>
  );
}
