/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { iconGray, brightOrange } from '../../../Constants/Constants';
import { makeStyles, createStyles, Button, Typography, Theme } from '@material-ui/core';
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import { Link } from 'react-router-dom';
import FullLayout from '../../../layouts/FullLayout';
import Spacer from '@components/Spacer';

/**
 * The success page for the Working Group Application form.
 */
export const SuccessPage: React.FC = () => {
  const classes = useStyles();

  return (
    <FullLayout>
      <GroupWorkIcon className={classes.headerIcon} />
      <Typography className={classes.pageTitle} variant="h4" component="h1">
        Application Submitted Successfully!
      </Typography>
      <Typography>
        Thank you for your interest in joining our Working Group(s). We have
        received your application and it is currently under review.
      </Typography>
      <Spacer y={1} />
      <Typography>
        Our membership team will contact you to provide a Working Group
        Participation Agreement within 2 business days, which is required to
        formally join the Working Group(s).
      </Typography>
      <Spacer y={1} />
      <Typography>
        If you have any questions or need to update your application, feel free
        to reach out to us at <a href="mailto:membership@eclipse.org">membership@eclipse.org</a>. 
      </Typography>
      <Spacer y={2} />
      <Link to="/portal/dashboard">
        <Button variant="contained" color="primary">Return to Dashboard</Button>
      </Link>
    </FullLayout>
  );
};

export default SuccessPage;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageTitle: {
      margin: theme.spacing(0.5, 0, 4),
    },
  })
);


