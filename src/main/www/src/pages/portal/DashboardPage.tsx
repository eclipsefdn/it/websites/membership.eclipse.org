/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Grid, Typography, useTheme } from '@material-ui/core';
import { 
  BusinessCenter as BusinessCenterIcon,
  PeopleAlt as PeopleAltIcon,
  Description as DescriptionIcon,
  ContactSupport as ContactSupportIcon,
} from '@material-ui/icons';
import { useHashScrolling } from '../../hooks';
import FullLayout from '../../layouts/FullLayout';
import YourOrganizationSnippet from '../../modules/dashboard/YourOrganizationSnippet';
import OrganizationLogoCard from '../../modules/dashboard/OrganizationLogoCard';
import RepresentativeCard from '../../modules/dashboard/RepresentativeCard';
import ImportantLinksCard from '../../modules/dashboard/ImportantLinksCard';
import SectionLink from '@components/SectionLink';
import Spacer from '@components/Spacer';
import YourWorkingGroupsCard from '../../modules/dashboard/YourWorkingGroupsCard';
import InterestedWorkingGroupsCard from '../../modules/dashboard/InterestedWorkingGroupsCard';
import YourProjectsCard from '../../modules/dashboard/YourProjectsCard';
import InterestedProjectsCard from '../../modules/dashboard/InterestedProjectsCard';
import CommitterCard from '../../modules/dashboard/CommitterCard';
import ContributorCard from '../../modules/dashboard/ContributorCard';
import CommitterChart from '../../modules/dashboard/CommitterChart';
import NewsroomResourceCard from '../../modules/dashboard/NewsroomResourceCard';
import CBIStats from '../../modules/dashboard/CBIStats';
import FAQ from '../../modules/dashboard/FAQ';
import { darkOrange, brightBlue } from '../../Constants/Constants';

const DashboardPage: React.FC = () => {
  const theme = useTheme();
  useHashScrolling();

  return (
    <FullLayout>
      {/* Intro Section */}
      <YourOrganizationSnippet />
      <Spacer y={4} />
      <Grid container spacing={6}>
        <Grid item sm={12} lg={4}>
          <OrganizationLogoCard />
        </Grid>
        <Grid item sm={12} md={6} lg={4}>
          <RepresentativeCard />
        </Grid>
        <Grid item sm={12} md={6} lg={4}>
          <ImportantLinksCard />
        </Grid>
      </Grid>
      <Spacer y={8} />
      {/* Overview Section */}
      <Typography variant="h4" component="h2">Overview</Typography>
      <Spacer y={2} />
      <Grid container spacing={6}>
        <Grid item xs={12} md={6} xl={3}>
          <SectionLink 
            href="#projects-wg" 
            color={darkOrange} 
            icon={<BusinessCenterIcon/>}
          >
            Projects and Working Groups
          </SectionLink>
        </Grid>
        <Grid item xs={12} md={6} xl={3}>
          <SectionLink 
            href="#committers-contributors"
            color={brightBlue} 
            icon={<PeopleAltIcon/>}
          >
            Committers and Contributors
          </SectionLink>
        </Grid>
        <Grid item xs={12} md={6} xl={3}>
          <SectionLink 
            href="#resources"
            icon={<DescriptionIcon/>}
          >
            Resources
          </SectionLink>
        </Grid>
        <Grid item xs={12} md={6} xl={3}>
          <SectionLink 
            href="#faqs"
            color={theme.palette.secondary.main} 
            icon={<ContactSupportIcon/>}
          >
            FAQs
          </SectionLink>
        </Grid>
      </Grid>
      <Spacer y={8} />
      {/* Projects and Working Groups Section */}
      <Typography id="projects-wg" variant="h4" component="h2">Projects and Working Groups</Typography>
      <Spacer y={4} />
      <Grid container alignItems="stretch" spacing={2}>
        <Grid item xs={12} sm={6} lg={3}>
          <YourWorkingGroupsCard />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <InterestedWorkingGroupsCard />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <YourProjectsCard />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <InterestedProjectsCard />
        </Grid>
      </Grid>
      <Spacer y={8} />
      {/* Committers and Contributors */}
      <Typography id="committers-contributors" variant="h4" component="h2">Committers and Contributors</Typography>
      <Spacer y={4} />
      <Grid container alignItems="stretch" spacing={4}>
        <Grid item container md={6} spacing={2} alignItems="stretch">
          <Grid item sm={6}>
            <CommitterCard />
          </Grid>
          <Grid item sm={6}>
            <ContributorCard />
          </Grid>
          <Grid item sm={12}>
            <Spacer y={2} />
            <CBIStats />
          </Grid>
        </Grid>
        <Grid item sm={12} md={6}>
          <CommitterChart />
        </Grid>
      </Grid>
      <Spacer y={8} />
      <Typography id="resources" variant="h4" component="h2">Resources</Typography>
      <Spacer y={4} />
      <Grid container alignItems="stretch" spacing={4}>
        <Grid item xs={12} sm={6} lg={3}>
          <NewsroomResourceCard title="Your Survey Reports" type="survey-report" />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <NewsroomResourceCard title="Your White Papers" type="white-paper" />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <NewsroomResourceCard title="Your Market Reports" type="market-report" />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <NewsroomResourceCard title="Your Case Studies" type="case-study" />
        </Grid>
      </Grid>
      <Spacer y={8} />
      <Typography id="faqs" variant="h4" component="h2">FAQs</Typography>
      <Spacer />
      <FAQ />
    </FullLayout>
  );
}

export default DashboardPage;
