/*!
 * Copyright (c) 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { createStyles, makeStyles, Typography, Theme } from '@material-ui/core';
import BusinessIcon from '@material-ui/icons/Business';
import { brightOrange, iconGray } from '../../Constants/Constants';
import { useAppSelector } from '../../hooks';
import { useGetOrganizationQuery } from '../../api/membership';
import FullLayout from '../../layouts/FullLayout';
import EditOrganization from '../../modules/adminEditOrganizationForm/EditOrganization';

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageHeader: {
      margin: theme.spacing(0.5, 0, 4),
    },
  })
);

const YourOrganizationProfilePage: React.FC = () => {
  const classes = useStyle();
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: organization, isLoading, error } = useGetOrganizationQuery(organizationId!);

  let render;
  
  if (isLoading || !organization) {
    render = <></>
  } else if (error) {
    render = <></>
  } else {
    render = <EditOrganization id={organizationId!} name={organization!.name} />
  }

  return (
    <FullLayout>
      <BusinessIcon className={classes.headerIcon} />
      <Typography variant="h4" component="h1" className={classes.pageHeader}>
        Your Organization Profile
      </Typography>
      {render} 
    </FullLayout>
  );
}

export default YourOrganizationProfilePage;
