/*!
 * Copyright (c) 2021, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Typography, Theme, makeStyles, createStyles } from '@material-ui/core';
import { RecentActors as RecentActorsIcon } from '@material-ui/icons';
import { iconGray } from '../../Constants/Constants';
import FullLayout from '../../layouts/FullLayout';
import ContactManagementTable from '../../modules/ContactManagementTable';

const ContactManagementPage: React.FC = () => {
  const classes = useStyles();

  return (
    <FullLayout>
      <RecentActorsIcon className={classes.headerIcon} />
      <Typography className={classes.pageTitle} variant="h4" component="h1">
        Contact Management
      </Typography>
      <ContactManagementTable />
    </FullLayout>
  );
};

export default ContactManagementPage;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${theme.palette.primary.main} solid`,
    },
    pageTitle: {
      margin: theme.spacing(0.5, 0, 4),
    },
  })
);

