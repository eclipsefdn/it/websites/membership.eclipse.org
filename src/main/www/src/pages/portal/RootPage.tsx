/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useHistory } from 'react-router-dom';
import { useGetUserInfoQuery } from '../../api/membership';
import { roles } from '../../Constants/roles';
import { ORIGINAL_PATH_KEY } from '../../Constants/Constants';
import { User } from '../../types';
import { isUserAuthorizedToAccessPortal } from '../../utils';

/**
  * This page handles the logic for redirecting the user to their destination.
  * The root page has no UI of its own.
  */
const RootPage: React.FC = () => {
  const { data: user, isLoading } = useGetUserInfoQuery();
  const history = useHistory();

  // Do an early return if the user is not available.
  if (isLoading) return null;

  // If the request to fetch user has completed but user still does not exist,
  // it means the person is not authenticated. If that is the case, we want to
  // redirect the user to the portal login page.
  if (!user) {
    history.replace('/portal/login');
    return null;
  }

  const hasAuthorization = isUserAuthorizedToAccessPortal(user);
  // If the user is authorized to access the membership portal, route them to
  // their destination.
  if (hasAuthorization) {
    const destination = getUserDestination(user);
    history.replace(destination);
  } else {
    // Otherwise, if they are not authorized, route them to the login page so
    // they can see the "no access" screen.
    history.replace('/portal/login');
  }

  return null;
};

export default RootPage;

const isUserElevatedOrAdmin = (user: User): boolean => {
  return user.roles.includes(roles.elevated) || user.roles.includes(roles.admin)
}

/**
  * Retrieves the destination which the user is navigating to. If they are
  * coming from a protected page but required to authenticate, this function
  * will return that original page they tried to access. Otherwise, it will
  * return the dashboard which is the default page in the portal.
  *
  * The destination of the user is determined by checking localStorage for the
  * `originalPath` entry.
  *
  * @returns The destination of the user. It is `/portal/dashboard` by default.
  */
const getUserDestination = (user: User): string => {
  const originalPath = localStorage.getItem(ORIGINAL_PATH_KEY);
  
  // If the user is admin or elevated, they need to enter through standby. 
  // Disregard what is stored in originalPath.
  if (isUserElevatedOrAdmin(user)) {
    return '/portal/standby';
  }
  // If the user has no original path, it means they did not try to access a
  // protected route while unauthenticated. They should be routed to the
  // dashboard by default.
  if (!originalPath) {
    return '/portal/dashboard';    
  }

  // Otherwise, send the user back to their original destination.
  return originalPath;
}
