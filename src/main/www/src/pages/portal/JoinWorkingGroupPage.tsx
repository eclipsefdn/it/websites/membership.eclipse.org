/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { iconGray, brightOrange } from '../../Constants/Constants';
import { makeStyles, createStyles, Typography, Theme } from '@material-ui/core';
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import FullLayout from '../../layouts/FullLayout';
import JoinWorkingGroupForm from '../../modules/JoinWorkingGroupForm';

export const JoinWorkingGroupPage: React.FC = () => {
  const classes = useStyles();

  return (
    <FullLayout>
      <GroupWorkIcon className={classes.headerIcon} />
      <Typography className={classes.pageTitle} variant="h4" component="h1">
        Join a Working Group
      </Typography>
      <JoinWorkingGroupForm />
    </FullLayout>
  );
};

export default JoinWorkingGroupPage;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageTitle: {
      margin: theme.spacing(0.5, 0, 4),
    },
  })
);

