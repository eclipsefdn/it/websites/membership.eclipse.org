/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import * as yup from 'yup';

const requiredFieldError = 'Required field';
const maximumLengthError = (max: number) => `The value exceeds max length ${max} characters`;
const invalidUrlError = 'Please enter a valid URL, the format should be https://example.com';

// Basic Organization Profile Form
export const basicOrganizationProfileSchema = yup.object().shape({
  description: yup
    .string()
    .max(700, maximumLengthError(700)),
  url: yup
    .string()
    .max(255, maximumLengthError(255))
    .url(invalidUrlError),
});

export const organizationProductsSchema = yup.object().shape({
  name: yup
    .string()
    .max(255, maximumLengthError(255))
    .required(requiredFieldError),
  description: yup
    .string()
    .max(700, maximumLengthError(700)),
  url: yup
    .string()
    .url(invalidUrlError)
    .max(255, maximumLengthError(255))
    .required(requiredFieldError)
});

const contactSchema = yup.object().shape({
  email: yup.string()
    .required(requiredFieldError)
    .email('Please enter a valid email'),
  firstName: yup.string()
    .required(requiredFieldError)
    .max(255, maximumLengthError(255)),
  lastName: yup.string() 
    .required(requiredFieldError)
    .max(255, maximumLengthError(255)),
  jobTitle: yup.string()
    .required(requiredFieldError)
    .max(255, maximumLengthError(255)),
});

// Join Working Group Form
const joinWorkingGroupEntriesSchema = yup.object().shape({
  entries: yup.array().of(
    yup.object().shape({
      workingGroup: yup.object()
        .required(requiredFieldError),
      participationLevel: yup.string()
        .required(requiredFieldError),
      contact: contactSchema
    })
  )
});

const signingAuthoritySchema = yup.object().shape({
  signingAuthority: contactSchema,
});

export const joinWorkingGroupSchema = yup.object().shape({
  0: joinWorkingGroupEntriesSchema,
  1: signingAuthoritySchema,
  2: yup.object(),
});
