/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { useEffect } from 'react';
import { errorRed, successGreen } from '../../../Constants/Constants';

export default function TopSlideMsg({ shouldShowUp, setShouldShowUp, isError, msgContent, displayPeriod }) {
  useEffect(() => {
    if (shouldShowUp) {
      setTimeout(() => {
        setShouldShowUp(false);
      }, displayPeriod || 4000);
    }
  }, [shouldShowUp, setShouldShowUp, displayPeriod]);
  return (
    <div className={shouldShowUp ? 'msg-popup msg-popup-show' : 'msg-popup'}>
      {isError ? (
        <ErrorOutlineIcon style={{ color: errorRed, fontSize: 30 }} />
      ) : (
        <CheckCircleOutlineIcon style={{ color: successGreen, fontSize: 30 }} />
      )}
      <span>{msgContent}</span>
    </div>
  );
}
