/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
  },
}));

const DateInput = ({ ariaLabel, onChange, name, value }) => {
  const classes = useStyles();
  const today = new Date();

  return (
    <div className="date-input">
      <TextField
        id="date"
        type="date"
        name={name}
        required={true}
        className={classes.root}
        onChange={onChange}
        value={value}
        InputProps={{
          inputProps: {
            'aria-labelledby': ariaLabel,
            min: today.toISOString().slice(0, 10), //expect yyyy-mm-dd
          },
        }}
      />
    </div>
  );
};

export default DateInput;
