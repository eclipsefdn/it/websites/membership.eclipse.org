/*!
 * Copyright (c) 2021, 2022, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Switch, Route } from 'react-router-dom';
import { CircularProgress, Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useGetUserInfoQuery } from '../../api/membership';
import { useEffect, useContext } from 'react';
import GlobalContext from '../../Context/GlobalContext';
import ProtectedRoute from '../ProtectedRoute';
import RootPage from '../../pages/portal/RootPage';
import LoginPage from '../../pages/portal/LoginPage';
import DashboardPage from '../../pages/portal/DashboardPage';
import ContactManagementPage from '../../pages/portal/ContactManagementPage';
import YourProjectsPage from '../../pages/portal/YourProjectsPage';
import YourOrganizationProfilePage from '../../pages/portal/YourOrganizationProfilePage';
import JoinWorkingGroupPage from '../../pages/portal/JoinWorkingGroupPage';
import JoinWorkingGroupSuccessPage from '../../pages/portal/joinWorkingGroup/SuccessPage';
import StandbyPage from '../../pages/portal/StandbyPage';
import UnauthorizedPage from '../../pages/portal/UnauthorizedPage';
import AdminPage from '../../pages/portal/AdminPage';
import permissions from '../../Constants/permissions';
import { useAppDispatch } from '../../hooks';
import { changeOrganization } from '../../features/organization-slice';

const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    loadingContainer: {
      backgroundColor: '#fff',
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  })
);

export default function Portal() {
  const classes = useStyles();
  const { gotCSRF } = useContext(GlobalContext);
  const { data: user, isLoading: isLoadingUser } = useGetUserInfoQuery();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!user) return;

    const userOrganizationId = Object.keys(user.allRelations)?.at(0);

    if (userOrganizationId) {
      dispatch(
        changeOrganization(parseInt(userOrganizationId))
      );
    }
  }, [user]);

  if (!gotCSRF || isLoadingUser) {
    return (
      <div className={classes.loadingContainer}>
        <CircularProgress />
      </div>
    );
  }

  return (
    <Switch>
      <Route path="/portal/login" component={LoginPage} />
      <ProtectedRoute
        exact
        path="/portal/admin"
        component={AdminPage}
        allowedRoles={permissions.accessAdmin}
        strict
      />
      <ProtectedRoute
        path="/portal/dashboard"
        component={DashboardPage}
        allowedRoles={permissions.accessDashboard}
      />
      <ProtectedRoute
        exact
        path="/portal/contact-management"
        component={ContactManagementPage}
        allowedRoles={permissions.accessContacts}
      />
      <ProtectedRoute
        exact
        path="/portal/your-projects"
        component={YourProjectsPage}
        allowedRoles={permissions.accessDashboard}
      />
      <ProtectedRoute
        exact
        path="/portal/org-profile" 
        component={YourOrganizationProfilePage}
        allowedRoles={permissions.accessOrgProfile}
      />
      <ProtectedRoute
        exact
        path="/portal/join-working-group"
        component={JoinWorkingGroupPage}
        allowedRoles={permissions.joinWorkingGroup}
      />
      <ProtectedRoute
        exact
        path="/portal/join-working-group/submitted"
        component={JoinWorkingGroupSuccessPage}
        allowedRoles={permissions.joinWorkingGroup}
      />
      <ProtectedRoute
        path="/portal/standby"
        component={StandbyPage}
        allowedRoles={permissions.accessStandby}
        strict
      />
      <Route 
        exact
        path="/portal/unauthorized"
        component={UnauthorizedPage}
      />
      <Route path="/portal" component={RootPage} />
    </Switch>
  );
}

