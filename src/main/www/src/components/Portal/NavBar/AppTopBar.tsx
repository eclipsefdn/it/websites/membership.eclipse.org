/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import {
  AppBar,
  Avatar,
  Toolbar,
  Typography,
  Theme,
} from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import efGRYLogo from '../../../assets/logos/ef-gry.svg';
import { darkOrange, themeBlack } from '../../../Constants/Constants';
import permissions from '../../../Constants/permissions';
import PreviewAutocomplete from '../../../modules/PreviewAutocomplete';
import Protected from '../../Protected';
import ProfileSnippet from '../../../modules/ProfileSnippet';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      position: 'sticky',
      height: 110,
      backgroundColor: '#fff',
      boxShadow: '0px 0px 16px rgba(0, 0, 0, 0.05)',
      [theme.breakpoints.up('md')]: {
        height: 70,
      },
      [theme.breakpoints.down('sm')]: {
        // Has to use !important to overwrite the padding-right 17px added by MUI when hamburger menu is open
        // This can avoid making user dropdown menu move left and right
        padding: '0 !important',
      },
      transition: theme.transitions.create('height', {
        easing: theme.transitions.easing.easeInOut,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    toolbarCtn: {
      height: '100%',
    },
    iconCtn: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    efLogo: {
      width: 120,
    },
    hamburgerIcon: {
      marginRight: theme.spacing(0.5),
      color: themeBlack,
    },
    userInfoCtn: {
      display: 'flex',
      top: theme.spacing(1),
      right: theme.spacing(1),
      width: '100%',
      height: 40,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    verticalDivider: {
      display: 'none',
      backgroundColor: '#EBEBF2',
      height: 30,
      width: 1,
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('md')]: {
        display: 'block',
      },
    },
    username: {
      color: themeBlack,
      marginBottom: 0,
      marginRight: theme.spacing(1),
    },
    dropDownBtn: {
      minWidth: 40,
      height: 30,
      padding: 0,
    },
    dropDownIcon: {
      color: '#A4AFB7',
    },
    dropDownItemIcon: {
      minWidth: 30,
    },
    avatarCtn: {
      display: 'none',
      width: 40,
      height: 40,
      marginLeft: theme.spacing(1),
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
      backgroundColor: darkOrange,
    },
    anchorTag: {
      textDecoration: 'none',
      color: 'inherit',
      '&:hover ': {
        textDecoration: 'none',
        color: 'inherit',
      },
    },
  })
);

interface AppTopBarProps {
  handleDrawerToggle: () => void;
}

const AppTopBar: React.FC<AppTopBarProps> = ({ handleDrawerToggle }) => {
  const classes = useStyles();

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar className={classes.toolbarCtn}>
        <div className={classes.iconCtn}>
          <img src={efGRYLogo} alt="Eclipse Foundation logo" className={classes.efLogo} />
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            className={classes.hamburgerIcon}
            onClick={handleDrawerToggle}
          >
            <MenuIcon />
          </IconButton>
        </div>

        <div className={classes.userInfoCtn}>
          <Protected allowedRoles={permissions.accessStandby} strict>
            <PreviewAutocomplete display={{ xs: 'none', md: 'block' }} width="24rem" size="small" />
          </Protected>
          <div className={classes.verticalDivider}></div>
          <ProfileSnippet />
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default AppTopBar;

