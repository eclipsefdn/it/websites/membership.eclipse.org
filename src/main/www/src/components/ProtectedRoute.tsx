/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Route, RouteProps, Redirect, useLocation } from 'react-router-dom'
import { useAppSelector } from '../hooks';
import { Role } from '../types';
import { ORIGINAL_PATH_KEY } from '../Constants/Constants';
import { roles } from '../Constants/roles';
import { checkPermission } from '../utils/portalFunctionHelpers';
import { useGetUserInfoQuery } from '../api/membership';

interface Props extends RouteProps {
    component: React.ComponentType<any>;
    allowedRoles?: Role[];
    strict?: boolean;
}

const ProtectedRoute: React.FC<Props> = ({ component: Component, allowedRoles, strict = false, ...otherProps }: Props) => {
    const { data: user, isLoading } = useGetUserInfoQuery();
    const { organizationId, inPreviewMode } = useAppSelector(state => state.organization);
    const location = useLocation();
    
    // If the user "allRelations" object has at least one key, it means the
    // user is part of an organization. If the organizationId is null, it means
    // we still haven't loaded the organization id into state.
    const isLoadingOrganization = isLoading || 
      (!organizationId && user && Object.keys(user?.allRelations).length !== 0);

    if (isLoading || isLoadingOrganization) {
      return null;
    }

    // Redirect to login page if user is not logged in, or if the user has no EMPLY role
    // The minimum requirement for a user to have access to the membership portal is to have the 
    // EMPLY role    
    if (!user || (!user.roles.includes(roles.admin) && !user.roles.includes(roles.elevated) && !user.relation.includes(roles.employee))) {
      // Set the "originalPath" in local storage to the current page. Once the
      // user is authenticated, the portal will figure out how to reroute the
      // user back to this protected page.
      localStorage.setItem(ORIGINAL_PATH_KEY, location.pathname);
      return <Redirect to="/portal/login" />;
    }

    const isAuthorized = allowedRoles === undefined 
        || (inPreviewMode && !strict)
        || checkPermission(allowedRoles, user.roles) 
        || checkPermission(allowedRoles, user.relation)

    if (!isAuthorized) {
        return <Redirect to="/portal/unauthorized" />;
    }

    return <Route 
      { ...otherProps } 
      render={(props) => <Component {...props} />} 
    />;
}

export default ProtectedRoute;
