/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { makeStyles, createStyles, Theme, ButtonBase } from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';
import clsx from 'clsx';

interface HelpButtonProps {
  className?: string;
  onClick?: React.MouseEventHandler;
}

/**
  * A help button with a question mark icon.
  *
  * This is typically used in combination with a popover to display help text.
  */
const HelpButton: React.FC<HelpButtonProps> = ({ className, onClick: handleClick }) => {
  const classes = useStyles();

  return (
    <ButtonBase className={clsx(classes.root, className)} onClick={handleClick} disableRipple>
      <HelpIcon fontSize="large" />
    </ButtonBase>
  );
}

export default HelpButton;

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      color: theme.palette.common.white,
    },
  })
});
