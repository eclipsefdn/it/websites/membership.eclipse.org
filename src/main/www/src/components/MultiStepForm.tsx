/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { FormikHelpers, FormikConfig, FormikValues } from 'formik';
import React, { useState, createContext } from 'react';
import FormContainer from './FormContainer';

interface MultiStepFormProps extends FormikConfig<any> {
  children: React.ReactNode;
  validationSchema: any;
}

const MultiStepForm: React.FC<MultiStepFormProps> = ({ children, initialValues, validationSchema, onSubmit }) => {
  const [step, setStep] = useState(0);
  const steps = React.Children.toArray(children) as React.ReactElement[];

  const [snapshot, setSnapshot] = useState(initialValues);

  const currentStep = steps[step];
  const totalSteps = steps.length;
  const isLastStep = step === totalSteps - 1;

  const handleSubmit = async (values: FormikValues, actions: FormikHelpers<any>) => {
    if (currentStep.props.onSubmit) {
      await currentStep.props.onSubmit(values, actions);
    }

    if (isLastStep) {
      return onSubmit(snapshot, actions);
    } else {
      // Reset the touched fields when navigating.
      actions.setTouched({});
      next(values);
    }
  }

  const next = (values: FormikValues) => {
    setSnapshot({ ...snapshot, [step]: values }); 
    setStep(
      Math.min(step + 1, totalSteps - 1)
    );
  };

  const prev = (values: FormikValues) => {
    setSnapshot({ ...snapshot, [step]: values });
    setStep(
      Math.max(step - 1, 0)
    );
  };

  return (
    <FormContainer onSubmit={handleSubmit} initialValues={snapshot[step]} validationSchema={validationSchema.fields[step]} allowPreview={!isLastStep}>
      <MultiStepFormContext.Provider value={{step, next, prev, hasNext: !isLastStep, hasPrev: step > 0, allFormValues: snapshot }}>
        {currentStep}
      </MultiStepFormContext.Provider>
    </FormContainer>
  );
}

export default MultiStepForm;

interface MultiStepFormContextValue {
  /** The current step. It is a 0-based index. */
  step: number;
  prev: (values: FormikValues) => void,
  next: (values: FormikValues) => void,
  hasNext: boolean,
  hasPrev: boolean,
  /** The form values for all steps of the form. This should be used in form
    * steps that require all the data. E.g. a review step. */
  allFormValues: FormikValues;
}

export const MultiStepFormContext = createContext<MultiStepFormContextValue>({ 
  step: 0, 
  prev: () => {},
  next: () => {},
  hasNext: true,
  hasPrev: false,
  allFormValues: {},
});

