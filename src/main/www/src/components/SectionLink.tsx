/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Link } from 'react-router-dom';
import { Box, Paper, Typography, Theme, makeStyles, createStyles, useTheme } from '@material-ui/core';
import { iconGray } from '../Constants/Constants';

interface SectionLinkProps {
  href?: string;
  color?: string;
  icon?: React.ReactNode;
  children?: React.ReactNode;
}

const SectionLink: React.FC<SectionLinkProps> = ({ color, href, icon, children }) => {
  const theme = useTheme();
  const classes = useStyles();

  // Set the default underline color to primary.
  if (!color) {
    color = theme.palette.primary.main;
  }

  const content = (
    <>
      <Box display="flex" alignItems="center">
        { icon 
          ? <Box className={classes.icon}>{icon}</Box>
          : null
        }
        <Typography component="div" variant="subtitle2" style={{fontSize: '1.75rem'}}>{children}</Typography>
      </Box>
      <Box className={classes.underline} style={{ backgroundColor: color }}/>
    </>
  );

  // Render content within a Link if href was provided. Otherwise, only render content.
  return (
    <Paper className={classes.root} elevation={1}>
      { href ? <Link className={classes.link} to={href}>{content}</Link> : content }
    </Paper>
  );
};

export default SectionLink;

const useStyles = makeStyles((theme: Theme) => {
  const maxWidth = '8rem';

  return createStyles({
    root: {
      position: 'relative',
      padding: theme.spacing(2),
      borderRadius: theme.shape.borderRadius,
      transition: 'all 0.2s ease-out',
      '&:hover': {
        transform: 'scale(1.03)',
      },
    },
    link: {
      '&:active': {
        textDecoration: 'none',
      },
      '&:hover': {
        textDecoration: 'none',
      }
    },
    icon: {
      width: '100%',
      maxWidth,
      '& svg': {
        maxWidth: `calc(${maxWidth} - 4rem)`,
        width: '100%',
        height: '100%',
        color: iconGray,
      }
    },
    underline: {
      maxWidth,
      content: ' ',
      position: 'absolute',
      display: 'block',
      left: 0,
      bottom: 0,
      width: '100%',
      height: '0.5rem',
      borderRadius: theme.shape.borderRadius,
      borderTopLeftRadius: 0,
      borderTopRightRadius: 0,
    }
  })
});
