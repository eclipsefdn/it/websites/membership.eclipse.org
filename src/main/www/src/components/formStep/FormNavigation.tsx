/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useContext } from 'react';
import { Grid, Button, CircularProgress, createStyles, makeStyles, Theme } from '@material-ui/core';
import { useForm } from '../../hooks';
import { MultiStepFormContext } from '../MultiStepForm';

const FormNavigation: React.FC = () => {
  const { formValues, isSubmitting } = useForm();
  const { prev, hasPrev, hasNext } = useContext(MultiStepFormContext);
  const classes = useStyles();

  const handlePrev = () => prev(formValues as any);
  
  return (
    <Grid container justify="center" spacing={2} style={{maxWidth: '30rem', margin: 'auto'}}>
      <Grid item xs={6}> 
        <Button 
          variant="contained" 
          color="primary" 
          size="large"
          onClick={handlePrev}
          disabled={!hasPrev}
          fullWidth
        >
          Back
        </Button>
      </Grid>
      <Grid item xs={6}> 
        <Button 
          type="submit"
          variant="contained" 
          color="primary" 
          size="large" 
          fullWidth
        >
          { hasNext ? 'Next' : 'Submit' }
          { isSubmitting ? <CircularProgress className={classes.progress} size="1.5rem" /> : null }
        </Button>
      </Grid>
    </Grid>
  );
};

export default FormNavigation;

const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    progress: {
      position: 'absolute',
      right: '2rem',
      color: '#ffffff88',
    }
  })
);

