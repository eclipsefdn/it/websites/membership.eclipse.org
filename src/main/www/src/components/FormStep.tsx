/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useContext } from 'react';
import FormNavigation from './formStep/FormNavigation';
import { useForm } from '../hooks';
import Spacer from './Spacer';
import { Form } from 'formik';
import { Prompt } from 'react-router';
import { MultiStepFormContext } from './MultiStepForm';
import { Alert } from '@material-ui/lab';

const FormStep: React.FC = ({ children }) => {
  const { handleSubmit, dirty, status } = useForm();
  const { hasNext, hasPrev } = useContext(MultiStepFormContext);
  
  // A variable which indicates whether the form is in progress or not. We
  // consider a form in progress if the fields are dirty or if the user is not
  // on the first or last step. 
  const isFormInProgress = dirty || (hasPrev && hasNext);

  return (
    <Form onSubmit={handleSubmit}>
      <Prompt 
        when={isFormInProgress} 
        message="Are you sure you want to leave this page? Any unsaved changes will be lost" 
      />
      { status 
        ?
          <>
            <Alert severity={status.severity}>
              { status.message }
            </Alert>
            <Spacer y={1} />
          </>
        : null 
      }
      {children}
      <FormNavigation />
    </Form>
  )
}

export default FormStep;

