/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Box, Card, CardProps, CardContent, CardActions, Theme, createStyles, makeStyles } from '@material-ui/core';
import { iconGray } from '../Constants/Constants';

interface IconCardProps extends CardProps {
  /** The icon to display in the top-left corner of the card. */
  icon: React.ReactNode;
  /** The background color of the card. */
  color?: string;
  /** The color of the underline beneath the icon. */
  accentColor?: string;
  className?: string;
  children: React.ReactNode;
}

/** 
  * A card component which displays an icon in the top left corner.
  */
function IconCard({ icon, color, accentColor = '#dd730a', children, className, ...props }: IconCardProps) {
  const classes = useStyles({ color, accentColor });

  return (
    <Card className={`${classes.root} ${className}`} {...props}>
      <CardContent className={classes.iconContainer}>
        <Box className={classes.icon}>
          {icon} 
        </Box>
      </CardContent>
      {children}
    </Card>
  );
};

export default IconCard;

IconCard.Content = ({ children }: React.PropsWithChildren<{}>) => {
  const classes = useStyles({});

  return (
    <CardContent className={classes.bodyContent}>
      {children}
    </CardContent>
  );
};

IconCard.Actions = ({ children }: React.PropsWithChildren<{}>) => {
  return (
    <CardActions>
      {children}
    </CardActions>
  );
};

const useStyles = makeStyles<Theme, Pick<IconCardProps, 'color' | 'accentColor'>>((theme: Theme) => {
  return createStyles({
    root: {
      // Requires an overflow unset to have the icon display outside of the
      // boundaries of Card.
      overflow: 'unset',
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      backgroundColor: (props) => props?.color ?? theme.palette.neutral['50'],
    },
    iconContainer: {
      position: 'relative',
    },
    icon: {
      width: '5.5rem',
      marginTop: '-4.5rem',
      borderBottom: (props) => `0.5rem solid ${props.accentColor}`,
      aspectRatio: '1',
      '& svg': {
        width: '100%',
        height: '100%',
        color: iconGray,
      },
    },
    bodyContent: {
      flexGrow: 1,
      paddingTop: 0,
    }
  });
});
