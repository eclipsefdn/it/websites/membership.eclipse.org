/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import React from 'react';

/**
 * User and form Id context shared among the whole App
 *
 * For more about context, please refer to: https://reactjs.org/docs/context.html
 *
 * It is simliar to state, but you can export and import anywhere,
 * no need to pass all the way down to the child component
 */

const MembershipContext = React.createContext({
  currentUserForm: {},
  setCurrentUserForm: () => {
    // Intentionally left empty
  },
  currentFormId: '',
  setCurrentFormId: () => {
    // Intentionally left empty
  },
  furthestPage: '',
  setFurthestPage: () => {
    // Intentionally left empty
  },
  needLoadingSignIn: '',
  setNeedLoadingSignIn: () => {
    // Intentionally left empty
  },
  currentStepIndex: 0,
  setCurrentStepIndex: (stepIndex) => {
    // Intentionally left empty
  },
  orgId: 0,
  contactFilterRole: '',
  setContactFilterRole: (role) => {
    // Intentionally left empty
  },
});

export default MembershipContext;
