/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { setupStore } from './store';

const enableMocking = async () => {
  if (process.env.NODE_ENV !== 'development') {
    return;
  }

  const { worker } = await import('./api/mocks/browser');
  return worker.start();
};

const initializeApp = () => {
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={setupStore()}>
        <App />
      </Provider>
    </React.StrictMode>,
    document.getElementById('root')
  );
};

enableMocking().then(initializeApp);
