/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type OrganizationState = {
  organizationId: number | null,
  inPreviewMode: boolean,
};

const initialState: OrganizationState = {
  organizationId: null,
  inPreviewMode: false,
}; 

const organizationSlice = createSlice({
  initialState,
  name: 'organization',
  reducers: {
    changeOrganization(state: OrganizationState, action: PayloadAction<number>) {
      // The payload is the desired organization ID.
      state.organizationId = action.payload;
      state.inPreviewMode = false;
    },
    clearOrganization(state: OrganizationState) {
      // Unassign user from organization.
      state.organizationId = null;
      state.inPreviewMode = false;
    },
    enablePreviewMode(state: OrganizationState, action: PayloadAction<number>) {
      // The payload is the organization ID to preview.
      state.organizationId = action.payload;
      state.inPreviewMode = true;
    },
    disablePreviewMode(state: OrganizationState) {
      // When disabling preview mode, user is expected to go back to standby.
      // They should not be assigned an organization ID.
      state.organizationId = null;
      state.inPreviewMode = false;
    },
  }
});

export const { 
  changeOrganization,
  clearOrganization,
  enablePreviewMode, 
  disablePreviewMode 
} = organizationSlice.actions;

export default organizationSlice.reducer;
