/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { NewsroomResourceType, NewsroomResource } from '../types';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const newsroomResourceMapper = (data: any): NewsroomResource => {
  return {
    id: data.id,
    title: data.title,
    date: data.date,
    directLink: data.direct_link,
  };
};

const newsroomTypeParams: Record<NewsroomResourceType, string> = {
  'survey-report': 'survey_report',
  'market-report': 'market_report',
  'social-media-kit': 'social_media_kit',
  'white-paper': 'white_paper',
  'case-study': 'case_study',
};

export const newsroomAPI = createApi({
  reducerPath: 'newsroom-api',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://newsroom.eclipse.org/api' }),
  endpoints: (builder) => ({
    getNewsroomResources: builder.query<NewsroomResource[], NewsroomResourceType>({
      query: (type) => `resources?parameters[resource_type][]=${newsroomTypeParams[type]}`,
      transformResponse: (response: any) => response.resources.map(newsroomResourceMapper),
    }),
  }),
});

export const { 
  useGetNewsroomResourcesQuery,
} = newsroomAPI;



