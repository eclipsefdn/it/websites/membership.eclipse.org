/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Profile } from '../types';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const profileMapper = (data: any): Profile => {
  return {
    username: data.name,
    firstName: data.first_name,
    lastName: data.last_name,
    picture: data.picture,
    jobTitle: data.job_title,
  };
};

export const accountAPI = createApi({
  reducerPath: 'account-api',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://api.eclipse.org/account' }),
  endpoints: (builder) => ({
    getProfile: builder.query<Profile, string>({
      query: (userId) => `profile/${userId}`,
      transformResponse: (response: any) => profileMapper(response),
    }),
  }),
});

export const { 
  useLazyGetProfileQuery,
} = accountAPI;


