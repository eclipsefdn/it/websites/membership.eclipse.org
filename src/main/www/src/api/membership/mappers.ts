/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { 
  CBIData, 
  Contributor,
  Committer,
  CommitterData,
  Organization, 
  OrganizationContactDetailed, 
  OrganizationRepresentative,
  OrganizationProduct,
  User
} from '../../types';
import { transformSnakeToCamel } from '../shared';

const organizationProductMapper = (data: any): OrganizationProduct => {
  return {
    id: data.product_id,
    name: data.name,
    description: data.description,
    url: data.product_url,
  }
}

const userMapper = (data: any): User => {
  let allRelations: Record<number, string[]> = {};
  for (let [organizationIdString, relations] of Object.entries(data.organizational_roles)) {
    const organizationId = parseInt(organizationIdString);
    allRelations[organizationId] = relations as string[];
  }

  // For now, one user will only belong to one organization. Therefore, get the
  // first organization ID.
  const organizationId = Object.keys(allRelations).at(0) as number | undefined;

  return {
    username: data.name,
    firstName: data.given_name,
    lastName: data.family_name,
    relation: organizationId ? allRelations[organizationId] : [],
    allRelations, // Needed to retrieve user's real organization ID.
    roles: data.additional_roles,
  };
};

const representativeMapper = (data: any): OrganizationRepresentative => {
  return {
    personId: data.id,
    firstName: data.first_name,
    lastName: data.last_name,
    relations: data.relations,
  };
};

const committerDataMapper = (data: any): CommitterData => {
  return {
    activity: data.activity.map((a: any) => ({
      count: a.count,
      period: {
        year: a.period.substring(0, 4),
        month: a.period.substring(4, 6),
      }
    }))
  }
};

const organizationMapper = (data: any): Organization => {
  data = transformSnakeToCamel(data);
  return data as Organization;
};

const organizationContactDetailedMapper = (data: any): OrganizationContactDetailed => {
  const transformedData = transformSnakeToCamel(data) as any;

  return {
    id: transformedData.id,
    firstName: transformedData.firstName,
    lastName: transformedData.lastName,
    email: transformedData.email,
    relations: transformedData.relations,
    jobTitle: transformedData.title,
    comments: transformedData.comments,
  };
}

const projectSlimMapper = (data: any) => {
  return transformSnakeToCamel({
    id: data.project_id,
    name: data.name,
    description: data.description,
    active: data.active,
  });
}

/**
  * Maps the response data from the cbi endpoint to the CBIData model.
  *
  * @param data - the response data
  * @returns Organization CBI benefits
  */
const getOrganizationCBIMapper = (data: any): CBIData | null => {
  const organizationBenefits: any = transformSnakeToCamel(data);

  return {
    resourcePacks: {
      inUse: organizationBenefits.resourcePacksUsed,
      allocated: organizationBenefits.resourcePacks,
    },
    runners: {
      githubLargeRunners: {
        inUse: organizationBenefits.ghLargeRunnersUsed,
        allocated: organizationBenefits.ghLargeRunners,
      }
    },
    dedicatedAgents: {
      inUse: organizationBenefits.dedicatedAgentsUsed,
      allocated: organizationBenefits.dedicatedAgents,
    }
  };
};

export default {
  // model specific mappers
  userMapper,
  organizationMapper, 
  organizationContactDetailedMapper, 
  organizationProductMapper,
  representativeMapper, 
  committerDataMapper,
  projectSlimMapper, 
  // route specific mappers
  getOrganizationCBIMapper,
}
