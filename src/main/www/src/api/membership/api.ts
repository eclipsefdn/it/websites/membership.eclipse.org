/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { transformSnakeToCamel } from '../shared';
import { 
  CBIData, 
  Contributor,
  Committer,
  CommitterData,
  Organization, 
  SlimOrganization,
  OrganizationalRole,
  OrganizationInfo,
  OrganizationContact, 
  OrganizationContactDetailed, 
  OrganizationRepresentative,
  OrganizationProduct,
  LogoFormat,
  ProjectSlim, 
  WorkingGroup, 
  User
} from '../../types';
import mappers from './mappers';
import { createApi, fetchBaseQuery, FetchBaseQueryError } from '@reduxjs/toolkit/query/react';

const baseUrl = process.env.NODE_ENV === 'testing' 
  ? 'http://localhost/api' 
  : '/api';

export const membershipAPI = createApi({
  reducerPath: 'membership-api',
  baseQuery: fetchBaseQuery({ 
    baseUrl,
    prepareHeaders: async (headers) => {
      try {
        const response = await fetch('/api/csrf');
        const csrfToken = response.headers.get('x-csrf-token');
        
        if (!csrfToken) {
          throw new Error('Could not retrieve CSRF token while preparing headers.');
        }

        headers.set('x-csrf-token', csrfToken);
      } catch (error) {
        console.error(error);
      }
    },
  }),
  tagTypes: ['Organization', 'OrganizationContact', 'OrganizationProduct'],
  endpoints: (builder) => ({
    getOrganization: builder.query<Organization, number>({
      query: (organizationId) => `organizations/${organizationId}`,
      transformResponse: (response: any) => mappers.organizationMapper(response),
      providesTags: (_result, _error, organizationId) => [{ type: 'Organization', organizationId }],
    }),
    getOrganizations: builder.query<Organization[], void>({
      query: () => `organizations`,
      transformResponse: (response: any) => response.map(mappers.organizationMapper),
    }),
    getSlimOrganizations: builder.query<SlimOrganization[], void>({
      query: () => `organizations/slim`,
      transformResponse: (response: any) => response.map(transformSnakeToCamel),
    }),
    updateOrganizationInfo: builder.mutation<void, OrganizationInfo>({
      query: ({ organizationId, description, url }) => ({
        url: `organizations/${organizationId}`,
        method: 'POST',
        body: {
          description: description,
          company_url: url,
        },
      }),
      invalidatesTags: (_result, _error, { organizationId }) => [{ type: 'Organization', organizationId }],
    }), 
    updateOrganizationLogo: builder.mutation<void, { organizationId: number, logo: File, format: LogoFormat }>({
      query: ({ organizationId, logo, format }) => {
        // Assemble the form data to send to the server for the logo upload.
        const formData = new FormData();
        formData.append('image', logo);
        formData.append('image_mime', logo.type);
        formData.append('image_format', format);

        return {
          url: `organizations/${organizationId}/logos`,
          method: 'POST',
          body: formData, 
        };
      },
      invalidatesTags: (_result, _error, { organizationId }) => [{ type: 'Organization', organizationId }],
    }),
    getOrganizationProducts: builder.query<OrganizationProduct[], number>({
      query: (organizationId) => `organizations/${organizationId}/products`,
      transformResponse: (response: any) => response.map(mappers.organizationProductMapper),
      providesTags: (_result, _error, organizationId) => [{ type: 'OrganizationProduct', organizationId }],
    }),
    addOrganizationProduct: builder.mutation<void, Omit<OrganizationProduct, 'id'> & { organizationId: number }>({
      query: ({ organizationId, ...product }) => ({
        url: `organizations/${organizationId}/products`,
        method: 'POST',
        body: {
          organization_id: organizationId,
          name: product.name,
          description: product.description,
          product_url: product.url,
        },
      }),
      invalidatesTags: (_result, _error, { organizationId }) => [{ type: 'OrganizationProduct', organizationId }],
    }),
    removeOrganizationProduct: builder.mutation<void, { organizationId: number, productId: number }>({
      query: ({ organizationId, productId }) => ({
        url: `organizations/${organizationId}/products/${productId}`,
        method: 'DELETE',
      }),
      invalidatesTags: (_result, _error, { organizationId }) => [{ type: 'OrganizationProduct', organizationId }],
    }),
    getOrganizationDetailedContact: builder.query<OrganizationContactDetailed, { organizationId: number, contactId: string }>({
      queryFn: async ({ organizationId, contactId }, _queryAPI, _extraOptions, fetchWithBQ) => {
        // Fetch all the contacts to find the contact with our contactId.
        // We need to fetch data from this endpoint because the individual
        // contact endpoint doesn't have all the required data.
        let contactsResult = await fetchWithBQ(`/organizations/${organizationId}/contacts`); 
        if (contactsResult.error) {
          return { error: contactsResult.error as FetchBaseQueryError };
        }        
        const contacts = contactsResult.data as any[];
        const partialContact = contacts.find((contact: any) => contact.id === contactId); 

        // Now we must fetch the individual contact for the remaining information.
        const individualContactResult = await fetchWithBQ(`organizations/${organizationId}/contacts/${contactId}`);
        if (individualContactResult.error) {
          return { error: individualContactResult.error as FetchBaseQueryError };
        }
        const individualContact = individualContactResult.data as any[];
        
        // Now we transform the data into our OrganizationContactDetailed
        // model.
        const detailedContact = mappers.organizationContactDetailedMapper({ ...individualContact[0], ...partialContact }) as OrganizationContactDetailed;

        return { data: detailedContact }; 
      },
    }),
    getOrganizationContacts: builder.query<OrganizationContact[], number>({
      query: (organizationId) => `organizations/${organizationId}/contacts`,
      transformResponse: (response: any) => response.map(transformSnakeToCamel),
      providesTags: (_result, _error, organizationId) => [{ type: 'OrganizationContact', organizationId }],
    }),
    requestOrganizationContactRemoval: builder.mutation<void, { organizationId: number, contactId: string, details: string }>({
      query: ({ organizationId, contactId, details }) => ({
        url: `organizations/${organizationId}/contacts/${contactId}`,
        method: 'DELETE',
        responseHandler: 'text',
        body: {
          organization_id: organizationId,
          username: contactId,
          reason: details,
        },
      }),
    }),
    addRelationToContact: builder.mutation<void, { relation: OrganizationalRole, organizationId: number, contactId: string }>({
      query: ({ relation, contactId, organizationId }) => ({
        url: `organizations/${organizationId}/contacts/${contactId}/${relation}`,
        method: 'POST',
        responseHandler: 'text',
      }),
      invalidatesTags: (_result, _error, { organizationId }) => [{ type: 'OrganizationContact', organizationId }],
    }),
    removeRelationFromContact: builder.mutation<void, { relation: OrganizationalRole, organizationId: number, contactId: string }>({
      query: ({ relation, contactId, organizationId }) => ({
        url: `organizations/${organizationId}/contacts/${contactId}/${relation}`,
        method: 'DELETE',
        responseHandler: 'text',
      }),
      invalidatesTags: (_result, _error, { organizationId }) => [{ type: 'OrganizationContact', organizationId }],
    }),
    getOrganizationCBI: builder.query<CBIData, number>({
      query: (organizationId) => `organizations/${organizationId}/cbi`,
      transformResponse: (response: any) => mappers.getOrganizationCBIMapper(
        response.memberOrganizationsBenefits
          .find((organizationBenefits: any) => organizationBenefits.membership === 'Eclipse')
      ) as  CBIData,
    }),
    getOrganizationCommitters: builder.query<Committer[], number>({
      query: (organizationId) => `organizations/${organizationId}/committers`,
      transformResponse: (response: any) => response.map(transformSnakeToCamel),
    }),
    getOrganizationContributors: builder.query<Contributor[], number>({
      query: (organizationId) => `organizations/${organizationId}/contributors`,
      transformResponse: (response: any) => response.map(transformSnakeToCamel),
    }),
    getOrganizationRepresentatives: builder.query<OrganizationRepresentative[], number>({
      query: (organizationId) => `organizations/${organizationId}/representatives`,
      transformResponse: (response: any) => response.map(mappers.representativeMapper) as OrganizationRepresentative[],
    }),
    getUserInfo: builder.query<User | null, void>({
      query: () => `userinfo`,
      transformResponse: (response: any, meta) => {
        if (meta?.response?.status === 200) {
          return mappers.userMapper(response);
        }

        return null;
      },
    }),
    getOrganizationWorkingGroups: builder.query<WorkingGroup[], number>({
      query: (organizationId) => `organizations/${organizationId}/working_groups`,
      transformResponse: (response: any) => response
        .map(transformSnakeToCamel)
        .filter((workingGroup: WorkingGroup) => workingGroup.parentOrganization === 'eclipse'),
    }),
    getOrganizationProjects: builder.query<ProjectSlim[], number>({
      query: (organizationId) => `organizations/${organizationId}/projects`,
      transformResponse: (response: any) => response.map(mappers.projectSlimMapper)
    }),
    getOrganizationCommitYearlyActivity: builder.query<CommitterData, number>({
      query: (organizationId) => `organizations/${organizationId}/activity/yearly`,
      transformResponse: (response: any) => mappers.committerDataMapper(response),
    }),
    getOrganizationCommitterYearlyActivity: builder.query<CommitterData, number>({
      query: (organizationId) => `organizations/${organizationId}/committers/activity/yearly`,
      transformResponse: (response: any) => mappers.committerDataMapper(response),
    }),
  }),
});

export const { 
  useGetUserInfoQuery,
  useGetOrganizationQuery, 
  useGetOrganizationsQuery, 
  useUpdateOrganizationInfoMutation,
  useUpdateOrganizationLogoMutation,
  useGetSlimOrganizationsQuery, 
  useGetOrganizationProductsQuery,
  useAddOrganizationProductMutation,
  useRemoveOrganizationProductMutation,
  useGetOrganizationContactsQuery,
  useGetOrganizationDetailedContactQuery,
  useRequestOrganizationContactRemovalMutation,
  useAddRelationToContactMutation,
  useRemoveRelationFromContactMutation,
  useGetOrganizationCBIQuery,
  useGetOrganizationCommittersQuery,
  useGetOrganizationContributorsQuery,
  useGetOrganizationRepresentativesQuery,
  useGetOrganizationWorkingGroupsQuery,
  useGetOrganizationProjectsQuery,
  useGetOrganizationCommitYearlyActivityQuery,
  useGetOrganizationCommitterYearlyActivityQuery,
} = membershipAPI;

