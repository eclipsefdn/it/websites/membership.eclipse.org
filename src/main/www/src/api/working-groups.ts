/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { transformSnakeToCamel } from './shared';
import { WorkingGroup } from '../types';

export const workingGroupsAPI = createApi({
  reducerPath: 'working-groups-api',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://api.eclipse.org/working-groups' }),
  endpoints: (builder) => ({
    getWorkingGroups: builder.query<WorkingGroup[], void>({
      query: () => `/`,
      transformResponse: (response: any) => response
        .map(transformSnakeToCamel)
        .filter((workingGroup: WorkingGroup) => workingGroup.parentOrganization === 'eclipse'),
    }),
  }),
});

export const { 
  useGetWorkingGroupsQuery,
} = workingGroupsAPI;

