/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { http, delay, HttpResponse } from 'msw';
import { setupWorker } from 'msw/browser';
import { setupServer } from 'msw/node';

import { 
  generateMockProject, 
  generateMockCommitter, 
  generateMockContributor, 
  generateMockProjectSlim,
} from './data/projects';
import {
  generateMockCommitYearlyActivity,
  generateMockCommitterYearlyActivity,
  generateMockOrganization,
  generateMockProduct,
  generateMockCBI,
  generateMockContact,
  generateMockContactDetailed,
  generateMockUserInfo,
  generateMockOrganizationActivity,
  generateMockRepresentative,
  generateMockProfile,
  generateMockOrganizationWorkingGroups,
} from './data/membership';

// "/api/login" is the only endpoint that cannot be mocked. Since we hit this
// via window.location.assign, it will skip over MSW since it is outside of the
// SPA. Therefore, we need to make a check in the source code to not redirect
// if we are in DEV mode.

const testAdminUser = false;

const mockOrganizations = Array.from({ length: 20 }).map(generateMockOrganization);
const mockUserInfo = testAdminUser 
  ? generateMockUserInfo()
  : generateMockUserInfo(mockOrganizations.at(0)!.organization_id); 
const mockProjects = Array.from({ length: 5 }).map(generateMockProject);
const mockProfile = generateMockProfile(mockUserInfo.given_name, mockUserInfo.family_name);
const mockContactsByOrganization = mockOrganizations.reduce((acc, organization) => {
  acc[organization.organization_id] = Array.from({ length: 10 }).map(generateMockContact);
  return acc;
}, {} as {[organizationId: string]: ReturnType<typeof generateMockContact>[]});
const mockProductsByOrganization = mockOrganizations.reduce((acc, organization) => {
  acc[organization.organization_id] = Array
    .from({ length: 4 })
    .map(() => generateMockProduct(organization.organization_id));
  return acc;
}, {} as {[organizationId: string]: ReturnType<typeof generateMockProduct>[]});

const mockWorkingGroupApplications = new Map<number, any>();

const handlers = [
  http.get('/api/organizations/:organizationId/projects', () => {
    return HttpResponse.json(mockProjects);
  }),
  http.get('/api/organizations/:organizationId/committers', () => {
    return HttpResponse.json(
      Array.from({ length: 5 }).map(generateMockCommitter)
    );
  }),
  http.get('/api/organizations/:organizationId/contributors', () => {
    return HttpResponse.json(
      Array.from({ length: 5 }).map(generateMockContributor)
    );
  }),
  http.get('/api/organizations/:organizationId/working_groups', () => {
    return HttpResponse.json(generateMockOrganizationWorkingGroups());
  }),
  http.get('/api/organizations', () => {
    return HttpResponse.json(mockOrganizations);
  }),
  http.get('/api/organizations/slim', () => {
    return HttpResponse.json(
      mockOrganizations.map((organization) => ({
        organization_id: organization.organization_id,
        name: organization.name,
        logos: organization.logos,
      }),
      )
    );
  }),
  http.get('/api/organizations/:organizationId', (req) => {
    // @todo: pass organizationId as param
    return HttpResponse.json(
      mockOrganizations.find((organization) => organization.organization_id === parseInt(req.params.organizationId as string))
    );
  }),
  http.post('/api/organizations/:organizationId', async (req) => {
    let organization = mockOrganizations.find(organization => organization.organization_id === parseInt(req.params.organizationId as string));
    
    if (!organization) {
      return new HttpResponse(undefined, { status: 400 });
    }

    const payload = await req.request.json();

    if (!payload || typeof payload !== 'object') {
      return new HttpResponse(undefined, { status: 400 });
    }

    organization.description.long = payload.description;
    organization.website = payload.company_url;

    return HttpResponse.json(organization);
  }),
  http.post('/api/organizations/:organizationId/logos', async (req) => {
    const data = await req.request.formData();

    const organizationId = parseInt(req.params.organizationId as string);
    const imageFile = data.get('image') as File | null;
    const format = data.get('image_format') as string | null;

    if (!imageFile || !format) {
      return new HttpResponse(undefined, { status: 400 });
    }

    const imageUrl = URL.createObjectURL(imageFile);

    const organization = mockOrganizations.find(organization => organization.organization_id === organizationId);

    if (!organization) return new HttpResponse(undefined, { status: 404 });
    
    switch (format) {
      case 'WEB':
        organization.logos.web = imageUrl;
        break;
      case 'PRINT':
        organization.logos.print = imageUrl;
        break;
      default:
        return new HttpResponse(undefined, { status: 400 });
    }

    return HttpResponse.json(organization);
  }),
  http.get('/api/organizations/:organizationId/products', (req) => {
    return HttpResponse.json(mockProductsByOrganization[req.params.organizationId as string]);
  }),
  http.post('/api/organizations/:organizationId/products', async (req) => {
    const organizationId = req.params.organizationId as string;
    const payload = await req.request.json();

    if (!payload || typeof payload !== 'object') {
      return new HttpResponse(undefined, { status: 400 });
    }

    mockProductsByOrganization[organizationId].push({
      product_id: generateMockProduct(0).product_id, // Generating a product for it's ID. Disregard the rest of it.
      ...payload,
    } as ReturnType<typeof generateMockProduct>);

    return HttpResponse.json();
  }),
  http.delete('/api/organizations/:organizationId/products/:productId', (req) => {
    const organizationId = parseInt(req.params.organizationId as string);
    const productId = parseInt(req.params.productId as string);
  
    mockProductsByOrganization[organizationId] = mockProductsByOrganization[organizationId]
      .filter(product => product.product_id !== productId);

    return new HttpResponse(undefined, { status: 200 });
  }),
  http.get('/api/organizations/:organizationId/cbi', () => {
    return HttpResponse.json(generateMockCBI());
  }),
  http.get('/api/organizations/:organizationId/contacts', async (req) => {
    await delay(800);

    return HttpResponse.json(mockContactsByOrganization[req.params.organizationId as string]);
  }),
  http.post('/api/organizations/:organizationId/contacts/:contactId/:relation', async (req) => {
    const organizationId = req.params.organizationId as string;
    const relation = req.params.relation as string;
    const contactId = req.params.contactId as string;

    const contact = mockContactsByOrganization[organizationId].find(contact => contact.id === contactId);
    if (!contact) {
      return new HttpResponse(undefined, { status: 404 });
    }

    // If contact already has the relation, send 400.
    if (contact.relations.includes(relation)) {
      return new HttpResponse(undefined, { status: 400 });
    }

    // Mutate the contact in memory.
    contact.relations.push(relation);

    await delay(1000);

    // Return a 200 status.
    return new HttpResponse(undefined, { status: 200 });
  }),
  http.delete('/api/organizations/:organizationId/contacts/:contactId/:relation', (req) => {
    const organizationId = req.params.organizationId as string;
    const relation = req.params.relation as string;
    const contactId = req.params.contactId as string;

    const contact = mockContactsByOrganization[organizationId].find(contact => contact.id === contactId);
    if (!contact) {
      return new HttpResponse(undefined, { status: 404 });
    }

    // If contact doesn't have the relation, send 400.
    if (!contact.relations.includes(relation)) {
      return new HttpResponse(undefined, { status: 400 });
    }

    // Mutate the relations in memory to not include the relation.
    contact.relations = contact.relations.filter(r => r !== relation);

    return new HttpResponse(undefined, { status: 200 });
  }),
  http.get('/api/organizations/:organizationId/projects', () => {
    return HttpResponse.json(
      Array
        .from({ length: 10 })
        .map(generateMockProjectSlim)
    );
  }),
  http.get('/api/organizations/:organizationId/activity/yearly', () => {
    return HttpResponse.json({ activity: Array.from({ length: 20 }).map(generateMockCommitYearlyActivity) });
  }),
  http.get('/api/organizations/:organizationId/committers/activity/yearly', () => {
    return HttpResponse.json({ activity: Array.from({ length: 20 }).map(generateMockCommitterYearlyActivity) });
  }),
  http.get('/api/organizations/:organizationId/contacts/:contactId', () => {
    return HttpResponse.json(generateMockContactDetailed());
  }),
  http.delete('/api/organizations/:organizationId/contacts/:contactId', () => {
    // Since this endpoint is for "request to remove contact", we won't mutate
    // the contacts. We will pretend the request is in review.
    return new HttpResponse(undefined, { status: 200 });
  }),
  http.get('/api/organizations/:organizationId/activity', () => {
    return HttpResponse.json(generateMockOrganizationActivity())
  }),
  http.get('/api/organizations/:organizationId/representatives', () => {
    return HttpResponse.json(Array.from({ length: 7 }).map(generateMockRepresentative))
  }),
  http.get('/api/userinfo', () => {
    return HttpResponse.json(mockUserInfo, { status: 200 });
  }),
  http.get('/api/csrf', () => {
    return new HttpResponse(undefined, { 
      status: 200, 
      headers: {
        'x-csrf-token': 'some-random-csrf-token',
      },
    })
  }),
  http.get('/application_api/working_groups/application/:applicationId', (req) => {
    const applicationId = parseInt(req.params.applicationId as string);
    const application = mockWorkingGroupApplications.get(applicationId);

    if (!application) {
      return new HttpResponse(undefined, { status: 404 });
    }
  }),
  http.post('/application_api/working_groups/application', async (req) => {
    const applicationId = mockWorkingGroupApplications.size;
    let application = await req.request.json() as any;

    application.id = applicationId;
    mockWorkingGroupApplications.set(applicationId, application);

    return HttpResponse.json(application);
  }),
  http.put('/application_api/working_groups/application/:applicationId', async (req) => {
    const applicationId = parseInt(req.params.applicationId as string);
    let application = mockWorkingGroupApplications.get(applicationId);

    if (!application) {
      return new HttpResponse(undefined, { status: 404 });
    }

    const payload = await req.request.json() as object;
    
    // Merge the existing application with the updated application
    application = { ...application, ...payload }; 

    return HttpResponse.json(application);
  }),
  http.post('/application_api/working_groups/application/:applicationId/complete', async (req) => {
    const applicationId = parseInt(req.params.applicationId as string);
    let application = mockWorkingGroupApplications.get(applicationId);

    if (!application) {
      return new HttpResponse(undefined, { status: 404 });
    }

    application.state = 'SUBMITTED';

    return new HttpResponse(undefined, { status: 200 });
  }),
  http.get('https://api.eclipse.org/account/profile/:userId', () => {
    return HttpResponse.json(mockProfile);
  }),
];

