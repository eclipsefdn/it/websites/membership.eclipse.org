/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
//
// This is used to setup the mock service worker for dev. This is not the
// server that is used to mock API endpoints for tests.

import { setupWorker } from 'msw/browser';
import handlers from './handlers';

// Setup a service worker for dev.
export const worker = setupWorker(...handlers);

