import { faker } from '@faker-js/faker';
import { mockWorkingGroups } from './working-groups';
import { roles } from '../../../Constants/roles';

const usedOrganizationIds = new Set<number>();
const generateOrganizationId = (): number => {
  let id = 300;
  // Repeat until unique
  while (usedOrganizationIds.has(id)) {
    id = faker.number.int({ min: 301, max: 1200 });
  }
  usedOrganizationIds.add(id);

  return id;
}

export const generateMockOrganizationWorkingGroups = () => {
  // Grab a random amount of working groups. Turn the array into a set to
  // remove duplicates. Once duplicates are removed, convert back to array.
  return Array.from(
    new Set(
      faker.helpers.arrayElements(mockWorkingGroups, { min: 1, max: 5 })
    )
  );
};

export const generateMockProduct = (organizationId: number) => {
  return {
    organization_id: organizationId,
    product_id: faker.number.int({ min: 1000, max: 9999 }),
    name: faker.commerce.productName(),
    description: faker.lorem.sentence(),
    product_url: faker.internet.url(),
  };
}
export const generateMockOrganization = () => {
  const name = faker.company.name();

  return {
    organization_id: generateOrganizationId(),
    name,
    member_since: faker.date.past(),
    renewal_date: faker.date.future(),
    description: {
      long: faker.lorem.paragraphs(2),
    },
    logos: {
      print: null as string | null,
      web: faker.image.urlPlaceholder({ format: 'png', text: name, width: 300, height: 300 }),
    },
    website: faker.internet.url(),
    levels: faker.helpers.arrayElements([
      {
        level: 'SD',
        description: 'Strategic Developer',
        sort_order: '',
      },
      {
        level: 'AS',
        description: 'Associate S'
      },
    ]),
    wgpas: [],
  };
};

const generateMockMemberOrganizationBenefits = () => {
  const maxDedicatedAgents = faker.number.int(10);
  const maxGitHubLargeRunners = faker.number.int(10);
  const maxResourcePacks = faker.number.int(10);
  return {
    id: faker.number.int({ min: 3, max: 4 }),
    displayName: faker.company.name(),
    dedicatedAgents: maxDedicatedAgents,
    ghLargeRunners: maxGitHubLargeRunners,
    membership: faker.helpers.arrayElement(['Eclipse']),
    resourcePacks: maxResourcePacks,
    dedicatedAgents_used: faker.number.int(maxDedicatedAgents),
    ghLargeRunners_used: faker.number.int(maxGitHubLargeRunners),
    resourcePacks_used: faker.number.int(maxResourcePacks),
  };
};

const generateMockSponsoredProject = () => {
  return {
    project: {
      id: faker.company.name(),
    },
    sponsoringOrganizations: [{
      id: faker.number.int({ min: 3, max: 4 }),
      displayName: faker.company.name(),
      resourcePacks: faker.number.int(),
      dedicatedAgents: faker.number.int(),
      ghLargeRunners: faker.number.int(),
      comment: faker.lorem.lines(1),
      requestTickets: [faker.internet.url()],
    }]
  };
};

export const generateMockCBI = () => {
  return {
    memberOrganizationsBenefits: [generateMockMemberOrganizationBenefits()],
    sponsoredProjects: Array.from({ length: 5 }).map(generateMockSponsoredProject),
  };
};

export const generateMockContact = () => {
  let relations = ['EMPLY', ...faker.helpers.arrayElements(['CR', 'DE', 'CM'])]
  
  return {
    id: faker.string.uuid(),
    first_name: faker.person.firstName(),
    last_name: faker.person.lastName(),
    email: faker.internet.email().toLowerCase(),
    relations,
  };
};

export const generateMockContactDetailed = () => {
  const { id: person_id, relations } = generateMockContact();
  return relations.map((relation) => ({
    person_id,
    relation,
    organization_id: faker.number.int(),
    title: faker.person.jobTitle(),
  }));
};

export const generateMockUserInfo = (organizationId?: number) => {
  const firstName = faker.person.firstName();
  const lastName = faker.person.lastName();

  return {
    name: `${firstName} ${lastName}`,
    given_name: firstName,
    family_name: lastName,
    email: faker.internet.email().toLowerCase(),
    organizational_roles: organizationId ? {
      [organizationId]: [
        'CR',
        'EMPLY',
      ]
    } : {},
    additional_roles: organizationId ? [] : [roles.admin],
  }
};

export const generateMockElevatedUserInfo = () => {
  let user = generateMockUserInfo();
  user.additional_roles = [roles.elevated];

  return user;
}

export const generateMockAdminUserInfo = () => {
  let user = generateMockUserInfo();
  return user;
}

export const generateMockRepresentative = () => {
  return {
    id: faker.internet.userName,
    first_name: faker.person.firstName(),
    last_name: faker.person.lastName(),
    relations: faker.helpers.arrayElements(['CR', 'DE', 'CRA', 'MA']),
  }
};

export const generateMockOrganizationActivity = () => {
  const date = faker.date.past();

  return {
    login: faker.internet.userName(),
    project: faker.company.name(),
    period: `${date.getFullYear()}${String(date.getMonth() + 1).padStart(2, '0')}`, // +1 added to the month because it is zero-based 
    count: faker.number.int(),
  };
}

export const generateMockProfile = (firstName?: string, lastName?: string) => {
  firstName = firstName ?? faker.person.firstName();
  lastName = lastName ?? faker.person.lastName();

  return {
    name: faker.internet.userName(),
    picture: faker.helpers.maybe(faker.image.avatar), 
    first_name: firstName,
    last_name: lastName,
    full_name: `${firstName} ${lastName}`,
    job_title: faker.person.jobTitle(),
  };
};

export const generateMockCommitYearlyActivity = (_current: any, index: number) => {
  let date = new Date();
  // Subtract a month per every time this function runs within an Array.map.
  date.setMonth(date.getMonth() - index);

  return {
    count: faker.number.int({ min: 1, max: 300 }),
    period: `${date.getFullYear()}${String(date.getMonth() + 1).padStart(2, '0')}`, // +1 added to the month because it is zero-based 
  }
}
export const generateMockCommitterYearlyActivity = (_current: any, index: number) => {
  let date = new Date();
  // Subtract a month per every time this function runs within an Array.map.
  date.setMonth(date.getMonth() + 1 - index); // +1 added to the month because it is zero-based

  return {
    count: faker.number.int({ min: 1, max: 40 }),
    period: `${date.getFullYear()}${date.getMonth().toString().padStart(2, '0')}`, 
  }
}
