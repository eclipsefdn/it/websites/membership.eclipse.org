export const mockWorkingGroups = [
    {
      "alias": "openhw-europe",
      "title": "OpenHW Europe Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-openhw-europe.svg",
        "description": "This working group is a joint initiative of the Eclipse Foundation AISBL and its partner the OpenHW Group. The overarching goal of this working group is to engage in initiatives complementing the general work program of the OpenHW Group that are of strategic interest to the joint members of Eclipse Foundation and OpenHW Group that relate to the European marketplace, including strategic market analysis, development of technical requirements, and digital sovereignty.",
      "parent_organization": "ohwg",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/openhw-europe-charter.php",
          "website": "https://www.openhwgroup.org/working-groups/openhw-europe/",
          "members": "https://www.openhwgroup.org/working-groups/openhw-europe/#members",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wgpa/openhw-europe-working-group-participation-agreement.pdf",
              "document_id": "d292143a3fad6b5e6ebe"
          }
        }
      },
      "levels": [
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "openmdm",
      "title": "openMDM Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-openmdm.svg",
        "description": "The openMDM Working Group fosters and supports an open and innovative eco-system providing tools and systems, qualification kits and adapters for standardized and vendor independent management of measurement data in accordance with the ASAM ODS standard.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/mdmwg_charter.php",
          "website": "https://openmdm.org",
          "members": "https://openmdm.org/members/",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/openmdm-working-group-participation-agreement.pdf",
              "document_id": "776731a0c98fcf80ee81"
          }
        }
      },
      "levels": [
        {
          "relation": "WGPAE",
          "description": "Driver Member"
        },
        {
          "relation": "WGPAR",
          "description": "User Member"
        },
        {
          "relation": "WGKNA",
          "description": "Application Member"
        },
        {
          "relation": "WGJAN",
          "description": "Service Provider Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "opengenesis",
      "title": "openGENESIS Working Group",
      "status": "inactive",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-opengenesis-logo.svg",
        "description": "Before deployment onto public roads, artificial intelligence (AI) must be proven safe and roadworthy. The openGENESIS Working Group aims to foster, support and provide knowledge, methods, and tools for the assessment of AI used in autonomous driving applications.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/opengenesis_charter.php",
          "website": "https://wiki.eclipse.org/OpenGENESIS_WG",
          "members": "",
        "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/opengenesis-working-group-participation-agreement.pdf",
              "document_id": "c282723cbb8ab2e2ed9e"
          }
        }
      },
      "levels": [
        {
          "relation": "WGPAE",
          "description": "Driver Member"
        },
        {
          "relation": "WGHIL",
          "description": "Development Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "openhw-group",
      "title": "OpenHW Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg_openhw.svg",
        "description": "OpenHW Group is an independent not-for-profit, global organization where hardware and software designers collaborate in the development of open-source cores, related IP, tools and software, using industry best practices. OpenHW has released its first 32bit RISC-V compliant processor core and has several processor cores in active development.",
      "parent_organization": "ohwg",
      "resources": {
        "charter": "",
        "website": "https://www.openhwgroup.org",
          "members": "https://www.openhwgroup.org/membership/members",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": null
        }
      },
      "levels": []
    },
    {
      "alias": "eclipse-ide",
      "title": "Eclipse® IDE Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg_eclipse_ide.svg",
        "description": "The Eclipse® IDE Working Group is formed to ensure the continued sustainability, integrity, evolution and adoption of the Eclipse IDE suite of products and related technologies. In particular, it is formed to provide governance, guidance, and funding for the communities that support the delivery of the Eclipse Foundation’s flagship “Eclipse IDE” products.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/eclipse-ide-charter.php",
          "website": "https://ide-wg.eclipse.org",
          "members": "https://eclipseide.org/working-group/#members",
          "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://ide-wg.eclipse.org/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/eclipse-ide-working-group-participation-agreement.pdf",
              "document_id": "fb4d6a3ddca7cfc1de0d"
          }
        }
      },
      "levels": [
        {
          "relation": "WGPLM",
          "description": "Platinum Member"
        },
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGIDE",
          "description": "Supporter Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "orc",
      "title": "Eclipse Open Regulatory Compliance Working Group",
      "status": "proposed",
      "logo": "",
      "description": "The Eclipse Open Regulatory Compliance Working Group is being created to meet the needs of new government regulations on the software industry which will impact open source communities. This working group is intended to bring together key stakeholders from industry, small and medium enterprise (SME), research, and open source foundations to work with government in forging specifications that will enable industry to continue to leverage open source through the software supply chain to meet those regulatory requirements, and in turn will enable the open source projects to better meet industry's needs in this regard.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/workinggroups/open-regulatory-compliance-charter.php",
          "website": "https://outreach.eclipse.foundation/open-regulatory-compliance",
          "members": "",
        "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/workinggroups/wgpa/open-regulatory-compliance-working-group-participation-agreement.pdf",
              "document_id": "efcdef5b284754c43de2"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGFDN",
          "description": "Foundation Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "jakarta-ee",
      "title": "Jakarta EE Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-jakarta.svg",
        "description": "The Jakarta EE Working Group enables Java ecosystem players to collaborate on advancing enterprise Java technologies in the cloud. This initiative focuses on cultivating the business interests associated with Eclipse Enterprise for Java (EE4J) technologies.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/jakarta_ee_charter.php",
          "website": "https://jakarta.ee",
          "members": "https://jakarta.ee/membership/members/",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership/jakarta-ee",
          "participation_agreements": {
          "individual": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/jakarta-ee-individual-working-group-participation-agreement.pdf",
              "document_id": "76ef85c1acbb4fb6d348"
          },
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/jakarta-ee-working-group-participation-agreement.pdf",
              "document_id": "e3985a1aead45e62ef7b"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGDSA",
          "description": "Enterprise Member"
        },
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "dataspace",
      "title": "Eclipse Dataspace Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/eclipse-dataspace-logo.svg",
        "description": "The Eclipse Dataspace Working Group will encourage, develop, and promote open-source solutions that will enable the development, as well as the participation in dataspaces for organizations of all types and sizes. It does not favor a specific industry or type of organization and is fully dedicated to enable global adoption of dataspace technologies to foster the creation and operation of trusted data sharing ecosystems. The working group will have a strong focus on participating in standards development, implementation and onboarding of existing open-source projects and providing guidance to associated projects on alignment of supporting a broad ecosystem of interoperable dataspaces.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/dataspace-charter.php",
          "website": "https://dataspace.eclipse.org/",
          "members": "",
        "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/eclipse-dataspace-working-group-participation-agreement.pdf",
              "document_id": "1921b2dfac4cc9b00ad6"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGIDE",
          "description": "Supporter Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "cloud-development-tools",
      "title": "Eclipse Cloud Development (ECD) Tools Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-ecd-tools.svg",
        "description": "The Eclipse Cloud Development (ECD) Tools Working Group drives the evolution and broad adoption of de facto standards for cloud development tools, including language support, extensions, and developer workspace definition.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/eclipse_cloud_development_charter.php",
          "website": "https://ecdtools.eclipse.org/",
          "members": "https://ecdtools.eclipse.org/#members",
          "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership/ecdtools",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/eclipse-cloud-development-tools-participation-agreement.pdf",
              "document_id": "d96a8cabd0d04f29dfe6"
          }
        }
      },
      "levels": [
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "microprofile",
      "title": "MicroProfile® Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg_microprofile.svg",
        "description": "The MicroProfile® Working Group drives the evolution and broad adoption of technologies related to the MicroProfile Project. MicroProfile is an open forum that optimizes Enterprise Java for a microservice architecture by innovating across multiple implementations and collaborating on common areas of interest with a goal of standardization.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/microprofile-charter.php",
          "website": "https://microprofile.io/",
          "members": "https://microprofile.io/workinggroup/",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/microprofile-working-group-participation-agreement-individual.pdf",
              "document_id": "1cb0c421ab3ddd2fbaf8"
          },
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/microprofile-working-group-participation-agreement.pdf",
              "document_id": "8d438deac670301c236d"
          }
        }
      },
      "levels": [
        {
          "relation": "abcde",
          "description": "Corporate Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "osgi",
      "title": "OSGi Working Group ",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-osgi.png",
        "description": "The OSGi Working Group drives the evolution and broad adoption of software technologies derived from or related to the OSGi Specification Project which is an open source initiative to create software specifications, compatible implementations and TCKs to enable development, deployment and management of embedded, server-side, and cloud-native applications by using software modularity to vastly improve the evolution, maintainability, and interoperability of applications and infrastructure.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/osgi-charter.php",
          "website": "https://www.osgi.org/",
          "members": "https://www.osgi.org/membership/members/",
          "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/osgi-individual-participation-agreement.pdf",
              "document_id": "c7975e1d05ffd0a8ebe1"
          },
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/osgi-working-group-participation-agreement.pdf",
              "document_id": "68db2919ffe553250ae8"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "openpass",
      "title": "openPASS Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-openpass.svg",
        "description": "The openPASS Working Group promotes a collaborative and innovative ecosystem by offering tools, systems, and adapters for a standardized, openly-available and vendor-neutral platform for traffic scenario simulation.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/openpasswg_charter.php",
          "website": "https://openpass.eclipse.org/",
          "members": "https://openpass.eclipse.org/about/#members",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/openpass-working-group-participation-agreement.pdf",
              "document_id": "aea0514f5cc2d0407ffd"
          }
        }
      },
      "levels": [
        {
          "relation": "WGPAE",
          "description": "Driver Member"
        },
        {
          "relation": "WGJAN",
          "description": "Service Provider Member"
        },
        {
          "relation": "WGPAR",
          "description": "User Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "internet-things-iot",
      "title": "Eclipse IoT Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg_iot.svg",
        "description": "The Eclipse IoT Working Group enables collaboration on the development of open source implementations of IoT standards and protocols, frameworks and services used by IoT solutions, and tools for IoT developers for commercial-grade IoT.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/iotwg_charter.php",
          "website": "https://iot.eclipse.org",
          "members": "https://iot.eclipse.org/membership/members/",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership/iot",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/eclipse-iot-working-group-participation-agreement.pdf",
              "document_id": "598b0ec4df9dcbb23499"
          }
        }
      },
      "levels": [
        {
          "relation": "WGLM",
          "description": "Leader Member"
        },
        {
          "relation": "WGPAA",
          "description": "Innovator Member"
        },
        {
          "relation": "WGSM",
          "description": "Supporter Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "oniro",
      "title": "Oniro Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-oniro.png",
        "description": "The Oniro Working Group will foster an ecosystem of organizations that supports the community in the production and evolution of the Oniro operating system and platform. Oniro is a new commercially oriented, modular, and multikernel open source software platform. Its ecosystem will be developed in an environment where collaboration is promoted via the core Eclipse Foundation principles of vendor-neutrality, transparency, and openness.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/oniro-charter.php",
          "website": "https://oniroproject.org/",
          "members": "https://oniroproject.org/#initiating-members",
          "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/oniro-working-group-participation-agreement.pdf",
              "document_id": "f348c2873ca8ed8e9eca"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGFVA",
          "description": "Gold Member"
        },
        {
          "relation": "WGHLP",
          "description": "Silver Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGSM",
          "description": "Supporting Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "sparkplug",
      "title": "Sparkplug® Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-sparkplug.svg",
        "description": "The Eclipse Sparkplug® Working Group drives the evolution and broad adoption of the Eclipse Sparkplug® protocol and related technologies that enable the creation of open, collaborative, and interoperable Industrial IoT solutions, and a framework for supporting Industry 4.0.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/eclipse_sparkplug_charter.php",
          "website": "https://sparkplug.eclipse.org/",
          "members": "https://sparkplug.eclipse.org/#members",
          "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wgpa/sparkplug-working-group-individual-participation-agreement.pdf",
              "document_id": "0186c51ccfa2bc7dbbe8"
          },
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/sparkplug-working-group-participation-agreement.pdf",
              "document_id": "f351fc9ba96cb468393f"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "sdv",
      "title": "Software Defined Vehicle Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-sdv.svg",
        "description": "The Eclipse Software Defined Vehicle (SDV) Working Group will provide a forum for individuals and organizations to build and promote open source software, specifications, and open collaboration models needed to create a scalable, modular, extensible, industry-ready open source licensed vehicle software platform to support in-vehicle and around the vehicle applications development and deployment.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/sdv-charter.php",
          "website": "https://sdv.eclipse.org/",
          "members": "https://sdv.eclipse.org/index.html#members",
          "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wgpa/sdv-working-group-participation-agreement.pdf",
              "document_id": "fbb9be0bc283dc584b23"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGSM",
          "description": "Supporting Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "open-vsx",
      "title": "Open VSX Working Group",
      "status": "active",
      "logo": "https://eclipse.org/org/workinggroups/assets/images/wg-open-vsx.png",
        "description": "The Open VSX Working Group aims to ensure the continued sustainability, integrity, evolution and adoption of the Open VSX Registry. In particular, it is formed to provide governance, guidance, and funding for the communities that support the implementation, deployment, maintenance and adoption of the Eclipse Foundation’s Open VSX Registry at open-vsx.org.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/openvsx-charter.php",
          "website": "",
        "members": "https://open-vsx.org/members",
          "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wgpa/openvsx-working-group-participation-agreement.pdf",
              "document_id": "b557cbcb3973aac1beae"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSM",
          "description": "Supporter Member"
        },
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "asciidoc",
      "title": "AsciiDoc® Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg_asciidoc.svg",
        "description": "The AsciiDoc® Working Group drives the standardization, adoption, and evolution of AsciiDoc. This group encourages and shapes the open, collaborative development of the AsciiDoc language and its processors.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/asciidoc-charter.php",
          "website": "https://asciidoc-wg.eclipse.org",
          "members": "https://asciidoc-wg.eclipse.org/membership/members",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/asciidoc-individual-participation-agreement.pdf",
              "document_id": "9cc0aa80da4741eefc4c"
          },
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wpga/asciidoc-working-group-participation-agreement.pdf",
              "document_id": "df911e0adcad1a1c51df"
          }
        }
      },
      "levels": [
        {
          "relation": "WGPAA",
          "description": "Partner Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "openhw-asia",
      "title": "OpenHW Asia Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-openhw-asia.svg",
        "description": "This working group is a joint initiative of the Eclipse Foundation AISBL and its partner the OpenHW Group. The overarching goal of this working group is to address Asia-focused initiatives complementing the work program of the OpenHW Group. Those initiatives may address strategic or market analysis, technical requirements, and digital sovereignty.",
      "parent_organization": "ohwg",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/openhw-asia-charter.php",
          "website": "https://www.openhwgroup.org/working-groups/openhw-asia/",
          "members": "https://www.openhwgroup.org/working-groups/openhw-asia/#members",
          "sponsorship": "",
        "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wgpa/openhw-asia-working-group-participation-agreement.pdf",
              "document_id": "a3f1857c324e91b7f05f"
          }
        }
      },
      "levels": [
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    },
    {
      "alias": "adoptium",
      "title": "Adoptium Working Group",
      "status": "active",
      "logo": "https://www.eclipse.org/org/workinggroups/assets/images/wg-adoptium.svg",
        "description": "The Adoptium Working Group promotes and supports high-quality runtimes and associated technology for use across the Java ecosystem. Our vision is to meet the needs of Eclipse and the broader Java community by providing runtimes for Java-based applications. We embrace existing standards and a wide variety of hardware and cloud platforms.",
      "parent_organization": "eclipse",
      "resources": {
        "charter": "https://www.eclipse.org/org/workinggroups/adoptium-charter.php",
          "website": "https://adoptium.net",
          "members": "https://adoptium.net/members.html",
          "sponsorship": "https://www.eclipse.org/org/workinggroups/sponsorship/working-group-sponsorship-agreement.pdf",
          "contact_form": "https://accounts.eclipse.org/contact/membership",
          "participation_agreements": {
          "individual": null,
          "organization": {
            "pdf": "https://www.eclipse.org/org/workinggroups/wgpa/adoptium-working-group-participation-agreement.pdf",
              "document_id": "32c5a745df264aa2a809"
          }
        }
      },
      "levels": [
        {
          "relation": "WGSD",
          "description": "Strategic Member"
        },
        {
          "relation": "WGDSA",
          "description": "Enterprise Member"
        },
        {
          "relation": "WGAPS",
          "description": "Participant Member"
        },
        {
          "relation": "WGFHA",
          "description": "Committer Member"
        },
        {
          "relation": "WGSAP",
          "description": "Guest Member"
        }
      ]
    }
  ]

