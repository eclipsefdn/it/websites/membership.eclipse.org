import { faker } from '@faker-js/faker';

export const generateMockProject = () => {
  return {
    project_id: faker.string.nanoid(6),
    name: faker.company.name(),
    level: faker.number.int(3),
    parent_project_id: faker.string.nanoid(6),
    description: faker.lorem.lines({ min: 0, max: 5 }),
    url_download: faker.internet.url(),
    url_index: faker.internet.url(),
    date_active: faker.date.past(),
    sort_order: faker.number.int(1),
    active: faker.datatype.boolean(0.8),
    bugs_name: faker.lorem.lines(1),
    project_phase: faker.helpers.arrayElement(['Incubation', 'Regular', 'Archived']),
    disk_quota_gb: faker.number.float(1),
    component: faker.datatype.boolean(),
    standard: faker.datatype.boolean(),
  };
};

export const generateMockCommitter = () => {
  return {
    id: faker.string.nanoid(6),
    first_name: faker.person.firstName(),
    last_name: faker.person.lastName(),
    email: faker.internet.email(),
    relations: [],
  };
};

export const generateMockContributor = () => {
  return {
    id: faker.string.nanoid(6),
    first_name: faker.person.firstName(),
    last_name: faker.person.lastName(),
    email: faker.internet.email(),
    relations: [],
  };
};


export const generateMockProjectSlim = () => {
  return {
    project_id: faker.word.noun().toLowerCase(),
    name: 'Eclipse ' + faker.word.noun(),
    description: faker.lorem.paragraph(),
    active: faker.datatype.boolean(),
  }
};

