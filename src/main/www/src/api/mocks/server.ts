/*!
 * Copyright (c) 2025 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// This is used to setup the mock server for tests. This is not the service
// worker that is used to mock API endpoints in dev. See ./worker.ts if that is
// what you are looking for.

import { setupServer } from 'msw/node';
import handlers from './handlers';

// Setup a service worker for dev.
export const server = setupServer(...handlers);


