/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { APIResponse, HttpError, APIResponseError, transformSnakeToCamel } from './shared';
import { Project } from '../types';
const apiBasePath = 'https://projects.eclipse.org/api';

interface GetRandomProjectsOptions {
  count?: number;
};

/**
  * Retrieves random projects.
  *
  * @deprecated
  */
export const getRandomProjects = async ({ count = 1 }: GetRandomProjectsOptions): Promise<APIResponse<Project[]>> => {
  try {
    const url = new URL(apiBasePath + '/projects?order_by=random');
    if (count) {
      url.searchParams.set('pagesize', count.toString());
    }

    const response = await fetch(url.href);
    if (!response.ok) throw new HttpError(response.status, response.statusText);
    
    const data = await response.json();
    const projects = data.map(projectMapper) as Project[];

    return [projects, null];
  } catch (error) {
    return [null, error as APIResponseError];
  }
};

const projectMapper = (data: any): Project => {
  return transformSnakeToCamel(data) as Project;
}
