/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { transformSnakeToCamel } from '../shared';
import { WorkingGroupApplication } from '../../types';

const workingGroupApplicationMapper = (data: any): WorkingGroupApplication => {
  let transformedData = transformSnakeToCamel(data) as any;
  
  return {
    id: transformedData.id,
    user: transformedData.user,
    organizationId: transformedData.orgId,
    contacts: transformedData.contacts?.map((contact: any) => ({
      user: contact.user,
      firstName: contact.firstName,
      lastName: contact.lastName,
      jobTitle: contact.jobTitle,
      email: contact.mail,
      isSigningAuthority: contact.isSigningAuthority,
      workingGroup: {
        alias: contact.workingGroupAlias,
        level: contact.workingGroupLevel,
      },
    })) || [],
    created: new Date(transformedData.created),
    updated: new Date(transformedData.updated),
    state: transformedData.state,
  };
}

/** 
  * Maps the working group application model to an object shape accepted by the
  * API (before stringified).
  * @param payload - the working group application
  */
const workingGroupApplicationRequestBodyMapper = (payload: WorkingGroupApplication) => {
  return {
    id: payload.id,
    user: payload.user || null,
    org_id: payload.organizationId,
    state: payload.state,
    contacts: payload.contacts.map((c) => {
      return {
        user: null,
        first_name: c.firstName,
        last_name: c.lastName,
        working_group_alias: c.workingGroup?.alias || null,
        working_group_level: c.workingGroup?.level || null,
        mail: c.email,
        is_signing_authority: Boolean(c.isSigningAuthority),
        job_title: c.jobTitle,
      };
    }),
    created: payload.created.toISOString(),
    updated: payload.updated.toISOString(),
  }
};

export default {
  workingGroupApplicationMapper,
  workingGroupApplicationRequestBodyMapper,
}
