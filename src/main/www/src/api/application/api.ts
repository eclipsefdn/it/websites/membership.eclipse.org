import { WorkingGroupApplication } from '../../types';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import mappers from './mappers';

export const applicationAPI = createApi({
  reducerPath: 'application-api',
  baseQuery: fetchBaseQuery({ 
    baseUrl: '/application_api',
    prepareHeaders: async (headers) => {
      try {
        const response = await fetch('/api/csrf');
        const csrfToken = response.headers.get('x-csrf-token');
        
        if (!csrfToken) {
          throw new Error('Could not retrieve CSRF token while preparing headers.');
        }

        headers.set('x-csrf-token', csrfToken);
      } catch (error) {
        console.error(error);
      }
    },
  }),
  tagTypes: ['WorkingGroupApplication'],
  endpoints: (builder) => ({
    getWorkingGroupApplication: builder.query<WorkingGroupApplication, number>({
      query: (applicationId) => `working_groups/application/${applicationId}`,
      transformResponse: (response: any) => mappers.workingGroupApplicationMapper(response),
      providesTags: (_result, _error, applicationId) => [{ type: 'WorkingGroupApplication', applicationId }],
    }),
    createWorkingGroupApplication: builder.mutation<WorkingGroupApplication, WorkingGroupApplication>({
      query: (application) => ({
        url: 'working_groups/application',
        method: 'POST',
        body: mappers.workingGroupApplicationRequestBodyMapper(application),
      }),
      transformResponse: (response: any) => mappers.workingGroupApplicationMapper(response),
      invalidatesTags: (_result, _error, application) => [{ type: 'WorkingGroupApplication', application }]
    }),
    updateWorkingGroupApplication: builder.mutation<WorkingGroupApplication, WorkingGroupApplication>({
      query: (application) => ({
        url: `working_groups/application/${application.id}`, 
        method: 'PUT',
        body: mappers.workingGroupApplicationRequestBodyMapper(application),
      }),
      transformResponse: (response: any) => mappers.workingGroupApplicationMapper(response),
      invalidatesTags: (_result, _error, application) => [{ type: 'WorkingGroupApplication', application }],
    }),
    submitWorkingGroupApplication: builder.mutation<void, number>({
      query: (applicationId) => ({
        url: `working_groups/application/${applicationId}/complete`,
        method: 'POST',
      }),
      invalidatesTags: (_result, _error, application) => [{ type: 'WorkingGroupApplication', application }],
    }),
  }),
});

export const { 
  useLazyGetWorkingGroupApplicationQuery,
  useCreateWorkingGroupApplicationMutation,
  useUpdateWorkingGroupApplicationMutation,
  useSubmitWorkingGroupApplicationMutation,
} = applicationAPI;

