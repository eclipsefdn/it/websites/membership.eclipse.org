/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Avatar, Typography, createStyles, makeStyles, Theme } from '@material-ui/core';
import { useGetUserInfoQuery } from '../api/membership';
import { useLazyGetProfileQuery } from '../api/account';
import AccountMenu from './AccountMenu';
import { Skeleton } from '@material-ui/lab';

const ProfileSnippet: React.FC = () => {
  const { data: userInfo } = useGetUserInfoQuery();
  const [fetchProfile, { data: profile, isLoading: isLoadingProfile }] = useLazyGetProfileQuery();
  const classes = useStyles({ username: profile?.username });

  if (userInfo && !profile && !isLoadingProfile) {
    fetchProfile(userInfo.username);
  }

  if (!profile) {
    return <Skeleton variant="circle"></Skeleton>
  }

  const avatarRender = profile?.picture 
    ? <Avatar className={classes.avatar} src={profile.picture} alt="" />
    : <Avatar className={classes.avatar}><span style={{ mixBlendMode: 'overlay' }}>{getInitials(profile.firstName, profile.lastName)}</span></Avatar>

  return (
    <>
      <Typography className={classes.username}>
        {profile.firstName} {profile.lastName}
      </Typography>

      <AccountMenu />
      {avatarRender}
    </>
  );
};

export default ProfileSnippet;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    userInfoCtn: {
      display: 'flex',
      top: theme.spacing(1),
      right: theme.spacing(1),
      width: '100%',
      height: 40,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    username: {
      color: '#000',
      marginBottom: 0,
      marginRight: theme.spacing(1),
    },
    dropDownBtn: {
      minWidth: 40,
      height: 30,
      padding: 0,
    },
    dropDownIcon: {
      color: '#A4AFB7',
    },
    dropDownItemIcon: {
      minWidth: 30,
    },
    avatar: {
      display: 'none',
      width: 45,
      height: 45,
      fontSize: '2rem',
      marginLeft: theme.spacing(1),
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
      background: (props: { username?: string }) => {
        if (!props.username) return 'transparent';

        const seed = stringToSeed(props.username);
        const randomIndex = Math.floor(seedRandom(seed) * avatarColors.length);

        return `linear-gradient(${avatarColors[randomIndex]}, ${avatarColors[randomIndex]}66)`;
      },
    },
    anchorTag: {
      textDecoration: 'none',
      color: 'inherit',
      '&:hover ': {
        textDecoration: 'none',
        color: 'inherit',
      },
    },
  })
);

const getInitials = (firstName: string, lastName: string): string => {
  const firstInitial = firstName.substring(0, 1).toUpperCase();
  const lastInitial = lastName.substring(0, 1).toUpperCase();

  return firstInitial + lastInitial;
};

const avatarColors = [
  '#f5911d',
  '#045b82',
  '#045b82',
]

const seedRandom = (seed: number) => {
    let n = Math.sin(seed++) * 10000;
    return n - Math.floor(n);
}

const stringToSeed = (str: string) => Array
  .from(str)
  .reduce((seed, char) => char.charCodeAt(0) + ((seed << 5) - seed), 0);
