/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { 
  Card, 
  CardContent, 
  Box, 
  Link, 
  List, 
  ListItem, 
  Typography, 
  IconButton, 
  CircularProgress, 
  Theme, 
  createStyles, 
  makeStyles 
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Delete as DeleteIcon } from '@material-ui/icons';
import { useRemoveOrganizationProductMutation, useGetOrganizationProductsQuery } from '../api/membership'

interface EditOrganizationLinksProps {
  organizationId: number;
};

const EditOrganizationLinks: React.FC<EditOrganizationLinksProps> = ({ organizationId }) => {
  const { data: products, isLoading, error } = useGetOrganizationProductsQuery(organizationId);

  let render: JSX.Element | null = null;

  if (isLoading) {
    render = <CircularProgress color="primary" />
  } else if (error) {
    render = <Alert severity="error">Could not load your organisation's linked products</Alert>
  } else if (products?.length === 0) {
    render = <Typography>Your organisation has no linked products.</Typography>
  } else {
    render = (
      <>
        {
          products!.map(product => (
            <ListItem key={product.id} disableGutters>
              <OrganizationLink 
                organizationId={organizationId}
                productId={product.id} 
                name={product.name} 
                url={product.url} 
              />
            </ListItem>
          ))
        }
      </>
    )
  }

  return (
    <>
      <Typography variant="h6" component="h2">
        Current Links
      </Typography>
      <List>
        {render}
      </List>
    </>
  );
};

export default EditOrganizationLinks;

interface OrganizationLinkProps {
  organizationId: number;
  productId: number;
  name: string;
  url: string;
}

const OrganizationLink: React.FC<OrganizationLinkProps> = ({ organizationId, productId, name, url }) => {
  const classes = useStyles();
  const [removeOrganizationProduct] = useRemoveOrganizationProductMutation();

  const handleDeleteClick = async () => {
    await removeOrganizationProduct({ organizationId, productId });
  };

  return (
    <Card style={{width: '100%'}}>
      <CardContent>
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Box>
            <Typography component="h3" variant="h6">
              {name}
            </Typography>
            <Link href={url}>{url}</Link>
          </Box>
          <IconButton className={classes.icon} onClick={handleDeleteClick}>
            <DeleteIcon fontSize="large" />
          </IconButton>
        </Box>
      </CardContent>
    </Card>
  );
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    icon: {
      '&:hover': {
        color: theme.palette.error.main,
        transition: 'color 0.2s ease-in-out',
      }
    }
  })
);
