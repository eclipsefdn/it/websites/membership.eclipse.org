/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useMemo, useContext } from 'react';
import { WorkingGroup } from '../../types';
import { useForm, useAppSelector } from '../../hooks';
import { WorkingGroupStepValues, SigningAuthorityStepValues } from './shared';
import { MultiStepFormContext } from '@components/MultiStepForm';
import { useGetOrganizationWorkingGroupsQuery, useGetOrganizationContactsQuery, useGetOrganizationDetailedContactQuery } from '../../api/membership';
import { useGetWorkingGroupsQuery } from '../../api/working-groups';

type JoinWorkingGroupFormValues = WorkingGroupStepValues & SigningAuthorityStepValues;

const useJoinWorkingGroupForm = () => {
  const formHelpers = useForm<JoinWorkingGroupFormValues>();
  const { organizationId } = useAppSelector(state => state.organization);
  const { allFormValues } = useContext(MultiStepFormContext);

  const getMemberRepresentativeId = (): string | undefined => {
    if (isLoadingContacts || contactsError) {
      return;
    }

    const memberRepresentative = contacts?.filter(contact => contact.relations.find(relation => relation === 'CR')).at(0);

    return memberRepresentative?.id;
  }

  const { 
    data: allWorkingGroups, 
    error: allWorkingGroupError, 
  } = useGetWorkingGroupsQuery();
  let { 
    data: joinedWorkingGroups, 
    error: joinedWorkingGroupsError,
    isLoading: isLoadingJoinedWorkingGroups 
  } = useGetOrganizationWorkingGroupsQuery(organizationId!); 

  const { 
    data: contacts, 
    error: contactsError, 
    isLoading: isLoadingContacts
  } = useGetOrganizationContactsQuery(organizationId!);
  const { 
    data: memberRepresentative, 
  } = useGetOrganizationDetailedContactQuery({ organizationId: organizationId!, contactId: getMemberRepresentativeId() as string}, { skip: !contacts || !organizationId });

  const workingGroups: WorkingGroup[] = useMemo(() => {
    if (allWorkingGroupError) {
      formHelpers.setStatus({ severity: 'error', message: 'Could not retrieve working groups. Please try again later.' });
    }
    if (!allWorkingGroups || isLoadingJoinedWorkingGroups) return [];

    // If there was a problem loading an organization's joined working groups,
    // set it to an empty array.
    // We do not want to show an error since it shouldn't prevent a user from
    // continuing with the form.
    if (joinedWorkingGroupsError) {
      joinedWorkingGroups = [];
    }

    // Filter out working groups that the organization has already joined.
    // This function is memoized to prevent this filter logic from running
    // every re-render.
    const joinedWorkingGroupsSet = new Set(joinedWorkingGroups!.map((workingGroup) => workingGroup.alias));
    return allWorkingGroups!.filter((workingGroup: any) => !joinedWorkingGroupsSet.has(workingGroup.alias))
  }, [JSON.stringify(allWorkingGroups), JSON.stringify(joinedWorkingGroups), isLoadingJoinedWorkingGroups, allWorkingGroupError]);

  return { 
    ...formHelpers, 
    allFormValues, 
    workingGroups: workingGroups ?? [],
    memberRepresentative 
  };
};

export default useJoinWorkingGroupForm;
