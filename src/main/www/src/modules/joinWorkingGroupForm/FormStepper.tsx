/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Grid, useTheme } from '@material-ui/core';
import { BusinessCenter as BusinessCenterIcon, Edit as EditIcon, Description as DescriptionIcon } from '@material-ui/icons';
import { SectionLink } from '@components';

const FormStepper = () => {
  const theme = useTheme();

  return (
    <Grid container spacing={2} style={{ maxWidth: theme.breakpoints.width('lg') }}>
      <Grid item xs={12} sm={6} lg={4}>
        <SectionLink icon={<BusinessCenterIcon />}>
          Step 1 - Working Group
        </SectionLink>
      </Grid>
      <Grid item xs={12} sm={6} lg={4}>
        <SectionLink color={theme.palette.info.main} icon={<EditIcon />}>
          Step 2 - Signing Authority
        </SectionLink>
      </Grid>
      <Grid item xs={12} sm={6} lg={4}>
        <SectionLink color={theme.palette.secondary.main} icon={<DescriptionIcon />}>
          Step 3 - Review 
        </SectionLink>
      </Grid>
    </Grid>
    );
};

export default FormStepper;
