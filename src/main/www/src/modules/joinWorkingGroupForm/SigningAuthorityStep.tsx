/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Spacer, TextField, FormStep } from '@components';
import useJoinWorkingGroupForm from './useJoinWorkingGroupForm';
import { Checkbox, Grid, FormControlLabel, Typography } from '@material-ui/core';
import { FormStepBaseProps } from '../../types';
import { shouldDisableContactField } from './shared';

/** 
  * The signing authority form step for the Working Group Application Form.
  */
const SigningAuthorityStep: React.FC<FormStepBaseProps> = () => {
  const { formValues, memberRepresentative, setFieldValue, handleFieldBlur, handleFieldChange } = useJoinWorkingGroupForm();
  if (!formValues.signingAuthority) return null;

  const handleSameContact = (event: React.ChangeEvent<HTMLInputElement>) => {
    // If member representative is loaded and the checkbox is checked, set all
    // the signing authority fields to automatically fill with member rep info.
    if (event.target.checked && memberRepresentative) {
      setFieldValue(`signingAuthority.firstName`, memberRepresentative.firstName);
      setFieldValue(`signingAuthority.lastName`, memberRepresentative.lastName);
      setFieldValue(`signingAuthority.jobTitle`, '');
      setFieldValue(`signingAuthority.email`, memberRepresentative.email);
    }

    setFieldValue(`signingAuthority.sameContact`, event.target.checked);
  };

  return (
    <FormStep>
      <Typography variant="h4" component="h2">
        Signing Authority
      </Typography>
      <Spacer y={1} />
      <Typography>
        Please indicate who has the signing authority for the agreement.
      </Typography>
      <Spacer y={1} />
      <FormControlLabel label="Same as Member Representative" control={<Checkbox value={formValues.signingAuthority.sameContact} checked={formValues.signingAuthority.sameContact} onChange={handleSameContact} />} />
      <Grid container spacing={1}>
        <Grid item sm={6}>
          <TextField 
            name="signingAuthority.firstName" 
            label="First Name"
            value={formValues.signingAuthority.firstName}
            margin="dense" 
            placeholder="First Name" 
            onBlur={handleFieldBlur}
            onChange={handleFieldChange}
            disabled={shouldDisableContactField('firstName', formValues.signingAuthority, memberRepresentative)}
            fullWidth
          />
        </Grid>
        <Grid item sm={6}>
          <TextField 
            name="signingAuthority.lastName" 
            label="Last Name"
            value={formValues.signingAuthority.lastName} 
            margin="dense" 
            placeholder="Last Name" 
            onBlur={handleFieldBlur}
            onChange={handleFieldChange}
            disabled={shouldDisableContactField('lastName', formValues.signingAuthority, memberRepresentative)}
            fullWidth
          />
        </Grid>
        <Grid item sm={6}>
          <TextField 
            name="signingAuthority.jobTitle" 
            label="Job Title"
            value={formValues.signingAuthority.jobTitle}
            margin="dense" 
            placeholder="Job Title" 
            onBlur={handleFieldBlur}
            onChange={handleFieldChange}
            disabled={shouldDisableContactField('jobTitle', formValues.signingAuthority, memberRepresentative)} 
            fullWidth
          />
        </Grid>
        <Grid item sm={6}>
          <TextField 
            name="signingAuthority.email" 
            label="Email"
            value={formValues.signingAuthority.email} 
            margin="dense" 
            placeholder="Email Address" 
            onBlur={handleFieldBlur}
            onChange={handleFieldChange}
            disabled={shouldDisableContactField('email', formValues.signingAuthority, memberRepresentative)}
            fullWidth
          />
        </Grid>
      </Grid>
      <Spacer y={2} />
    </FormStep>
  );
};

export default SigningAuthorityStep;
