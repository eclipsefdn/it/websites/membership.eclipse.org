/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { WorkingGroup, DeepPartial, OrganizationContactDetailed } from '../../types';

export interface FormEntry {
  _id: number;
  workingGroup: WorkingGroup;
  participationLevel: string;
  contact: {
    sameContact: boolean;
    firstName: string;
    lastName: string;
    email: string;
    jobTitle: string;
  }
}

export interface WorkingGroupStepValues {
  entries: FormEntry[];
}

export const createFormEntry  = (): DeepPartial<FormEntry> => {
  return {
    _id: Date.now(),
    participationLevel: '',
    contact: {
      sameContact: false,
      firstName: '',
      lastName: '',
      email: '',
      jobTitle: '',
    }
  }
}

export interface SigningAuthorityStepValues {
  signingAuthority: {
    sameContact: boolean;
    firstName: string;
    lastName: string;
    email: string;
    jobTitle: string;
  }
}


export interface JoinWorkingGroupValues {
  0: WorkingGroupStepValues;
  1: SigningAuthorityStepValues;
  2: {};
}

/**
  * Returns true or false depending on whether or not an input field needs to
  * be disabled.
  */
export const shouldDisableContactField = (
  fieldName: string, 
  contactValues: any, 
  memberRepresentative:any, 
): boolean => {
  // Disable field if member representative exists, and field value is not
  // empty, and field value is the same as member rep.
  return Boolean(memberRepresentative)
    && contactValues.sameContact
    && contactValues[fieldName as keyof typeof contactValues] !== ''
    && contactValues[fieldName as keyof typeof contactValues] === memberRepresentative![fieldName as keyof typeof memberRepresentative]; 
};
