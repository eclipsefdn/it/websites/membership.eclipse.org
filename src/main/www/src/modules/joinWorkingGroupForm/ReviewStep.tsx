/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { FormStep, Spacer } from '@components';
import useJoinWorkingGroupForm from './useJoinWorkingGroupForm';
import { Typography, Paper, Table, TableContainer, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { WorkingGroup } from '../../types';

/** The review step for the Working Group Application Form. This component
  * displays a table of all the form data to be submitted.
  */
const ReviewStep: React.FC = () => {
  // Since the review step concerns itself with the form data of all previous
  // steps, we must retrieve all form values.
  const { allFormValues } = useJoinWorkingGroupForm();

  // The working group entries.
  const entries = allFormValues[0].entries;
  // The form values pertaining to signing authority.
  const signingAuthority = allFormValues[1].signingAuthority;

  return (
    <FormStep>
      <Typography variant="h4" component="h2">
        Review and Submit Your Completed Application
      </Typography>
      <Spacer y={1} />
      <Typography>
        Please review your completed <em>Working Group Application Form</em>. If you
        would like to update any information, please click the back
        button.
      </Typography>
      <Spacer y={1} />
      <Typography>
        Please click <strong>submit</strong> when ready.
      </Typography>
      <Spacer y={2} />

      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Working Group</TableCell>
              <TableCell>Participation Level</TableCell>
              <TableCell>Member Representative</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {entries.map((entry: any) => (
              <WorkingGroupReviewEntry 
                key={entry._id} 
                workingGroup={entry.workingGroup.title} 
                participationLevel={entry.participationLevel} 
                firstName={entry.contact.firstName} 
                lastName={entry.contact.lastName}
                email={entry.contact.email} 
              />
            ))}
          </TableBody>
          <TableHead>
            <TableRow>
              <TableCell colSpan={3}>Signing Authority</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableCell colSpan={3}>
              {signingAuthority.firstName} {signingAuthority.lastName} ({signingAuthority.email})
            </TableCell>
          </TableBody>
        </Table>
      </TableContainer>
      <Spacer y={2} />
    </FormStep>
  );
};

export default ReviewStep;

interface WorkingGroupEntriesProps {
  workingGroup: string;
  firstName: string;
  lastName: string;
  email: string;
  participationLevel: string;
}

const WorkingGroupReviewEntry: React.FC<WorkingGroupEntriesProps> = ({ workingGroup, participationLevel, firstName, lastName, email }) => {
  const { workingGroups } = useJoinWorkingGroupForm();

  // Loading
  if (!workingGroups) return null;

  // Get the selected working group so we can retrieve the participation level
  // description from the level code (e.g. SD).
  const selectedWorkingGroup = workingGroups!.find((wg) => wg.title === workingGroup) as WorkingGroup;
  const levelDescription = selectedWorkingGroup.levels.find((level) => level.relation === participationLevel)?.description;

  return (
    <>
      <TableRow>
        <TableCell>{workingGroup}</TableCell>
        <TableCell>{levelDescription}</TableCell>
        <TableCell>{firstName} {lastName} ({email})</TableCell>
      </TableRow>
    </>
  );
}
