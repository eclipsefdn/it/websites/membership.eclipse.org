/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState } from 'react';
import { 
  Box, 
  Dialog, 
  TextField as MUITextField, 
  DialogTitle, DialogContent, 
  DialogActions, 
  Grid, 
  FormHelperText, 
  Button, 
  Divider, 
  Link as ExternalLink, 
  FormControl, 
  FormControlLabel, 
  Checkbox, 
  MenuItem, 
  Select, 
  Typography 
} from '@material-ui/core';
import { TextField, FormStep, Spacer } from '@components';
import { Autocomplete, AutocompleteChangeReason } from '@material-ui/lab';
import useJoinWorkingGroupForm from './useJoinWorkingGroupForm';
import { FieldArray, FieldArrayRenderProps, FormikErrors} from 'formik';
import { shouldDisableContactField, createFormEntry, FormEntry } from './shared';
import { WorkingGroup, FormStepBaseProps } from '../../types';

const WorkingGroupStep: React.FC<FormStepBaseProps> = () => {
  const { formValues } = useJoinWorkingGroupForm();

  const exceededMaximumFieldsets = formValues.entries?.length >= 10;
  
  let arrayHelpersBinding: FieldArrayRenderProps;

  const handleAddWorkingGroup = () => {
    if (exceededMaximumFieldsets || !arrayHelpersBinding) return;
    arrayHelpersBinding.push(createFormEntry());
  };

  return (
    <FormStep>
      <Typography variant="h5">Working Group</Typography>
      <Typography>
        Please complete the following details for joining a Working Group.
      </Typography>
      <Spacer y={2} />

      <FieldArray 
        name="entries"
        render={(arrayHelpers) => {
          arrayHelpersBinding = arrayHelpers;
          if (!formValues.entries) return null; // temp
          return formValues.entries.map((entry: FormEntry, index: number) => (
            <WorkingGroupFieldset 
              key={entry._id} 
              index={index}
              arrayHelpers={arrayHelpers} 
            />)
          );
        }}
      />

      <Grid container justify="center" spacing={2} style={{maxWidth: '30rem', margin: 'auto'}}>
        <Grid item xs={12}> 
          <Button 
            variant="contained" 
            color="secondary" 
            size="large" 
            disabled={exceededMaximumFieldsets} 
            onClick={handleAddWorkingGroup}
            fullWidth
          >
            Add Another Working Group
          </Button>
        </Grid>
      </Grid>
    </FormStep>
  );
};

export default WorkingGroupStep;

interface WorkingGroupFieldsetProps {
  index: number;
  arrayHelpers: FieldArrayRenderProps;
}

const WorkingGroupFieldset: React.FC<WorkingGroupFieldsetProps> = ({ index, arrayHelpers }) => {
  const { formValues, errors, setFieldValue, handleFieldChange, handleFieldBlur, workingGroups, memberRepresentative } = useJoinWorkingGroupForm();
  const [confirmationDialogOpen, setConfirmationDialogOpen] = useState(false);

  const selectedWorkingGroup = workingGroups?.find((workingGroup) => workingGroup.alias === formValues.entries[index].workingGroup?.alias);

  const handleSameContact = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked && memberRepresentative) {
      setFieldValue(`entries[${index}].contact.firstName`, memberRepresentative.firstName);
      setFieldValue(`entries[${index}].contact.lastName`, memberRepresentative.lastName);
      setFieldValue(`entries[${index}].contact.jobTitle`, memberRepresentative.jobTitle || '');
      setFieldValue(`entries[${index}].contact.email`, memberRepresentative.email);
    }

    setFieldValue(`entries[${index}].contact.sameContact`, event.target.checked);
  };

  const handleRemove = () => {
    setConfirmationDialogOpen(true);
  };

  const handleCancel = () => {
    setConfirmationDialogOpen(false); 
  };

  const handleConfirm = () => {
    setConfirmationDialogOpen(false);
    arrayHelpers.remove(index);
  };

  const handleWorkingGroupChange = (_: React.ChangeEvent<{}>, value: WorkingGroup | null, __: AutocompleteChangeReason) => {
    setFieldValue(`entries[${index}].workingGroup`, value);
  };

  return (
    <>
      <Dialog
        open={confirmationDialogOpen}
        onClose={handleCancel}
      >
        <DialogTitle>Confirm "Remove Working Group Entry"</DialogTitle>
        <DialogContent>
          Are you sure you want to remove this working group entry? This action
          cannot be undone.
        </DialogContent>
        <DialogActions>
          <Button variant="text" color="secondary" onClick={handleConfirm} autoFocus>Remove Working Group</Button>
          <Button variant="text" color="primary" onClick={handleCancel} autoFocus>Cancel</Button>
        </DialogActions>
      </Dialog>
      {/* Working Group Selection */}
      <Typography variant="h6">Which working group would you like to join?</Typography>
        { workingGroups && workingGroups.length
          ? (
            <Autocomplete
              value={formValues.entries[index].workingGroup}
              options={workingGroups}
              getOptionLabel={(option) => option.title}
              getOptionSelected={(option, value) => option.alias === value.alias}
              onChange={handleWorkingGroupChange}
              fullWidth
              renderInput={(params) => (
                <MUITextField 
                  {...params} 
                  variant="outlined"
                  placeholder="Working Groups"
                />
              )}
            />
          ) : null 
      }
      <Spacer y={2} />

      {selectedWorkingGroup ? 
        <>
          {/* Participation Level */}
          <Typography variant="h6" id="select-level-label">What is your intended participation level?</Typography>
          <FormControl 
            variant="outlined" 
            margin="dense" 
            fullWidth
          >
            <Select 
              name={`entries[${index}].participationLevel`} 
              value={formValues.entries[index].participationLevel}
              onChange={handleFieldChange}
              aria-labelledby="select-level-label"
            >
              {selectedWorkingGroup?.levels
                ? selectedWorkingGroup?.levels.map((level) => <MenuItem key={level.relation} value={level.relation}>{level.description}</MenuItem>)
                : ''
              }
            </Select>
            <FormHelperText>{errors.entries ? (errors.entries[index] as FormikErrors<FormEntry>)?.participationLevel : undefined}</FormHelperText>
          </FormControl>
          <Spacer y={2} />
          <Typography>
            Each Working Group has different participation levels and restrictions
            on who can join at those levels (e.g., Guest Member level is typically
            restricted to non-profit organizations). See the charter for full
            details on which choice is best for you, and to see the working group
            fees associated with joining this working group.
          </Typography>
          <Spacer y={2} />
          <Typography>
            Note: Working Group fees are in addition to the membership fees
            associated with joining the Eclipse Foundation. Please{' '}
            <ExternalLink href="mailto:membership.coordination@eclipse-foundation.org">contact our membership team</ExternalLink>{' '}
            with any questions.
          </Typography>
          <Spacer y={2} />
          
          {/* Representatives */}
          <Typography variant="h6">
            Who is to serve as your organisation's Member Representative with the
            Working Group?
          </Typography>
          <FormControlLabel 
            label="Same as Member Representative"
            control={(
              <Checkbox 
                value={formValues.entries[index].contact.sameContact}
                checked={formValues.entries[index].contact.sameContact}
                onChange={handleSameContact}
              />
            )}
          />
          <Grid container alignItems="stretch" spacing={2}>
            <Grid item md={6}>
              <TextField 
                name={`entries[${index}].contact.firstName`}
                label="First Name" 
                variant="outlined" 
                margin="dense" 
                value={formValues.entries[index].contact.firstName} 
                onBlur={handleFieldBlur}
                onChange={handleFieldChange}
                disabled={shouldDisableContactField('firstName', formValues.entries[index].contact, memberRepresentative)}
                fullWidth 
              />
            </Grid>
            <Grid item md={6}>
              <TextField 
                name={`entries[${index}].contact.lastName`}
                label="Last Name" 
                variant="outlined" 
                margin="dense" 
                value={formValues.entries[index].contact.lastName} 
                onBlur={handleFieldBlur}
                onChange={handleFieldChange}
                disabled={shouldDisableContactField('lastName', formValues.entries[index].contact, memberRepresentative)}
                fullWidth 
              />
            </Grid>
            <Grid item md={6}>
              <TextField 
                name={`entries[${index}].contact.jobTitle`}
                label="Job Title" 
                variant="outlined" 
                margin="dense" 
                value={formValues.entries[index].contact.jobTitle}
                onBlur={handleFieldBlur}
                onChange={handleFieldChange}
                disabled={shouldDisableContactField('jobTitle', formValues.entries[index].contact, memberRepresentative)}
                fullWidth 
              />
            </Grid>
            <Grid item md={6}>
              <TextField 
                name={`entries[${index}].contact.email`}
                type="email" 
                label="Email Address" 
                variant="outlined" 
                margin="dense" 
                value={formValues.entries[index].contact.email}
                onBlur={handleFieldBlur}
                onChange={handleFieldChange}
                disabled={shouldDisableContactField('email', formValues.entries[index].contact, memberRepresentative)}
                fullWidth 
              />
            </Grid>
          </Grid>
        </>
      : null}
      <Box marginY={2} textAlign="center">
        <Button variant="contained" color="secondary" size="large" disabled={formValues.entries.length <= 1} onClick={handleRemove}>Remove This Group</Button>
      </Box>
      <Spacer y={4} />
      <Divider />
      <Spacer y={4} />
    </>
  );
};

