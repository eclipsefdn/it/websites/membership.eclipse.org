/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Box } from '@material-ui/core';
import { AutocompleteChangeReason } from '@material-ui/lab/Autocomplete';
import { DisplayProps, SizingProps } from '@material-ui/system';
import OrganizationAutocomplete from '@components/OrganizationAutocomplete';
import { useAppDispatch, useAppSelector } from '../hooks';
import { useGetOrganizationQuery } from '../api/membership';
import { disablePreviewMode, enablePreviewMode } from '../features/organization-slice';
import { useHistory } from 'react-router-dom';

interface AutocompleteOption {
  value: number;
  label: string;
}

interface Props extends SizingProps, DisplayProps {
  size?: 'small' | 'medium';
}

const PreviewAutocomplete: React.FC<Props> = ({ size, ...props }) => {
  const history = useHistory();
  const { organizationId } = useAppSelector(state => state.organization);
  const dispatch = useAppDispatch();
  const { data: previewedOrganization } = useGetOrganizationQuery(organizationId!);

  const handleAutocompleteChange = (event: React.ChangeEvent<{}>, value: AutocompleteOption | null, _: AutocompleteChangeReason) => {
    // Manually clearing the field will trigger a 'change' event. Clearing the
    // field with the clear button will trigger a 'click' event.
    if (event.type === 'change' && value === null) {
      // Don't do anything if the user manually cleared the field.
      return;
    } else if (value === null) {
      // Assume the clear button was clicked if value is null and event is not
      // 'change'.
      dispatch(disablePreviewMode());
      history.push('/portal/standby');
      return;
    };
    
    // We retrieve the organization id from the value property.
    dispatch(enablePreviewMode(value.value));
    history.push('/portal/dashboard');
  };

  // The selectedOrganization is the organization that is currently being
  // previewed. If no organization is being previewed, then the Autocomplete
  // field will be empty.
  const selectedOrganization = previewedOrganization 
    ? { value: previewedOrganization.organizationId, label: previewedOrganization.name } 
    : null; 

  return (
    <Box {...props}>
      <OrganizationAutocomplete 
        label="Select a member to preview"
        size={size}
        value={selectedOrganization} 
        onChange={handleAutocompleteChange} 
      />
    </Box>
  );
};

export default PreviewAutocomplete;

