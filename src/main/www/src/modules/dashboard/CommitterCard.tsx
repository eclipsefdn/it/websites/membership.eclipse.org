/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import { useAppSelector } from '../../hooks';
import { 
  brightBlue, 
} from '../../Constants/Constants';
import { pickRandomItems } from '../../utils/portalFunctionHelpers';
import { useHistory } from 'react-router';
import { useGetOrganizationCommittersQuery } from '../../api/membership';
import IconCard from '@components/IconCard';
import { Button, CardActions, CircularProgress, Typography, List, ListItem } from '@material-ui/core';

/**
  * A component that displays a card with a list of 5 random committers. 
  */
export const CommitterCard = () => {
  const { isFetching, committers, viewAll } = useCommitterCard();

  let contents;
  if (isFetching) {
    contents = <CircularProgress />
  } else {
    contents = committers.map(committer => (
      <ListItem button component="a" href={committer.url}>
        <Typography>{committer.name}</Typography>
      </ListItem>
    ))
  }

  return (
      <IconCard accentColor={brightBlue} icon={<PeopleAltIcon/>}>
        <IconCard.Content>
          <Typography variant="subtitle2">Your Committers</Typography>
          <List>
            {contents}
          </List>
        </IconCard.Content>
        <IconCard.Actions>
          <Button onClick={viewAll} style={{ marginLeft: 'auto' }}>View All</Button>
        </IconCard.Actions>
      </IconCard>
  );
};

export default CommitterCard;

const useCommitterCard = () => {
  const history = useHistory();
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: committers, isLoading} = useGetOrganizationCommittersQuery(organizationId!);

  /**
    * Navigate to the contact management page with the contributor filter
    * applied. This will show all contributors.
    */
  const viewAll = () => {
    history.push('/portal/contact-management?filter=committer');
  }

  const fiveRandomCommitters = committers && committers?.length > 0
    ? pickRandomItems(committers, 5).map((committer) => ({ 
        name: `${committer.firstName} ${committer.lastName}`, 
        url: committer.id ? 'https://accounts.eclipse.org/users/' + committer.id : '',
      }))
    : [
        {
          name: 'No committers yet',
          url: '',
        },
      ];
    
  return { isFetching: isLoading , committers: fiveRandomCommitters, viewAll };
};
