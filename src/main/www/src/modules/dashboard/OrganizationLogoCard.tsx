/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Box, Card, CardMedia, CircularProgress, Typography, Theme, createStyles, makeStyles } from '@material-ui/core';
import { useAppSelector } from '../../hooks';
import { useGetOrganizationQuery } from '../../api/membership';

const OrganizationLogoCard: React.FC = () => {
  const classes = useStyles();
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: organization, error, isLoading } = useGetOrganizationQuery(organizationId!);
  
  let content;
  if (isLoading) {
    content = <CircularProgress />;
  } else if (error) {
    content = <Typography>Could not fetch logo</Typography>;
  } else if (organization?.logos.web) {
    content = <CardMedia component="img" image={organization!.logos.web as string} alt="" />;
  } else {
    content = <Typography>No logo set</Typography>;
  }

  return (
    <Card className={classes.root}>
      <Box 
        display="flex" 
        alignItems="center" 
        justifyContent="center" 
        maxWidth="25rem" 
        height="100%" 
        marginX="auto" 
        textAlign="center"
      >
        {content}
      </Box>
    </Card>
  );
}

export default OrganizationLogoCard;

const useStyles = makeStyles((theme: Theme) => 
  createStyles({
    root: {
      width: '100%',
      height: '100%',
      minHeight: '32rem',
    },
  }));
