/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useState } from 'react';
import { Card, CardContent, CardActions, Button } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import RepresentativeList from './representativeCard/RepresentativeList';
import RepresentativeDialog from './representativeCard/RepresentativeDialog';

const RepresentativeCard: React.FC = () => {
  const classes = useStyles();

  const [dialogOpen, setDialogOpen] = useState(false);

  const handleDialogOpen = () => {
    setDialogOpen(true);
  }

  const handleDialogClose = () => {
    setDialogOpen(false);
  }

  return (
    <>
      <Card className={classes.companyRepCard}>
        <CardContent>
          <RepresentativeList max={3} />
        </CardContent>
        <CardActions>
          <Button 
            style={{ marginLeft: 'auto' }} 
            onClick={handleDialogOpen}
          >
            View All
          </Button>
        </CardActions>
      </Card>
      <RepresentativeDialog open={dialogOpen} onClose={handleDialogClose} />
    </>
  );
};

export default RepresentativeCard;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100%',
    },
    companyRepCard: {
      position: 'relative',
      height: '100%',
      backgroundColor: '#D0D0D0',
    },
    viewAllBtnCtn: {
      position: 'absolute',
      bottom: 15,
      left: 15,
      right: 15,
      width: '100%',
      display: 'flex',
      justifyContent: 'end',
      alignItems: 'end',
    },
    viewAllBtn: {
      padding: theme.spacing(0.5, 1.5),
    },
  })
);
