/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { Box, CircularProgress, createStyles, makeStyles } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import {
  brightBlue,
  CONFIG_FOR_BAR_LINE_CHART,
} from '../../Constants/Constants';
import { Bar } from 'react-chartjs-2';
import { useAppSelector } from '../../hooks';
import { 
  useGetOrganizationCommitYearlyActivityQuery, 
  useGetOrganizationCommitterYearlyActivityQuery 
} from '../../api/membership';

const useStyles = makeStyles(() =>
  createStyles({
    fullWidthChartCtn: {
      width: '100%',
      height: '100%',
    },
    loadingCtn: {
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
);

/**
  * The chart to display commit and committer activity for an organization.
  * If one of the pieces of data fails to fetch, the component will try its
  * best to continue rendering the chart with the data it does have.
  */
function DashboardCommittersAndContributorsChart() {
  const classes = useStyles();
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: commitData, error: commitError, isLoading: isLoadingCommitData } = useGetOrganizationCommitYearlyActivityQuery(organizationId!);
  const { data: committerData, error: committerError, isLoading: isLoadingCommitterData } = useGetOrganizationCommitterYearlyActivityQuery(organizationId!);

  if (isLoadingCommitterData || isLoadingCommitData ) {
    return <CircularProgress />;
  }

  // If both data pieces have failed to fetch, render an overall error for the component.
  if (committerError && commitError) {
    return ( 
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        Could not load committer and contributor activity chart. Please try
        again at a later time.
      </Alert>
    );
  }

  // If the commit activity could be fetched, sort it.
  let sortedCommitActivity;
  if (!commitError) {
    sortedCommitActivity = Array.from(commitData!.activity).sort((a: any, b: any) => {
      const sortableDateStringA = `${a.period.year}${a.period.month}`;
      const sortableDateStringB = `${b.period.year}${b.period.month}`;
      return sortableDateStringA > sortableDateStringB ? 1 : -1;
    });
  }
  
  // If the committer activity could be fetched, sort it.
  let sortedCommitterActivity;
  if (!committerError) {
    sortedCommitterActivity = Array.from(committerData!.activity).sort((a: any, b: any) => {
      const sortableDateStringA = `${a.period.year}${a.period.month}`;
      const sortableDateStringB = `${b.period.year}${b.period.month}`;
      return sortableDateStringA > sortableDateStringB ? 1 : -1;
    });
  }

  // We know at least one of the pieces of data will be truthy, because we do
  // an early return above when both are falsy.
  const months = createMonthArray(sortedCommitActivity ?? sortedCommitterActivity);

  let datasets = [];
  
  // If we have committer activity, add it to the dataset.
  if (sortedCommitterActivity) {
    datasets.push({
      type: 'line',
      label: 'Committers',
      borderColor: brightBlue,
      borderWidth: 2,
      fill: false,
      data: sortedCommitterActivity.map((committer: any) => committer.count),
    });
  }

  // If we have commit activity, add it to the dataset.
  if (sortedCommitActivity) {
    datasets.push({
      type: 'bar',
      label: 'Commits',
      backgroundColor: '#F0F2F8',
      data: sortedCommitActivity.map((commits: any) => commits.count),
    });
  }

  const chartData: any = {
    labels: months,
    datasets,
  };

  return (
    <Box className={classes.fullWidthChartCtn}>
      <Bar data={chartData} options={CONFIG_FOR_BAR_LINE_CHART} />
      { !sortedCommitActivity ? <Alert severity="error">Could not load commit activity.</Alert> : null }
      { !sortedCommitterActivity ? <Alert severity="error">Could not load committer activity.</Alert> : null }
    </Box>
  );
}

export default DashboardCommittersAndContributorsChart;

export const createMonthArray = (data: any) => {
  return data.map((item: any) => {
    return `${item.period.year}-${item.period.month}`
  });
};
