/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import IconCard from '@components/IconCard'; 
import { useGetOrganizationProjectsQuery } from '../../api/membership';
import { useAppSelector } from '../../hooks';
import { ProjectSlim } from '../../types';
import { Link } from 'react-router-dom';
import { Alert, Skeleton } from '@material-ui/lab';
import { Box, Button, CardActions, List, ListItem, ListItemText, Typography } from '@material-ui/core';

export const YourProjectsCard: React.FC = () => {
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: projects, error, isLoading } = useGetOrganizationProjectsQuery(organizationId!);

  let contents;

  if (isLoading) {
    contents = <Skeleton animation="wave" />;
  } else if (error) {
    contents = <Alert severity="error">Could not fetch projects.</Alert>;
  } else {
    contents = <ProjectList projects={projects!} />;
  }

  return (
    <IconCard icon={<BusinessCenterIcon/>} style={{height: '100%'}}>
      <IconCard.Content>
        <Typography variant="subtitle2">Your Projects</Typography>
        {contents}
      </IconCard.Content>
      {projects && projects.length >= 1 
        ? (
          <IconCard.Actions>
            <Link to="/portal/your-projects">
              <Button>View All</Button>
            </Link>
          </IconCard.Actions>
        ) : null }
    </IconCard>
  );
};

export default YourProjectsCard;

interface ProjectListProps {
  projects: ProjectSlim[];
}

const ProjectList: React.FC<ProjectListProps> = ({ projects }) => {
  if (projects.length <= 0) {
    return <Box marginY="2rem">
      <Typography>Your organisation is not associated with any projects.</Typography>
    </Box>;
  }

  return (
    <List>
      {projects.slice(0, 4).map((project) => (
        <ListItem key={project.name} component="a" href={'https://projects.eclipse.org/projects/' + project.id} button>
          <ListItemText>{project.name}</ListItemText>
        </ListItem>
      ))}
    </List>
  );
};

