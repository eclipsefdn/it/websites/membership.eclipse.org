/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Avatar, Card, CardContent, List, ListItem, Typography, Theme, createStyles, makeStyles } from '@material-ui/core';
import { Note as NoteIcon, Group as GroupIcon, Mail as MailIcon, Assignment as AssignmentIcon } from '@material-ui/icons';

const ImportantLinksCard: React.FC = () => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <List>
          <ListItem
            className={classes.listItem}
            component="a"
            href="https://www.eclipse.org/community/newsletter/"
            target="_blank"
            rel="noopener"
            button
            divider
          >
            <Avatar className={classes.icon}>
              <NoteIcon />
            </Avatar>
            <Typography className={classes.text} variant="body1" component="p">
              Member Newsletter
            </Typography>
          </ListItem>
          <ListItem
            className={classes.listItem}
            component="a"
            href="https://www.eclipse.org/membership/exploreMembership.php"
            target="_blank"
            rel="noopener"
            button
            divider
          >
            <Avatar className={classes.icon}>
              <GroupIcon />
            </Avatar>
            <Typography className={classes.text} variant="body1" component="p">
              Explore our Members
            </Typography>
          </ListItem>
          <ListItem
            className={classes.listItem}
            component="a"
            href="https://www.eclipse.org/org/foundation/contact.php"
            target="_blank"
            rel="noopener"
            button
            divider
          >
            <Avatar className={classes.icon}>
              <MailIcon />
            </Avatar>
            <Typography className={classes.text} variant="body1" component="p">
              Contact Us
            </Typography>
          </ListItem>
          <ListItem
            className={classes.listItem}
            component="a"
            href="https://www.eclipse.org/org/foundation/contact.php"
            target="_blank"
            rel="noopener"
            button
            divider
          >
            <Avatar className={classes.icon}>
              <AssignmentIcon />
            </Avatar>
            <Typography className={classes.text} variant="body1" component="p">
              Governance Document
            </Typography>
          </ListItem>
        </List>
      </CardContent>
    </Card>
  );
}

export default ImportantLinksCard;

const useStyles = makeStyles((theme: Theme) => 
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      height: '100%',
      padding: '2rem 3rem',
    },
    icon: {
      width: '3.5rem',
      height: '3.5rem',
      margin: theme.spacing(0, 2),
    },
    text: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 0,
      fontSize: 18,
    },
    listItem: {
      padding: '1.5rem 0',
    }
  }));
