/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Link } from 'react-router-dom';
import { Button, CardActions, List, ListItem, ListItemText, Typography } from '@material-ui/core';
import { Alert, Skeleton } from '@material-ui/lab';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import IconCard from '@components/IconCard'; 
import { WorkingGroup } from '../../types';
import { selectRandom } from '../../utils';
import { useGetWorkingGroupsQuery } from '../../api/working-groups';


const InterestedWorkingGroupsCard: React.FC = () => {
  const { data: workingGroups, error, isLoading } = useGetWorkingGroupsQuery();

  const randomWorkingGroups = workingGroups && selectRandom(workingGroups, 4) as WorkingGroup[];

  let contents;

  if (isLoading) {
    contents = <Skeleton animation="wave" />
  } else if (randomWorkingGroups) {
    contents = <WorkingGroupList workingGroups={randomWorkingGroups} />;
  } else if (error) {
    contents = <Alert severity="error">{error}</Alert>;
  }

  return (
    <IconCard icon={<BusinessCenterIcon/>} style={{height: '100%'}}>
      <IconCard.Content>
        <Typography variant="subtitle2">Working Groups You Might Be Interested In</Typography>
        {contents}
      </IconCard.Content>
      <IconCard.Actions>
        <Link to="/portal/join-working-group">
          <Button>Join a Working Group</Button>
        </Link>
      </IconCard.Actions>
    </IconCard>
  );
}

export default InterestedWorkingGroupsCard;

interface WorkingGroupListProps {
  workingGroups: WorkingGroup[];
}

const WorkingGroupList: React.FC<WorkingGroupListProps> = ({ workingGroups }) => {
  return (
    <List>
      {workingGroups.map((workingGroup) => (
        <ListItem key={workingGroup.alias} component="a" href={workingGroup.resources.website} button>
          <ListItemText>{workingGroup.title}</ListItemText>
        </ListItem>
      ))}
    </List>
  );
};


