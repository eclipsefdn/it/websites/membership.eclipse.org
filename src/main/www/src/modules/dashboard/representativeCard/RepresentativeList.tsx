import { Box, List, ListItem, ListItemText, createStyles, makeStyles, Theme } from '@material-ui/core';
import { Alert, Skeleton } from '@material-ui/lab';
import { useGetOrganizationRepresentativesQuery } from '../../../api/membership';
import { useAppSelector } from '../../../hooks';
import { OrganizationRepresentative } from '../../../types';
import Spacer from '@components/Spacer';
import { getRelationNameFromCode } from '../../../api/shared';

interface RepresentativeListProps {
  max?: number;
}

const RepresentativeList: React.FC<RepresentativeListProps> = ({ max }) => {
  const classes = useStyles();
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: representatives, error, isLoading } = useGetOrganizationRepresentativesQuery(organizationId!);

  let render: JSX.Element;

  if (error) {
    render = (
      <Alert severity="error">
        An error occurred while loading your organization's representatives.
        Please try again later.
      </Alert>
    );
  } else if (isLoading) {
    render = <RepresentativeListSkeleton />;
  } else {
    render = <>
      {representatives!
        .slice(0, max || representatives!.length)
        .map((representative: OrganizationRepresentative) => (
          <ListItem>
            <ListItemText
              classes={{
                primary: classes.repPrimary,
                secondary: classes.repSecondary,
              }}
              primary={`${representative.firstName} ${representative.lastName}`}
              secondary={representative.relations.map(getRelationNameFromCode).join(', ')}
            />
          </ListItem>
        ))}
      </>
  }
  
  return (
    <List>
      {render} 
    </List>
  );
};

export default RepresentativeList;

const RepresentativeListSkeleton: React.FC = () => {
  return (
    <>
      { Array.from({ length: 3 }).map(() => (
        <>
          <Box justifyContent="center" marginX="auto" width="30%"><Skeleton variant="rect" width="100%" height={25} /></Box>
          <Spacer y={1} />
          <Skeleton variant="rect" width="100%" height={25} />
          <Spacer y={2} />
        </>
      ))}
    </>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    repPrimary: {
      fontSize: 18,
      textAlign: 'center',
    },
    repSecondary: {
      fontSize: 16,
      textAlign: 'center',
    },
  })
);
