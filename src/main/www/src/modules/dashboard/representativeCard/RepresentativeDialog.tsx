import { Button, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import RepresentativeList from './RepresentativeList';

interface RepresentativeDialogProps {
  open: boolean;
  onClose?: () => void;
}

const RepresentativeDialog: React.FC<RepresentativeDialogProps> = ({ open, onClose: handleClose }) => {
  return (
    <Dialog open={open}>
      <DialogTitle>
        Key Representatives
      </DialogTitle>
      <DialogContent>
        <RepresentativeList />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
};

export default RepresentativeDialog;
