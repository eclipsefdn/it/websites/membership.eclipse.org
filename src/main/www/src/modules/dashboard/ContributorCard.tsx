/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CircularProgress, Typography, List, ListItem, CardActions, Button } from '@material-ui/core';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import { 
  brightBlue, 
} from '../../Constants/Constants';
import { pickRandomItems } from '../../utils/portalFunctionHelpers';
import { useHistory } from 'react-router';
import { useGetOrganizationContributorsQuery } from '../../api/membership';
import { useAppSelector } from '../../hooks';
import IconCard from '@components/IconCard';

/** 
  * A component that displays a card with a list of 5 random committers.
  */
export const ContributorCard = () => {
  const { isFetching, contributors, viewAll } = useContributorCard();

  let contents;
  if (isFetching) {
    contents = <CircularProgress />
  } else {
    contents = contributors.map(contributor => (
      <ListItem button component="a" href={contributor.url}>
        <Typography>{contributor.name}</Typography>
      </ListItem>
    ))
  }

  return (
    <IconCard accentColor={brightBlue} icon={<PeopleAltIcon/>}>
      <IconCard.Content>
        <Typography variant="subtitle2">Your Contributors</Typography>
        <List>
          {contents}
        </List>
      </IconCard.Content>
      <IconCard.Actions>
        <Button onClick={viewAll} style={{ marginLeft: 'auto' }}>View All</Button>
      </IconCard.Actions>
    </IconCard>
  );
};

export default ContributorCard;

const useContributorCard = () => {
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: contributors, isLoading } = useGetOrganizationContributorsQuery(organizationId!);
  const history = useHistory();
  
  /**
    * Navigate to the contact management page with the contributor filter
    * applied. This will show all contributors.
    */
  const viewAll = () => {
    history.push('/portal/contact-management?filter=contributor');
  }

  const fiveRandomContributors = contributors && contributors?.length > 0
    ? pickRandomItems(contributors, 5).map((contributor) => ({ 
        name: `${contributor.firstName} ${contributor.lastName}`, 
        url: contributor.id ? 'https://accounts.eclipse.org/users/' + contributor.id : '',
      }))
    : [
        {
          name: 'No committers yet',
          url: '',
        },
      ];

  return { isFetching: isLoading, contributors: fiveRandomContributors, viewAll };
};

