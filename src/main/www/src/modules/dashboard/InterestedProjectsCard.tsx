/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useEffect, useState } from 'react';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import IconCard from '@components/IconCard'; 
import { Project } from '../../types';
import { getRandomProjects } from '../../api/projects';
import { Link } from 'react-router-dom';
import { Alert, Skeleton } from '@material-ui/lab';
import { Button, CardActions, List, ListItem, ListItemText, Typography } from '@material-ui/core';

// @todo: Refactor this component to not use useEffect.

export const YourProjectsCard: React.FC = () => {
  const [projects, setProjects] = useState<Project[] | null>(null);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadProjects = async () => {
      const [data, error] = await getRandomProjects({ count: 4 }); 
      
      if (error) {
        setError('Could not display projects. Please try again later.');
      } else if (data) {
        setProjects(data.slice(0, 4));
      }
    };

    if (!projects) {
      loadProjects();
    }
  }, [projects]);

  let contents = <Skeleton animation="wave" />;
  
  if (projects) {
    contents = <ProjectList projects={projects} />;
  } else if (error) {
    contents = <Alert severity="error">{error}</Alert>;
  }

  return (
    <IconCard icon={<BusinessCenterIcon/>}>
      <IconCard.Content>
        <Typography variant="subtitle2">Projects You May Be Interested In</Typography>
        {contents}
      </IconCard.Content>
    </IconCard>
  );
};

export default YourProjectsCard;

interface ProjectListProps {
  projects: Project[];
}

const ProjectList: React.FC<ProjectListProps> = ({ projects }) => {
  return (
    <List>
      {projects.map((project) => (
        <ListItem key={project.name} component="a" href={'https://projects.eclipse.org/projects/' + project.projectId} button>
          <ListItemText>{project.name}</ListItemText>
        </ListItem>
      ))}
    </List>
  );
};


