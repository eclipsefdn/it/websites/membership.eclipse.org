/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { 
  Box, 
  Button, 
  List, 
  ListItem, 
  ListItemText, 
  Typography, 
  Dialog, 
  DialogTitle, 
  DialogContent, 
  DialogActions 
} from '@material-ui/core';
import { Alert, Skeleton } from '@material-ui/lab';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import IconCard from '@components/IconCard'; 
import { WorkingGroup } from '../../types';
import { useAppSelector } from '../../hooks';
import { useGetOrganizationWorkingGroupsQuery } from '../../api/membership';

const YourWorkingGroupsCard: React.FC = () => {
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: workingGroups, error, isLoading } = useGetOrganizationWorkingGroupsQuery(organizationId!); 
  const [dialogOpen, setDialogOpen] = useState(false);
  const maxWorkingGroupDisplay = 4;
  
  let contents;
  if (isLoading) {
    contents = <Skeleton animation="wave" />;
  }
  if (workingGroups && !isLoading) {
    contents = <WorkingGroupList workingGroups={workingGroups} max={maxWorkingGroupDisplay} />;
  } else if (error) {
    contents = <Alert severity="error">An error occurred while fetching working groups.</Alert>;
  }

  const handleViewAllClick = () => {
    setDialogOpen(true);
  }
  
  const handleDialogClose = () => {
    setDialogOpen(false);
  }

  return (
    <>
      <IconCard icon={<BusinessCenterIcon/>}>
        <IconCard.Content>
          <Typography variant="subtitle2">Your Working Groups</Typography>
          {contents}
        </IconCard.Content>
        <IconCard.Actions>
          { workingGroups && workingGroups.length > maxWorkingGroupDisplay 
            ? (
              <Button onClick={handleViewAllClick}>View All</Button>
            ) : null }
        </IconCard.Actions>
      </IconCard>
      <ViewAllWorkingGroupsDialog
        workingGroups={workingGroups!}
        open={dialogOpen}
        onClose={handleDialogClose}
      />
    </>
  );
}

export default YourWorkingGroupsCard;

interface WorkingGroupListProps {
  workingGroups: WorkingGroup[];
  max?: number;
}

const WorkingGroupList: React.FC<WorkingGroupListProps> = ({ workingGroups, max }) => {
  if (workingGroups.length <= 0) {
    return <Box marginY="2rem">
      <Typography>Your organisation does not yet belong to a working group.</Typography>
      <Link style={{display: 'block', marginTop: '2rem'}} to="https://www.eclipse.org/org/workinggroups/">
        Learn more about Working Groups
      </Link>
    </Box>;
  }

  return (
    <List>
      {
        workingGroups
          .slice(0, max ?? workingGroups.length)
          .map((workingGroup) => (
            <ListItem key={workingGroup.alias} component="a" href={workingGroup.resources.website} button>
              <ListItemText>{workingGroup.title}</ListItemText>
            </ListItem>
          ))
      }
    </List>
  );
};

interface ViewAllWorkingGroupsDialogProps {
  open?: boolean;
  workingGroups: WorkingGroup[];
  onClose?: (...args: any) => any;
}

const ViewAllWorkingGroupsDialog = ({ workingGroups, open = false, onClose: handleClose }: ViewAllWorkingGroupsDialogProps) => {
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>
        View All Your Working Groups
      </DialogTitle>
      <DialogContent>
        <WorkingGroupList workingGroups={workingGroups} />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
}
