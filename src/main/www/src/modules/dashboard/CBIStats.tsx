/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState } from 'react';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import { Grid, Popover, Link, Typography, Theme, useTheme, makeStyles, createStyles } from '@material-ui/core';
import { Alert, AlertTitle, Skeleton } from '@material-ui/lab';
import { HelpButton, IconCard } from '@components';
import { useAppSelector } from '../../hooks';
import { useGetOrganizationCBIQuery } from '../../api/membership';

/**
  * The CBI stats component renders an icon card with stats on an organizations
  * CBI data. 
  */
const CBIStats: React.FC = () => {
  const theme = useTheme();
  const classes = useStyles();
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: cbiData, error, isLoading } = useGetOrganizationCBIQuery(organizationId!);

  // Depending on various states of our data fetching, we will render different contents.
  // e.g. We will render error messages, skeletons, or the data itself.
  let innerContent: JSX.Element;
  if (isLoading) {
    innerContent = <CBISkeleton />
  } else if (error) {
    innerContent = (
      <Grid item xs={12}>
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          Could not load CBI stats. Please try again later.
        </Alert>
      </Grid>
    );
  } else if (cbiData) {
    innerContent = (
      <>
        <Grid container item xs={8}>
            <Grid className={classes.statLabelGroup} item xs={12}>
              <CBIAllocationStat 
                name="resource packs" 
                inUse={cbiData.resourcePacks.inUse} 
                allocated={cbiData.resourcePacks.allocated} 
              />
            </Grid>
            <Grid className={classes.statLabelGroup} item xs={12}>
              <CBIAllocationStat 
                name="dedicated agents" 
                inUse={cbiData.dedicatedAgents.inUse} 
                allocated={cbiData.dedicatedAgents.allocated} 
              />
            </Grid>
            <Grid className={classes.statLabelGroup} item xs={12}>
              <CBIAllocationStat 
                name="GitHub large runners" 
                inUse={cbiData.runners.githubLargeRunners.inUse} 
                allocated={cbiData.runners.githubLargeRunners.allocated} 
              />
            </Grid>
        </Grid>
      </>
    );
  } else {
    innerContent = <Grid item xs={4}><Typography>No CBI data.</Typography></Grid>
  }

  return (
    <IconCard className={classes.root} icon={<PeopleAltIcon/>} color={theme.palette.primary.main} accentColor={theme.palette.info.main}>
      <IconCard.Content>
        <CBIStatsPopover />
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            <Typography component="h3" variant="h5" style={{fontWeight: 500}}>Common Build Infrastructure</Typography>
            <Typography variant="subtitle2" style={{color: theme.palette.neutral['800']}}>Additional Resources</Typography>
          </Grid>
          {innerContent}
        </Grid>
      </IconCard.Content>
    </IconCard>
  );
};

export default CBIStats;

const useStyles = makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      position: 'relative',
    },
    statNumber: {
      minWidth: '2ch',
      color: theme.palette.neutral['50'],
      fontSize: '2rem',
      textAlign: 'right',
    },
    statLabel: {
      color: theme.palette.neutral['700'],
    },
    statLabelGroup: {
      display: 'flex',
      alignItems: 'baseline',
      gap: theme.spacing(1),
    },
    helpButton: {
      position: 'absolute',
      top: 5,
      right: 10,
    },
  });
});

interface CBIAllocationStatProps {
  name: string;
  inUse: number;
  allocated: number;
}

/** Renders a line of text in the form of "1 of 2 resource packs are in use". */
const CBIAllocationStat: React.FC<CBIAllocationStatProps> = ({ inUse, allocated, name }) => {
  const classes = useStyles();

  return (
    <>
      <Typography className={classes.statNumber}>
        {inUse}
      </Typography>
      <Typography variant="subtitle2" className={classes.statLabel}>of</Typography>
      <Typography className={classes.statNumber}>
        {allocated}
      </Typography>
      <Typography variant="subtitle2" className={classes.statLabel}>{name} in use</Typography>
    </>
  );
};

/** 
  * The popover that contains the help text for the CBI stats card.
  */
const CBIStatsPopover: React.FC = () => {
  const classes = useStyles();
  const theme = useTheme();

  // We will store the help button into our state. We only store this when the
  // popover should be open.
  const [anchorElement, setAnchorElement] = useState<HTMLButtonElement | null>(null);

  const handleHelpButtonClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    setAnchorElement(event.currentTarget);
  };

  const handleClose: React.MouseEventHandler = () => {
    setAnchorElement(null);
  };
  
  // Cast anchorElement to a boolean. If it is truthy, it means the popover
  // should be open. 
  const open = Boolean(anchorElement);
  const id = open ? 'help-cbi' : undefined;

  return (
    <>
      <HelpButton className={classes.helpButton} onClick={handleHelpButtonClick} aria-describedby={id} />
      <Popover 
        open={open} 
        anchorEl={anchorElement}
        anchorOrigin={{horizontal: 'center', vertical: -65}} 
        transformOrigin={{horizontal: 'center', vertical: 'top'}}
        elevation={1} 
        onClose={handleClose} 
        PaperProps={{
          style: {
            width: '100%',
            maxWidth: '60rem',
            padding: theme.spacing(2),
          }
        }}
      >
        <Typography>
          The Eclipse Foundation offers a Common Build environment for all its
          Open Source projects. Member Organizations have access to additional{' '}
          <Link href="https://github.com/eclipse-cbi/cbi/wiki#additional-resources">
            build resources based on their membership level
          </Link>{' '} 
          like Resource Packs, Dedicated Agents and GitHub hosted large
          runners. For more information about build resources, please refer to
          the <Link href="https://github.com/eclipse-cbi/cbi/wiki">CBI Wiki</Link>.
        </Typography>
      </Popover>
    </>
  );
};


/**
  * A loading skeleton for the CBI stats card. 
  *
  * It renders three lines of text skeletons which should correspond to the
  * three lines of stats we have in the main component.
  */
const CBISkeleton: React.FC = () => {
  const theme = useTheme();
  return (
    <Grid item xs={6}>
      { Array.from({ length: 3 }).map((_element, index) => (
        <Skeleton 
          key={index}
          width="100%" 
          style={{ 
            backgroundColor: theme.palette.primary.light, 
            fontSize: '2rem' 
          }}
        />
      ))}
    </Grid>
  );
}
