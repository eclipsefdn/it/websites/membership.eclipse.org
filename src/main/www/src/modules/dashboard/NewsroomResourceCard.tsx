/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { CircularProgress, Typography, List, ListItem, CardActions, Button, useTheme } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Description as DescriptionIcon } from '@material-ui/icons';
import { NewsroomResourceType } from '../../types';
import { IconCard } from '@components';
import { useGetNewsroomResourcesQuery } from '../../api/newsroom';

interface NewsroomResourceCardProps {
  title: string;
  type: NewsroomResourceType;
}

const NewsroomResourceCard: React.FC<NewsroomResourceCardProps> = ({ title, type }) => {
  const theme = useTheme();
  const { data: resources, error, isLoading } = useGetNewsroomResourcesQuery(type);

  let contents;
  if (isLoading) {
    contents = <CircularProgress />
  } else if (error) {
    contents = <Alert severity="error">Could not load newsroom resources</Alert>
  } else {
    contents = resources!
      .slice(0, 3)
      .map((resource) => (
        <ListItem button component="a" href={resource.directLink}>
          <Typography>{resource.title}</Typography>
        </ListItem>
      ))
  }

  return (
    <IconCard accentColor={theme.palette.primary.main} icon={<DescriptionIcon/>}>
      <IconCard.Content>
        <Typography variant="subtitle2">{title}</Typography>
        <List>
          {contents}
        </List>
      </IconCard.Content>
    </IconCard>
  );
}

export default NewsroomResourceCard;
