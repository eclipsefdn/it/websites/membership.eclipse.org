/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Grid, Typography, Chip, useTheme } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { useGetOrganizationQuery } from '../../api/membership';
import { formatToLongDate } from '../../utils';
import { MEMBER_LEVELS } from '../../Constants/Constants';
import { Organization } from '../../types';
import { useAppSelector } from '../../hooks';

/** 
   * Displays the organization name along with member since and renewal date
   * information.
  */
export const YourOrganizationSnippet: React.FC = () => {
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: organization, error, isLoading } = useGetOrganizationQuery(organizationId!, { 
    skip: !organizationId, 
    refetchOnMountOrArgChange: true 
  });

  if (error) return <ErrorRender />;
  if (isLoading || !organization) return <LoadingRender />;

  return (
    <Grid container spacing={1}>
      <Grid item>
        <Typography variant="h4" component="h1">
          { error ? "Error retrieving organization" : `${organization.name} - ${getLevelDescription(organization)}` }
        </Typography>
      </Grid>
      <Grid container item spacing={1}>
        <Grid item>
          <Chip variant="outlined" size="small" label={`Member Since: ${formatToLongDate(organization.memberSince)}`} />
        </Grid>
        <Grid item>
          <Chip variant="outlined" size="small" label={`Next Renewal: ${formatToLongDate(organization.renewalDate)}`} />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default YourOrganizationSnippet;

/**
  * Displays the loading skeletons for the snippet
  */
const LoadingRender: React.FC = () => {
  const theme = useTheme();

  return (
    <Grid container spacing={1}>
      <Grid item xs={5}>
        <Skeleton variant="text" animation="wave" style={{ fontSize: theme.typography.h4.fontSize }} />
      </Grid>
      <Grid container item spacing={1}>
        <Grid item xs={2}>
          <Skeleton variant="text" animation="wave" style={{ fontSize: '2rem' }} />
        </Grid>
        <Grid item xs={2}>
          <Skeleton variant="text" animation="wave" style={{ fontSize: '2rem' }} />
        </Grid>
      </Grid>
    </Grid>
  );
}

/**
  * Displays the error for the snippet.
  */
const ErrorRender: React.FC = () => {
  return (
    <Grid container spacing={1}>
      <Grid item>
        <Typography variant="h4" component="h1">
          Cannot load organization. Please try again later.
        </Typography>
      </Grid>
    </Grid>
  );
};

/**
  * Retrieves the description of an organization's highest membership level. 
  * 
  * @param organization - the organization which to retrieve the description
  * from.
  * @returns The description of the membership level.
  */
const getLevelDescription = (organization: Organization): string => {
  if (organization.levels.find((item) => item.level.toUpperCase() === MEMBER_LEVELS.strategicMember.code)) {
    return MEMBER_LEVELS.strategicMember.description;
  }

  if (organization.levels.find((item) => MEMBER_LEVELS.contributingMember.code.includes(item.level.toUpperCase()))) {
    return MEMBER_LEVELS.contributingMember.description;
  }

  if (organization.levels.find((item) => item.level.toUpperCase() === MEMBER_LEVELS.associateMember.code)) {
    return MEMBER_LEVELS.associateMember.description;
  }

  return '';
};
