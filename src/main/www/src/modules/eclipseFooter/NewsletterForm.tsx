/*!
 * Copyright (c) 2025 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';

const NewsletterForm: React.FC = () => {
  return (
    <div className="footer-end-newsletter">
      <form 
        id="mc-embedded-subscribe-form" 
        action="https://eclipse.us6.list-manage.com/subscribe/post?u=eaf9e1f06f194eadc66788a85&amp;id=e7538485cd&amp;f_id=00f9c2e1f0" 
        method="post" 
        target="_blank"
        noValidate 
      >
        <label className="footer-end-newsletter-label" htmlFor="email">Subscribe to our Newsletter</label>
        <div className="footer-end-newsletter-input-wrapper">
          <input className="footer-end-newsletter-input" type="email" id="email" name="EMAIL" autoComplete="email" placeholder="Enter your email address" />
          <button className="footer-end-newsletter-submit btn btn-link" id="mc-embedded-subscribe" type="submit" name="subscribe">
            <i className="fa fa-solid fa-envelope fa-lg" aria-hidden="true"></i>
          </button>
        </div>
      </form>
    </div>
  );
}

export default NewsletterForm;
