/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState } from 'react';
import FormStepper from './joinWorkingGroupForm/FormStepper';
import WorkingGroupStep from './joinWorkingGroupForm/WorkingGroupStep';
import SigningAuthorityStep from './joinWorkingGroupForm/SigningAuthorityStep';
import ReviewStep from './joinWorkingGroupForm/ReviewStep';
import { createFormEntry, JoinWorkingGroupValues } from './joinWorkingGroupForm/shared';
import MultiStepForm from '@components/MultiStepForm';
import Spacer from '@components/Spacer';
import { joinWorkingGroupSchema } from '../validation';
import { FormikHelpers } from 'formik';
import { 
  useGetUserInfoQuery,
} from '../api/membership';
import { 
  useLazyGetWorkingGroupApplicationQuery,
  useCreateWorkingGroupApplicationMutation,
  useUpdateWorkingGroupApplicationMutation,
  useSubmitWorkingGroupApplicationMutation,
} from '../api/application';
import { WorkingGroupApplication } from '../types';
import { useAppSelector } from '../hooks';
import { useHistory } from 'react-router-dom';

// The initial values object for the form.
const initialValues = {
  0: { entries: [createFormEntry()] },
  1: { 
    signingAuthority: {
      sameContact: false,
      firstName: '',
      lastName: '',
      jobTitle: '',
      email: '',
    }
  },
  // Review Step has no need for its own form values.
  2: {},
}

const JoinWorkingGroupFormWrapper: React.FC = () => {
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: user } = useGetUserInfoQuery();

  const [applicationId, setApplicationId] = useState<number | null>(null);
  const [getWorkingGroupApplication] = useLazyGetWorkingGroupApplicationQuery();
  const [createWorkingGroupApplication] = useCreateWorkingGroupApplicationMutation();
  const [updateWorkingGroupApplication] = useUpdateWorkingGroupApplicationMutation();
  const [submitWorkingGroupApplication] = useSubmitWorkingGroupApplicationMutation();

  const history = useHistory();

  const handleSubmit = async (_formValues: JoinWorkingGroupValues, helpers: FormikHelpers<JoinWorkingGroupValues>) => {
    helpers.setSubmitting(true);

    const { error } = await submitWorkingGroupApplication(applicationId!);

    if (error) {
      helpers.setStatus({ 
        severity: 'error', 
        message: 'An error occurred while attempting to submit your working group application', 
      });
      return;
    }
    
    history.push('/portal/join-working-group/submitted');

    helpers.setSubmitting(false);
  };
  
  // Submits between steps.
  const handleWorkingGroupStepSubmit = async (formValues: any, helpers: FormikHelpers<JoinWorkingGroupValues>) => {
    helpers.setSubmitting(true);
    let application: WorkingGroupApplication = {
      id: null,
      user: user!.username,
      created: new Date(),
      updated: new Date(),
      state: 'INPROGRESS',
      organizationId: organizationId as number,
      contacts: formValues.entries.map((entry: any) => ({
        user: null,
        firstName: entry.contact.firstName,
        lastName: entry.contact.lastName,
        email: entry.contact.email,
        jobTitle: entry.contact.jobTitle,
        // Temporarily set isSigningAuthority to false. We won't know who the
        // signing authority is until the next step.
        isSigningAuthority: false,
        workingGroup: {
          alias: entry.workingGroup.alias,
          level: entry.participationLevel,
        }
      }))
    };

    const { data: submittedApplication, error } = await createWorkingGroupApplication(application);

    if (!submittedApplication || error) {
      helpers.setStatus({ severity: 'error', message: 'An error occurred while attempting to start working group application. Please try again.' });
    };

    setApplicationId(submittedApplication!.id);

    helpers.setSubmitting(false);
  }

  const handleSigningAuthorityStepSubmit = async (formValues: any, helpers: FormikHelpers<JoinWorkingGroupValues>) => {
    helpers.setSubmitting(true);
    // Request Application
    if (!applicationId) {
      helpers.setStatus({ 
        severity: 'error', 
        message: 'An error occurred. Please ensure that you are starting the working group application process from the beginning.' 
      });
      return;
    }

    let { data, error } = await getWorkingGroupApplication(applicationId);

    if (!data || error) {
      helpers.setStatus({
        severity: 'error',
        message: 'An error occurred. No working group application was found.'
      });
      return;
    }
    
    // Creating a copy of the application state. This is because redux state is
    // not directly mutable. We want to make a copy to then make another
    // HTTP request with updated data.
    let application = structuredClone(data);

    const signingAuthority = application.contacts.find((contact: any) => contact.email === formValues.signingAuthority.email);
    // If the contact was added from the previous form step, set them as
    // signing authority. Otherwise, add a new signing authority contact.
    if (signingAuthority) {
      signingAuthority.isSigningAuthority = true;
    } else {
      application.contacts.push({
        user: null,
        firstName: formValues.signingAuthority.firstName,
        lastName: formValues.signingAuthority.lastName,
        email: formValues.signingAuthority.email,
        jobTitle: formValues.signingAuthority.jobTitle,
        isSigningAuthority: true,
      });
    }

    const { data: submittedApplication, error: updateError }= await updateWorkingGroupApplication(application);
    if (!submittedApplication || updateError) {
      helpers.setStatus({
        severity: 'error',
        message: 'An error occurred while attempting to update your working group application. Please try again.'
      });
      return;
    }

    helpers.setSubmitting(false);
  };

  return (
    <>
      <FormStepper />
      <Spacer y={6} />
      <MultiStepForm 
        validationSchema={joinWorkingGroupSchema} 
        initialValues={initialValues} 
        onSubmit={handleSubmit}
      >
        <WorkingGroupStep onSubmit={handleWorkingGroupStepSubmit} />
        <SigningAuthorityStep onSubmit={handleSigningAuthorityStepSubmit} />
        <ReviewStep />
      </MultiStepForm>
    </>
  );
}

export default JoinWorkingGroupFormWrapper;

