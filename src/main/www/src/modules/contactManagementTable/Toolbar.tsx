import { GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';

const Toolbar = () => {
  const csvOptions = {
    fileName: 'contacts_data',
  };

  return (
    <GridToolbarContainer>
      <GridToolbarExport csvOptions={csvOptions} />
    </GridToolbarContainer>
  );
};

export default Toolbar;
