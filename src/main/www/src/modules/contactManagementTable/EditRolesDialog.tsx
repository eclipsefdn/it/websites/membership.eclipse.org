import { useState, useEffect } from 'react';
import { OrganizationalRole } from '../../types';
import { 
  Button,
  Box,
  Dialog, 
  DialogTitle, 
  DialogContent, 
  DialogActions, 
  LinearProgress,
  List, 
  ListItem,
  FormControlLabel,
  Checkbox,
  useTheme,
} from '@material-ui/core';
import { Help as HelpIcon } from '@material-ui/icons';
import { editableRoles as editableRolesSet } from '../../utils'; 
import { getRelationNameFromCode } from '../../api/shared';
import { useAppSelector } from '../../hooks';
import permissions from '../../Constants/permissions' 
import RoleHelpPopover from './editRolesDialog/RoleHelpPopover';
import { checkPermission } from '../../utils/portalFunctionHelpers';
import { 
  useGetUserInfoQuery, 
  useAddRelationToContactMutation, 
  useRemoveRelationFromContactMutation 
} from '../../api/membership';

interface EditRolesDialogProps {
  open: boolean;
  contactId: string;
  contactName: string;
  roles: OrganizationalRole[];
  /** Used by external components to indicate that a resource is still loading.
   * This will prevent the edit dialog from closing before it is once againt
   * true.
   *
   * e.g. contacts are still being fetched
   */
  isLoading: boolean;
  onClose: (...args: any[]) => any;
}

const EditRolesDialog: React.FC<EditRolesDialogProps> = ({ open, contactId, contactName, roles, isLoading = false, onClose: handleClose }) => {
  const { data: user } = useGetUserInfoQuery();
  const { organizationId } = useAppSelector(state => state.organization);
  const [addRelationToContact, { isLoading: isLoadingAddingRelation }] = useAddRelationToContactMutation();
  const [removeRelationFromContact, { isLoading: isLoadingRemovingRelation }] = useRemoveRelationFromContactMutation();
  const [checkedRoles, setCheckedRoles] = useState<{[role: string]: boolean}>({});
  const [isClosing, setIsClosing] = useState<boolean>(false);
  
  // A variable which indicates that the changes are still loading or contacts
  // are being refetched. This should be true after an edit to roles happens.
  const isLoadingChanges = isLoading || isLoadingAddingRelation || isLoadingRemovingRelation;
  
  // Create an array from the editable roles set.
  const editableRoles: OrganizationalRole[] = Array.from(editableRolesSet);

  const resetCheckedRoles = () => {
    setCheckedRoles(
      editableRoles.reduce((accumulator, editableRole) => ({ 
        ...accumulator,
        [editableRole]: Boolean(roles.find(role => role === editableRole))
      }), {})
    );
  };

  // When "open" changes, reset the checked roles. This will clear the values
  // for the next contact.
  useEffect(() => {
    resetCheckedRoles();
  }, [open, setCheckedRoles]);


  const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const hasRole = checkedRoles[event.currentTarget.name] !== undefined;

    if (!hasRole) return;

    setCheckedRoles({
      ...checkedRoles,
      [event.currentTarget.name]: event.target.checked,
    });
  }

  const handleSubmitClick: React.MouseEventHandler = async () => {
    for (const [role, isChecked] of Object.entries(checkedRoles)) {
      const contactHasRole = roles.find(r => r === role);
      
      if (contactHasRole && !isChecked) {
        // Remove the role if the checkbox is not checked, and contact had the
        // role already.
        removeRelationFromContact({
          contactId,
          organizationId: organizationId as number,
          relation: role as OrganizationalRole,
        })
      } else if (!contactHasRole && isChecked) {
        addRelationToContact({
          contactId,
          organizationId: organizationId as number,
          relation: role as OrganizationalRole,
        });
      }

      setIsClosing(true);
    }
  };

  useEffect(() => {
    if (!isClosing) return;

    if (!isLoadingChanges) {
      handleClose();
      setIsClosing(false);
    }
  }, [isClosing, isLoadingChanges])

  const shouldDisableEdit = (role: OrganizationalRole) => {
    // Disable checkboxes if the user is submitting.
    if (isLoadingChanges) {
      return true;
    }

    switch (role) {
      case 'DE':
        return !checkPermission(permissions.editDelegates, user!.relation);
      case 'CRA':
        return !checkPermission(permissions.editCRA, user!.relation);
      case 'CR':
        return true;
      default:
        return false;
    }
  }

  return (
    <Dialog open={open}>
      <DialogTitle>Edit {contactName}'s Roles</DialogTitle>
      <DialogContent>
        <List>
          { Array.from(editableRoles).map(editableRole => (
            <EditRoleRow 
              key={editableRole}
              role={editableRole} 
              checked={Boolean(checkedRoles[editableRole as unknown as any])}
              disabled={shouldDisableEdit(editableRole)}
              onChange={handleCheckboxChange}
            />
          ))} 
        </List>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button color="primary" onClick={handleSubmitClick}>Submit</Button>
      </DialogActions>
      { 
        isLoadingChanges 
          ? <LinearProgress />
          : null
      }
    </Dialog>
  );
};

export default EditRolesDialog;

interface EditRoleRowProps {
  role: OrganizationalRole;
  checked?: boolean;
  disabled?: boolean;
  onChange?: (...args: any[]) => any;
}

const EditRoleRow: React.FC<EditRoleRowProps> = ({ role, checked, disabled, onChange: handleChange }) => {
  const theme = useTheme();
  const [popoverAnchor, setPopoverAnchor] = useState<HTMLElement | null>(null);

  const handleHelpClick: React.MouseEventHandler = (event) => {
    setPopoverAnchor(event.currentTarget as HTMLElement);
  };

  const handleHelpClose = () => {
    setPopoverAnchor(null);
  };

  return (
    <ListItem>
      <FormControlLabel 
        label={getRelationNameFromCode(role)}
        control={
          <Checkbox
            name={role}
            edge="start"
            color="primary"
            checked={checked}
            tabIndex={-1}
            disabled={disabled}
            onChange={handleChange}
          />
        }
      />
      <Box display="flex" alignItems="center" gridGap={theme.spacing(1)}>
        <span onClick={handleHelpClick}>
          <HelpIcon htmlColor={theme.palette.secondary.light} />
        </span>
        { popoverAnchor 
          ? <RoleHelpPopover 
              open={Boolean(popoverAnchor)} 
              role={role} 
              anchorEl={popoverAnchor || undefined} 
              onClose={handleHelpClose}
            />
          : null
        }
      </Box>
    </ListItem>
  );
};
