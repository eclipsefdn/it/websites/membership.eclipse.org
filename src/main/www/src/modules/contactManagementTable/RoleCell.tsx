import { Box, IconButton, Typography } from '@material-ui/core';
import { GridRenderCellParams } from '@mui/x-data-grid';
import { Edit as EditIcon } from '@material-ui/icons';
import Protected from '@components/Protected';
import permissions from '../../Constants/permissions';
import { getRelationNameFromCode } from '../../api/shared';
import { OrganizationalRole } from '../../types';

interface RoleCellProps extends GridRenderCellParams {
  onEditClick: (...args: any[]) => any;
}

const RoleCell: React.FC<RoleCellProps> = ({ 
  onEditClick: handleEditClick, 
  value: roles, 
}) => {
  const roleNames = (roles as OrganizationalRole[])
    .map(getRelationNameFromCode)
    .filter(r => r !== undefined)
    .join(', ');

  return (
    <Box display="flex" alignItems="center" justifyContent="space-between" width="100%" style={{gap: '1rem'}}>
      <Typography variant="body2" style={{ whiteSpace: 'break-spaces' }}>
        {roleNames} 
      </Typography>
      <Protected allowedRoles={permissions.editContactRoles}>
        <IconButton onClick={handleEditClick} color="secondary" size="small" aria-label="edit">
          <EditIcon />
        </IconButton>
      </Protected>
    </Box>
  );
};

export default RoleCell;
