import { useState } from 'react';
import { 
  Button,
  Dialog, 
  DialogTitle, 
  DialogContent, 
  DialogActions, 
  Typography,
  TextField,
} from '@material-ui/core';
import Spacer from '@components/Spacer';
import { useAppSelector } from '../../hooks';
import { 
  useRequestOrganizationContactRemovalMutation,
} from '../../api/membership';

interface RequestContactRemovalDialogProps {
  open: boolean;
  contactId: string;
  contactName: string;
  onClose: (...args: any[]) => any;
}

const RequestContactRemovalDialog: React.FC<RequestContactRemovalDialogProps> = ({ open, contactId, contactName, onClose: handleClose }) => {
  const { organizationId } = useAppSelector(state => state.organization);
  const [requestContactRemoval] = useRequestOrganizationContactRemovalMutation();
  const [details, setDetails] = useState('');

  // Update the details state when the user inputs into the text field.
  const handleDetailsChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
    setDetails(event.target.value);
  }

  // When the submit button is clicked, send a contact removal request to the
  // Membership API. Afterwards, close the dialog.
  const handleSubmitClick: React.MouseEventHandler = () => {
    requestContactRemoval({ 
      organizationId: organizationId as number,
      contactId,
      details,
    });
    handleClose();
  };

  return (
    <Dialog open={open}>
      <DialogTitle>Request to Remove {contactName}</DialogTitle>
      <DialogContent>
        <Typography>
          Please provide any details, if necessary, relevant to Eclipse
          regarding removing this individual from your organisation in the
          Eclipse database. (Optional)
        </Typography>
        <Spacer y={2} />
        <TextField
          label="Details (Optional)"
          variant="outlined"
          rows={8}
          inputProps={{maxLength: 700}}
          onChange={handleDetailsChange}
          multiline
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button color="primary" onClick={handleSubmitClick}>Submit</Button>
      </DialogActions>
    </Dialog>
  );
};

export default RequestContactRemovalDialog;

