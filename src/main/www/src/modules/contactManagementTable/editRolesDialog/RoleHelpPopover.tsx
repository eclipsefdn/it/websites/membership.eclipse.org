import { Box, Typography, Popover } from '@material-ui/core';
import { OrganizationalRole } from '../../../types';

interface RoleHelpPopoverProps {
  open: boolean;
  role: OrganizationalRole;
  anchorEl?: HTMLElement;
  onClose?: (...args: any[]) => any;
}

const RoleHelpPopover: React.FC<RoleHelpPopoverProps> = ({ 
  open, 
  role, 
  anchorEl,
  onClose: handleClose 
}) => {
  return (
    <Popover 
      id={open ? "role-help-popover" : undefined}
      open={open}
      anchorEl={anchorEl}
      anchorOrigin={{
        horizontal: 'right',
        vertical: -20,
      }}
      transformOrigin={{
        horizontal: -10,
        vertical: 'top',
      }}
      onClose={handleClose}
    >
      <Box padding="2rem" maxWidth="40rem">
        <Typography>
          {getRoleDescription(role)}
        </Typography>
      </Box>
    </Popover>
  );
}

export default RoleHelpPopover;

const roleDescriptions: Map<OrganizationalRole, string> = new Map([
  ['CR', 'The Member Representative is the primary point of contact between your organization and Eclipse Foundation. We may reach out to Member Representatives from time to time with various business related activities, in particular in relation to your benefits as a member of the General Assembly. The Member Representative also has the authority for updating company information stored in the Member Portal.'],
  ['CRA', 'Alternate Member Representative shall be a secondary point of contact between your organization and Eclipse Foundation. In the absence of the Member Representative, we may reach out to the Alternate Member Representative for all communications and business related activities. The Alternate Member Representative also has the authority for updating company information stored in the Member Portal.'],
  ['MA', 'Marketing Representative shall be the point of contact for all marketing initiatives in relation to the Foundation. Our marketing team may reach out to the Marketing Representative from time to time for all marketing related activities, and will be included in the Foundation\'s marketing campaigns.'],
  ['DE', 'Member Representatives may delegate some or all of their responsibilities related to this Member Portal to Delegates. That is, Delegates are granted by the Member Representative the authority for updating company information.'],
]);

const getRoleDescription = (role: OrganizationalRole) => {
  if (roleDescriptions.has(role)) {
    return roleDescriptions.get(role);
  }

  return `There is no description set for the ${role} role.`;
}
