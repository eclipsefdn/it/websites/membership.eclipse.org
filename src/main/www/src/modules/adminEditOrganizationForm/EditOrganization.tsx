/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { 
  Box, 
  Link,
  CircularProgress, 
  Grid, 
  Typography 
} from '@material-ui/core';
import InfoForm from './editOrganization/InfoForm';
import WebLogoForm from './editOrganization/WebLogoForm';
import PrintLogoForm from './editOrganization/PrintLogoForm';
import AddNewLinkForm from '../AddNewLinkForm';
import EditOrganizationLinks from '../EditOrganizationLinks';
import { useGetOrganizationQuery } from '../../api/membership';
import { Alert } from '@material-ui/lab';

interface EditOrganizationProps {
  id: number;
  name: string;
}

const EditOrganization = ({ id, name }: EditOrganizationProps) => {
  const { data: organization, error, isLoading } = useGetOrganizationQuery(id);

  if (error) {
    return <Alert severity="error">Could not load {name}. Please try again later.</Alert>
  }

  if (isLoading) {
    return null;
  }

  const infoFormInitialValues = {
    description: organization?.description?.long || '',
    url: organization?.website || '',
  };

  return (
    <Box my="1rem">
      <Typography component="h2" variant="h5">
        {name}
      </Typography>
      { isLoading ? <CircularProgress /> : (
        <Box my="1rem">
          <InfoForm id={id} initialValues={infoFormInitialValues} />
          <Grid container spacing={3}>
            <Grid item xl={3} xs={6}>
              <WebLogoForm id={id} initialLogo={organization!.logos.web || ''} />
            </Grid>
            <Grid item xl={3} xs={6}>
              <PrintLogoForm id={id} />
            </Grid>
            <Grid item xs={6} />
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={12}>
                <Typography component="h2" variant="h6">
                  Links
                </Typography>
                <Typography>
                  This enables you to add link to your <Link href={`https://www.eclipse.org/membership/showMember.php?member_id=${id}`}>Membership Page</Link>,
                  products and services that aimed to be highlighted. Simply
                  click [add] to add new entree and to edit an existing entry.
                  To remove an entry, simply use the delete button for each
                  links you'd like to remove.
                </Typography>
                <Typography>
                  Please email <Link href="mailto:membership_admin@eclipse.org">membership_admin@eclipse.org</Link>{' '} 
                  if you required assistance.
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <EditOrganizationLinks organizationId={id} />
              </Grid>
              <Grid item xs={6}>
                <AddNewLinkForm organizationId={id} />
              </Grid>
            </Grid>
          </Grid>
        </Box>
      )}
    </Box>
  );
}

export default EditOrganization;

