/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import FormContainer from '@components/FormContainer';
import { TextField, Spacer } from '@components';
import { useForm } from '../hooks';
import { Box, Button, Grid, Typography } from '@material-ui/core';
import { useAddOrganizationProductMutation } from '../api/membership';
import { organizationProductsSchema } from '../validation';
import { FormikHelpers } from 'formik';

const AddNewLinkForm: React.FC = () => {
  const { handleSubmit, handleFieldChange, handleFieldBlur, formValues, hasErrors, dirty } = useForm<AddNewLinkFormValues>();
  return (
    <form onSubmit={handleSubmit}>
      <Typography component="h2" variant="h6">Add New Link</Typography>
      <Spacer y={1} />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField 
            name="name"
            value={formValues.name}
            variant="outlined" 
            label="Title" 
            onChange={handleFieldChange}
            onBlur={handleFieldBlur}
            fullWidth 
          />
        </Grid>
        <Grid item xs={12}>
          <TextField 
            name="description"
            value={formValues.description}
            variant="outlined" 
            label="Description" 
            rows={8}
            helperText="700 character limit" 
            inputProps={{maxLength: 700}}
            onChange={handleFieldChange}
            onBlur={handleFieldBlur}
            multiline
            fullWidth 
          />
        </Grid>
        <Grid item xs={12}>
          <TextField 
            name="url"
            value={formValues.url}
            variant="outlined" 
            label="URL" 
            onChange={handleFieldChange}
            onBlur={handleFieldBlur}
            fullWidth 
          />
        </Grid>
        <Grid item>
          <Box display="flex" gridGap="1rem">
            <Button 
              type="submit"
              variant="contained" 
              color="primary"
              disabled={hasErrors}
            >
              Add
            </Button>
            <Button 
              variant="contained" 
              color="secondary"
              disabled={!dirty}
            >
              Cancel
            </Button>
          </Box>
        </Grid>
      </Grid>
    </form>
  );
}

interface AddNewLinkFormProps {
  organizationId: number;
}

interface AddNewLinkFormValues {
  name: string;
  url: string;
  description: string;
}

const WrappedAddNewLinkForm: React.FC<AddNewLinkFormProps> = ({ organizationId }) => {
  const [addOrganizationProduct] = useAddOrganizationProductMutation();

  const handleSubmit = async (formValues: AddNewLinkFormValues, helpers: FormikHelpers<AddNewLinkFormValues>) => {
    helpers.setSubmitting(true);

    const { error } = await addOrganizationProduct({ 
      organizationId,
      name: formValues.name,
      url: formValues.url,
      description: formValues.description,
    });

    if (error) {
      return;
    }

    helpers.resetForm();
    helpers.setSubmitting(false);
  };

  const initialValues: AddNewLinkFormValues = {
    name: '',
    url: '',
    description: '',
  };

  return (
    <FormContainer 
      validationSchema={organizationProductsSchema} 
      initialValues={initialValues} 
      onSubmit={handleSubmit}
    >
      <AddNewLinkForm />
    </FormContainer>
  );
}

export default WrappedAddNewLinkForm;
