/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useState } from 'react';
import { useAppSelector } from '../hooks';
import { useGetOrganizationContactsQuery, useGetUserInfoQuery, useRequestOrganizationContactRemovalMutation } from '../api/membership';
import { Box, Button, Typography, Theme, createStyles, makeStyles } from '@material-ui/core';
import { DataGrid, GridColDef, GridFilterItem, GridFilterOperator, getGridStringOperators } from '@mui/x-data-grid';
import { Alert, AlertTitle } from '@material-ui/lab';
import Toolbar from './contactManagementTable/Toolbar';
import RoleCell from './contactManagementTable/RoleCell';
import EditRolesDialog from './contactManagementTable/EditRolesDialog';
import RequestContactRemovalDialog from './contactManagementTable/RequestContactRemovalDialog';
import permissions from '../Constants/permissions';
import { applicableRolesFilter } from '../utils';
import { checkPermission } from '../utils/portalFunctionHelpers';
import { OrganizationalRole } from '../types';
import { useLocation } from 'react-router-dom';

const rowsPerPageOptions = [5,  10, 100];

interface ContactRow {
  id: string;
  name: string;
  email: string;
  role: OrganizationalRole[];
}

/**
  * The contact management table component. This component allows authorized
  * users to edit contact roles or request to remove them entirely. 
  */
const ContactManagementTable: React.FC = () => {
  const classes = useStyles();
  const location = useLocation();
  const { organizationId } = useAppSelector(state => state.organization);
  const { data: user } = useGetUserInfoQuery();
  const { 
    data: contacts, 
    isFetching: isFetchingContacts,
    error: contactsError,
  } = useGetOrganizationContactsQuery(organizationId!);
  const [editRolesDialogOpen, setEditRolesDialogOpen] = useState(false);
  const [requestContactRemovalDialogOpen, setRequestContactRemovalDialogOpen] = useState(false);
  const [selectedContact, setSelectedContact] = useState<ContactRow | null>(null);

  // Get the URL search params to determine whether or not we should have a
  // contact filter.
  const searchParams = new URLSearchParams(location.search);
  const filter = searchParams.get('filter');

  const handleEditRolesDialogClose = () => {
    setEditRolesDialogOpen(false);
  };

  const handleRequestContactRemovalDialogClose = () => {
    setRequestContactRemovalDialogOpen(false);
  }

  const columns: GridColDef[] = [
    {
      field: 'name',
      headerName: 'Name',
      minWidth: 150,
      flex: 1,
    },
    {
      field: 'email',
      headerName: 'Email',
      minWidth: 200,
      flex: 1,
    },
    {
      field: 'role',
      headerName: 'Role(s)',
      filterOperators,
      flex: 2,
      minWidth: 320,
      valueFormatter: (value) => value.row.role.join(', '),
      renderCell: (params) => <RoleCell {...params} onEditClick={() => { 
          setSelectedContact(params.row as any);
          setEditRolesDialogOpen(true);
        }} 
      />,
    },
    {
      field: 'request-remove',
      headerName: ' ',
      width: 220,
      hide: !checkPermission(permissions.requestRemovalOfContacts, user!.relation),
      renderCell: (params) => (
        <Button 
          variant="contained" 
          onClick={() => {
            setSelectedContact(params.row as any);
            setRequestContactRemovalDialogOpen(true);
          }}
        >
          Request Removal
        </Button>
      ),
    }
  ];

  const rows = contacts 
    ? contacts.map(contact => ({
        id: contact.id,
        name: `${contact.firstName} ${contact.lastName}`,
        email: contact.email ?? 'no email found',
        role: contact.relations.filter(applicableRolesFilter),
      }))
    : [];

  return (
    <>
      <Box minHeight={600}>
        <Typography className={classes.header}>Contacts</Typography>
        <DataGrid
          className={classes.dataGrid}
          rows={rows}
          columns={columns}
          rowsPerPageOptions={rowsPerPageOptions}
          filterModel={filter ? getFilterModel(filter) : undefined}
          error={contactsError}
          components={{
            Toolbar,
            ErrorOverlay,
          }}
          disableVirtualization
          disableSelectionOnClick
        />
      </Box>
      { selectedContact 
        ? <EditRolesDialog 
            open={editRolesDialogOpen} 
            contactId={selectedContact.id}
            contactName={selectedContact.name} 
            roles={selectedContact.role} 
            onClose={handleEditRolesDialogClose}
            isLoading={isFetchingContacts}
          />
        : null
      }
      { selectedContact
        ? <RequestContactRemovalDialog
            open={requestContactRemovalDialogOpen}
            contactId={selectedContact.id}
            contactName={selectedContact.name}
            onClose={handleRequestContactRemovalDialogClose}
          />
        : null
      }
    </>
  );
};

export default ContactManagementTable;

const ErrorOverlay: React.FC = () => {
  return (
    <Box display="flex" width="100%" height="100%" alignItems="center" justifyContent="center" position="absolute">
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        Could not fetch contacts at this moment in time. Please try again
        later.
      </Alert>
    </Box>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    dataGrid: {
      minHeight: '50rem',
    },
    dataGridLoading: {
      filter: 'grayscale(0.8)',
      userSelect: 'none',
    },
    header: {
      padding: theme.spacing(1),
      borderTopLeftRadius: theme.spacing(0.5),
      borderTopRightRadius: theme.spacing(0.5),
      color: theme.palette.neutral['50'],
      backgroundColor: theme.palette.primary.main, 
    },
  }));

const getFilterModel = (filter?: string) => {
  switch (filter) {
    case 'committer':
      return {
        items: [{
          columnField: 'role',
          operatorValue: 'contains',
          value: 'CM',
        }],
      };
    case 'contributor':
      return {
        items: [{
          columnField: 'role',
          operatorValue: 'excludes',
          value: 'CM',
        }],
      };
    default:
      return;
  }
}

const filterOperators: GridFilterOperator[] = [
  ...getGridStringOperators(),
  {
    value: 'excludes',
    label: 'excludes',
    // The input component can be the same as other operators and they are all the same.
    InputComponent: getGridStringOperators()[0].InputComponent,
    getApplyFilterFn: (filterItem: GridFilterItem) => {
      if (!filterItem.columnField || !filterItem.value || !filterItem.operatorValue) {
        return null;
      }
      return (params: any): boolean => {
        return !params.value.find((item: string) => item.toLowerCase().includes(filterItem.value.toLowerCase()));
      };
    },
  },
];

