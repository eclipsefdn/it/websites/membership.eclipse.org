/*!
 * Copyright (c) 2025 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { Router } from 'react-router-dom';
import { 
  render as tlRender, 
  RenderOptions 
} from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryHistory } from 'history';
import { setupStore } from '../store';

type ReduxStore = ReturnType<typeof setupStore>;

interface RendererRouterOptions {
  initialEntries?: string[];
  history?: MemoryHistory,
}

export class Renderer {
  private store: ReduxStore;
  private history?: any;

  constructor() {
    this.store = setupStore();
  }

  /** Configures Redux to be wrapped around your component
   *  @param store - The store to be used in the Redux Provider
   */
  redux(store: ReduxStore) {
    this.store = store;
    return this;
  }

  /** Configures the Router from react-router-dom to be wrapped around your
   * component */
  router({ history }: RendererRouterOptions) {
    this.history = history;
    return this;
  }

  /** Renders the given component wrapped with your configuration and providers
   *  @param component - The component to render
   *  @param renderOptions - Render options for the react-testing-library
   *  render function
   */
  render(component: React.ReactElement, renderOptions: RenderOptions = {}) {
    const Wrapper = ({ children }: React.PropsWithChildren<{}>): JSX.Element => {
      return (
        <Provider store={this.store}>
          <Router
            /*@ts-ignore*/
            history={this.history}
          >
            {children}
          </Router>
        </Provider>
      );
    };

    return tlRender(component, {
      wrapper: Wrapper,
      ...renderOptions,
    });
  }
}
