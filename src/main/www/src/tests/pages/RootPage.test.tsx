/*!
 * Copyright (c) 2025 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '@testing-library/jest-dom/extend-expect';
import RootPage from '../../pages/portal/RootPage';
import { waitFor } from '@testing-library/react';
import { http, HttpResponse } from 'msw';
import { server } from '../../api/mocks/server';
import { generateMockAdminUserInfo, generateMockElevatedUserInfo } from '../../api/mocks/data/membership';
import { createMemoryHistory } from 'history';
import { Renderer } from '../utils';

describe('RootPage', () => {
  it('Redirects member user to dashboard page', async () => {
    // No need to patch the API response since by default it generates a member
    // user.
    
    // Setup UI preconditions
    const history = createMemoryHistory({ initialEntries: ['/portal' ] });
    
    const renderer = new Renderer().router({ history });
    renderer.render(<RootPage/>);

    await waitFor(() => expect(history.location.pathname).toBe('/portal/dashboard'));
  });

  it('Redirects admin user to standby page', async () => {
    // Setup API responses preconditions
    server.use(
      http.get('http://localhost/api/userinfo', async () => {
        return HttpResponse.json(generateMockAdminUserInfo())
      })
    );
    
    // Setup UI preconditions
    const history = createMemoryHistory({ initialEntries: ['/portal' ] });
    
    const renderer = new Renderer().router({ history });
    renderer.render(<RootPage/>);

    await waitFor(() => expect(history.location.pathname).toBe('/portal/standby'));
  });

  it('Redirects elevated users to standby page', async () => {
    server.use(
      http.get('http://localhost/api/userinfo', async () => {
        return HttpResponse.json(generateMockElevatedUserInfo());
      })
    );
    
    // Setup UI preconditions
    const history = createMemoryHistory({ initialEntries: ['/portal' ] });
    
    const renderer = new Renderer().router({ history });
    renderer.render(<RootPage/>);

    await waitFor(() => expect(history.location.pathname).toBe('/portal/standby'));
  });
});

