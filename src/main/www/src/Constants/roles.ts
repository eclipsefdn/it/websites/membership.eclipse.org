/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Role } from '../types';

// Type-safe enum of roles. The keys and values need to be the same.
const roleKeys = {
    admin: 'admin',
    elevated: 'elevated',
    employee: 'employee',
    companyRep: 'companyRep',
    companyRepAlternate: 'companyRepAlternate',
    marketing: 'marketing',
    delegate: 'delegate'
} as const;
type RoleKeys = keyof typeof roleKeys;

// This object includes relation roles (ex. company rep) and additional roles
// (ex. elevated). The values are the actual roles as defined in the API or
// database.
export const roles: { [key in RoleKeys]: Role} = {
    admin: 'admin',
    elevated: 'eclipsefdn_membership_portal_elevated_access',
    employee: 'EMPLY',
    companyRep: 'CR',
    companyRepAlternate: 'CRA',
    delegate: 'DE',
    marketing: 'MA'
};
